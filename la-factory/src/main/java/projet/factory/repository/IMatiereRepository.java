package projet.factory.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import projet.factory.model.Matiere;

public interface IMatiereRepository extends JpaRepository<Matiere, Long>{
	@Query("select m from Matiere m join m.formateurs f where f.id= :monid")
    List<Matiere> findAllMatiereByFormateurId(@Param("monid") long id);
	
	@Query("select ma from Matiere ma left join ma.module m left join m.formation f where f.id = :monid")
	List<Matiere> findMatieresFormateursByFormation(@Param("monid")Long id);
	
	@Query("select m from Matiere m where m not in (select m from Matiere m join m.formateurs f where f.id= :monid)")
    List<Matiere> findAllMatiereWithoutFormateurId(@Param("monid") long id);
	
}
