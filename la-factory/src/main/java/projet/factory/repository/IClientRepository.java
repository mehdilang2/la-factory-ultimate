package projet.factory.repository;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import projet.factory.model.Client;

public interface IClientRepository extends JpaRepository<Client,Long>{

	
	@Query ("select distinct c from Client c left join fetch c.clientFormations cf left join fetch cf.formation f where c.id = :monid")
	Client findByIdWithFormation(@Param("monid") Long id);
	
	@Query("select distinct c from Client c left join fetch c.stagiaires where c.id = :monid")
	Client findByIdWithStagiaire(@Param("monid") Long id);
	
	@Query("select distinct c from Client c left join c.clientFormations cf left join cf.formation f left join f.gestionnaire where c.id = :monid")
	Client findByIdWithGestionnaire(@Param("monid") Long id);
	
	@Query("select c from Client c left join c.clientFormations cf left join cf.formation f where f.id = :monid")
	List<Client> findClientsByFormation(@Param("monid") Long id);
	
	@Query("select c from Client c left join c.contacts ct  where ct.id = :monid")
	Client findClientByContact(@Param("monid") Long id);
	
	
}
