package projet.factory.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import projet.factory.model.Formation;
import projet.factory.model.StagiaireFormation;


public interface IStagiaireFormationRepository extends JpaRepository<StagiaireFormation, Long>{
	
	@Transactional
	 @Modifying
	@Query("delete  StagiaireFormation  where stagiaire.id= :monid")
	void deleteStagiaireFormationByStagiaire(@Param("monid") Long id);
	
	@Transactional
	 @Modifying
	@Query("delete  StagiaireFormation  where formation.id= :monid")
	void deleteStagiaireFormationByFormation(@Param("monid") Long id);
}
