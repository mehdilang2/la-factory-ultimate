package projet.factory.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import projet.factory.model.Module;


public interface IModuleRepository extends JpaRepository<Module,Long>{
	
//	@Query("select m from Module m left join fetch m.formateur f where m.id = :monId")
//	Module findByModuleIdWithFormateur(@Param("monId") Long id);
//	
//	@Query("select m from Module m where m.dtDebut = :dateDebut AND m.dtFin = :dateFin")
//	Module findModuleByDate(@Param("dateDebut")Date dtDebut, @Param("dateFin")Date dtFin);
//	
//	@Query("select m from Module where m.formation = formationid")
//	List<Module> findModuleByFormation(@Param("formationid")Long formationid);
	
//	@Query("select m from Module where m.formateur = formateurid")
//	List<Module> findModulesByFormateur(@Param("formateurid")Long formateurid);
	
//	@Query("")
//	List<Module> findModuleByTitre(@Param String titre);
	
//	findModuleByDuree

	@Query("select m from Module m left join fetch m.matiere ma where m.id = :monid")
	List<Module> findModulesWithMatieres(@Param("monid")Long id);
	
	@Query("select m from Module m left join fetch m.formation f where f.id = :monid")
	List<Module> findModulesByFormation(@Param("monid")Long id);
	
	
	


	
	
	

}
