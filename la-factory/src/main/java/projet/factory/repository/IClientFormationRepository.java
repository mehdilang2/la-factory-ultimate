package projet.factory.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import projet.factory.model.ClientFormation;

public interface IClientFormationRepository extends JpaRepository<ClientFormation, Long>{
	
	@Query ("select cf from ClientFormation cf left join cf.client c where c.id = :monid")
	ClientFormation findByIdClient(@Param("monid") Long id);

}
