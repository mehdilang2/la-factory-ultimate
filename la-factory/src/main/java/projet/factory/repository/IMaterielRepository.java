package projet.factory.repository;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import projet.factory.model.Materiel;
import projet.factory.model.Ordinateur;
import projet.factory.model.Salle;
import projet.factory.model.Videoprojecteur;

public interface IMaterielRepository extends JpaRepository<Materiel,Long>{
	@Query("select o from Ordinateur o")
    List<Ordinateur> findAllOrdinateur();
    @Query("select s from Salle s")
    List<Salle> findAllSalle();
    @Query("select v from Videoprojecteur v")
    List<Videoprojecteur> findAllVideoprojecteur();
    
//    @Query("select v from Videoprojecteur v left join v.materielFormation mf left join mf.formation f where mf.dtDebut<= myDate && mf.dtFin <= myDate")
//    List<Videoprojecteur> findAllVideoprojecteurLastFormationDate(@Param("MyDate") Date GetDate(););
    
    @Query("select o from Ordinateur o left join o.materielFormation om left join om.formation f where f.id = :monId")
    List<Ordinateur> findOrdinateursWithFormation(@Param("monId") Long id);
    
    @Query("select o from Ordinateur o left join o.materielFormation om left join om.formation f")
    List<Ordinateur> findAllOrdinateursWithFormation();
    
    
    @Query("select s from Salle s left join s.materielFormation sm left join sm.formation f where f.id = :monId")
    List<Salle> findSallesWithFormation(@Param("monId") Long id);
    
	@Query("select s from Salle s left join s.materielFormation mf where s.id = :monId")
    Salle findSalleWithDates(@Param("monId") Long id);
	
	@Query("select o from Ordinateur o left join o.materielFormation mf where o.id = :monId")
    Ordinateur findOrdinateurWithDates(@Param("monId") Long id);
    
    @Query("select v from Videoprojecteur v left join v.materielFormation vm left join vm.formation f where f.id = :monId")
    List<Videoprojecteur> findVideoprojecteursWithFormation(@Param("monId") Long id);
    
    @Query("select o from Ordinateur o left join o.stagiaire s where s.nom = :monNom and s.prenom = :monPrenom")
    List<Materiel> findAllMaterielfromStagiaireNomAndPrenom(@Param("monNom") String nom, @Param ("monPrenom") String prenom);
    
    @Query("select m from Materiel m left join m.materielFormation mf where mf.dtDebut > :maDate and mf.dtFin < :maDate")
    List<Materiel> findAllMaterielDispoByDate(@Param("maDate") Date date);
    
    @Query("select m from Materiel m left join m.materielFormation mf left join mf.formation f where f.intitule = :monIntitule")
    List<Materiel> findMaterielfromFormationIntitule(@Param("monIntitule") String intitule);
    
    @Query("select m from Materiel m left join m.materielFormation mf left join mf.formation f where f.id = :monid")
    List<Materiel> findByIdWithMateriel(@Param("monid")Long monid);
}
