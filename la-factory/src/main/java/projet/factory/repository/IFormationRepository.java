package projet.factory.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import projet.factory.model.Formation;


public interface IFormationRepository extends JpaRepository<Formation, Long>{

	
	@Query("select f from Formation f left join  f.stagiairesFormations sf left join sf.stagiaire s where s.nom = :nomStagiaire")
	Formation findFormationbyNomStagiaire(@Param("nomStagiaire") String nom);	
	
	@Query("select f from Formation f left join f.stagiairesFormations sf left join sf.stagiaire s where s.id = :idStagiaire")
	List <Formation> findFormationbyIdStagiaire(@Param("idStagiaire") Long id);	
	
	@Query("select f from Formation f left join fetch f.clientFormations c left join fetch c.formation t where f.id = :idClient")
	List<Formation> findFormationbyIdClient(@Param("idClient") Long id);

	@Query("select f from Formation f left join fetch f.modules m where f.id = :idModule")
	Formation findFormationbyIdModule(@Param("idModule") Long id);
	
	@Query("select f from Formation f left join fetch f.materielFormation m left join fetch m.materiel n where f.id = :idMateriel")
	List<Formation> findFormationbyIdMateriel(@Param("idMateriel") Long id);   
	
	@Query("select f from Formation f left join fetch f.gestionnaire g where g.nom = :nomGestionnaire")
	List<Formation> findFormationbyNomGestionnaire(@Param("nomGestionnaire") String nom);
	
	@Query("select f from Formation f left join fetch f.gestionnaire g where f.id = :monid")
	Formation findByIdWithGestionnaire(@Param("monid") Long id);
	
	@Query("select f from Formation f left join fetch f.modules g where f.id = :monid")
	Formation findByIdWithModules(@Param("monid") Long id);
	
	@Query("select f from Formation f left join fetch f.materielFormation mf left join fetch mf.materiel m where f.id = :monid")
	Formation findByIdWithMatiere(@Param("monid") Long id);
	
	@Query("select f from Formation f left join f.clientFormations cf left join cf.client c where c.id = :monid")
	List<Formation> findFormationByIdClient(@Param("monid") Long id);
	
	@Query("select f from Formation f left join f.modules m left join m.formateur fo where fo.id = :monid")
	List<Formation> findFormationByIdFormateur(@Param("monid") Long id);
	
	
	

	
	
	
	
}
