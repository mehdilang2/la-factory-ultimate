package projet.factory.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import projet.factory.model.MaterielFormation;

public interface IMaterielFormationRepository extends JpaRepository<MaterielFormation,Long> {
	
	 @Query("select mf from MaterielFormation mf left join mf.materiel m where m.id = :monId")
	    List<MaterielFormation> findMaterielFormationByVideoProjecteur(@Param("monId") Long id);

}
