package projet.factory.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import projet.factory.model.Presence;

public interface IPresenceRepository extends JpaRepository<Presence, Long> {

}
