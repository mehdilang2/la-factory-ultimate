package projet.factory.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import projet.factory.model.Disponibilite;

public interface IDisponibiliteRepository extends JpaRepository<Disponibilite,Long>{
	
	
	@Query("select d from Disponibilite d left join fetch d.formateur f where f.id= :monid")
	List<Disponibilite>findDisponibiliteByformateur(@Param("monid") long id);
	
	@Query("select d from Disponibilite d")
	List<Disponibilite> findAll();
	
	
	
}
