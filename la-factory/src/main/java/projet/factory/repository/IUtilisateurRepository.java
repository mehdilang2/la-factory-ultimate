package projet.factory.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import projet.factory.model.TypeUser;
import projet.factory.model.Utilisateur;

public interface IUtilisateurRepository extends JpaRepository<Utilisateur, Long>{

	@Query("select u from Utilisateur u")
	List<Utilisateur> findAllUtilisateurs();
	
	@Query("select u from Utilisateur u where u.typeUser = :typeUser")
	List<Utilisateur> findUtilisateursByType(@Param("typeUser") TypeUser typeUser);
	
	@Query("select distinct u from Utilisateur u left join fetch u.personne p where u.mail = :mail")
	Utilisateur findUtilisateursByMail(@Param("mail") String mail);
}
