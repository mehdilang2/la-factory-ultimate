package projet.factory.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import projet.factory.model.Administrateur;
import projet.factory.model.Contact;
import projet.factory.model.Disponibilite;
import projet.factory.model.Formateur;
import projet.factory.model.Gestionnaire;
import projet.factory.model.Personne;
import projet.factory.model.Stagiaire;

public interface IPersonneRepository extends JpaRepository<Personne, Long>{
	
	@Query("select a from Administrateur a")
	List<Administrateur> findAllAdministrateur();
	@Query("select c from Contact c")
	List<Contact> findAllContact();
	@Query("select f from Formateur f")
	List<Formateur> findAllFormateur();
	@Query("select g from Gestionnaire g")
	List<Gestionnaire> findAllGestionnaire();
	@Query("select s from Stagiaire s")
	List<Stagiaire> findAllStagiaire();
	@Query ("select g from Gestionnaire g left join fetch g.formations f where f.id = :monid" )
	List<Gestionnaire> findAllGestionnaireByFormationId(@Param("monid") Long id);
	
	@Query("select d from Disponibilite d left join fetch d.formateur f where f.id= :monid")
	List<Disponibilite>findDisponibiliteByformateur(@Param("monid") long id);
	
	@Query ("select s from Stagiaire s left join fetch s.client c where c.id = :monid" )
	List<Stagiaire> findStagiaireByIdClient(@Param("monid") Long id);
	
	@Query("select s from Stagiaire s left join s.stagiaireFormations sf left join sf.formation f where f.id = :monid")
	List<Stagiaire> findStagiairesByFormation(@Param("monid") Long id);
	
	@Query("select s from Stagiaire s where s not in (select s from Stagiaire s left join s.stagiaireFormations sf left join sf.formation f where f.id = :monid)")
	List<Stagiaire> findStagiairesWithoutFormation(@Param("monid") Long id);
	
	@Query ("select g from Gestionnaire g left join fetch g.formations f where g.id = :monid" )
	Gestionnaire findGestionnaireWithFormations(@Param("monid") Long id);
	
	@Query ("select g from Gestionnaire g left join g.formations f left join f.clientFormations cf left join cf.client c where c.id = :monid" )
	List<Gestionnaire> findGestionnaireByIdClient(@Param("monid") Long id);
	 
	@Query ("select s from Stagiaire s left join s.stagiaireFormations sf left join sf.formation f left join f.modules m left join m.formateur t where t.id =:monid")
	List<Stagiaire>findStagiairesPresenceByFormateur(@Param("monid") Long id);
	
	@Query ("select g from Gestionnaire g left join g.formations f left join f.stagiairesFormations sf left join sf.stagiaire s where s.id = :monid" )
	Gestionnaire findGestionnaireByStagiaire(@Param("monid") Long id);
	
	@Query ("select c from Contact c left join c.client cc left join cc.stagiaires ccs where ccs.id = :monid" )
	Contact findContactByStagiaire(@Param("monid") Long id);
	
	@Query ("select c from Contact c left join c.client cl where cl.id = :monid" )
	List<Contact> findContactByIdClient(@Param("monid") Long id);
	
	@Query ("select f from Formateur f left join f.formations ff where ff.id = :monid" )
	Formateur findFormateurReferentByFormation(@Param("monid") Long id);
	
	@Query ("Select distinct f from Formateur f left join f.modules m left join m.formation fo where fo.id = :monid" )
	List<Formateur> findFormateurByFormation(@Param("monid") Long id);
	
	@Query ("select g from Gestionnaire g left join g.formations ff where ff.id = :monid" )
	Gestionnaire findGestionnaireByFormation(@Param("monid") Long id);
	
	@Query ("select s from Stagiaire s left join fetch s.presences p where s.id = :monid" )
	Stagiaire findStagiaireWithPresence(@Param("monid") Long id);
	
	@Query("Select f from Formateur f left join f.matieres m where m.id = :monid")
	List<Formateur> findFormateurByMatiere(@Param("monid") Long id);

	@Query("select s from Stagiaire s left join s.stagiaireFormations sf")
	List<Stagiaire> findStagiairesWithFormation();
	
	@Query("select s from Stagiaire s left join s.client c where c.id = :monid")
	List<Stagiaire> findStagiairesByIdClient(@Param("monid") Long id);
		
	
//	@Query ("select g from Gestionnaire g left join fetch g.formations f left join fetch f.clientFormations z left join fetch z.client c where c.id = :monid" )
//	List<Gestionnaire> findAllGestionnaireByClientId(@Param("monid") Long id);
////TODO TEST	@Query ("select g from Gestionnaire g left join fetch g.formations f left join fetch f.client c where c.nomEntreprise = :monnom" )
//	List<Gestionnaire> findAllGestionnaireByClientnomEntreprise(@Param("monnom") Long id);
//	
//
//	@Query ("from Formateur where matiere = :maMatiere")
//	List<Formateur> findAllByMatiere (@Param("maMatiere") String matiere);
//	@Query ("select f from Formateur f left join f.matieres m where m.titre = :titreMatiere")
//	List<Formateur>  findAllFormateurByTitreMatiere (@Param("titreMatiere") String titre);
}
