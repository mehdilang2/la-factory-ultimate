package projet.factory.model;

public enum TypeEntreprise {
	
	SARL, SA, SAS;

}
