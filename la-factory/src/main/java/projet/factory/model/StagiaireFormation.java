package projet.factory.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonView;

@Entity
public class StagiaireFormation {
 
@Id	
@GeneratedValue
@JsonView(Views.ViewCommon.class)
private long id;
 @Version
 @JsonView(Views.ViewCommon.class)
 private int version;
 
 @ManyToOne
 @JsonView({Views.ViewFormation.class,Views.ViewStagiaireFormation.class,Views.ViewOrdinateur.class}) 
 @JoinColumn(name="stagiaire_id")
 private Stagiaire stagiaire;
 @ManyToOne
 @JsonView({Views.ViewStagiaireFormation.class,Views.ViewStagiaireWithFormation.class}) 
 @JoinColumn(name="formation_id")
 private Formation formation;

 
 
public StagiaireFormation() {
	super();
	// TODO Auto-generated constructor stub
}



public StagiaireFormation(long id, int version, Stagiaire stagiaire, Formation formation) {
	super();
	this.id = id;
	this.version = version;
	this.stagiaire = stagiaire;
	this.formation = formation;
}



public long getId() {
	return id;
}

public void setId(long id) {
	this.id = id;
}

public int getVersion() {
	return version;
}

public void setVersion(int version) {
	this.version = version;
}

public Stagiaire getStagiaire() {
	return stagiaire;
}

public void setStagiaire(Stagiaire stagiaire) {
	this.stagiaire = stagiaire;
}

public Formation getFormation() {
	return formation;
}

public void setFormation(Formation formation) {
	this.formation = formation;
}
 
 
 
}
