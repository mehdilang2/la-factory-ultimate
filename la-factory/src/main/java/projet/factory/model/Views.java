package projet.factory.model;



public class Views {
	public static class ViewCommon {}
	
	public static class ViewClient extends ViewCommon{}
	
	public static class ViewClientWithContacts extends ViewClient{}
	
	public static class ViewClientWithStagiaires extends ViewClient{}
	
	public static class ViewClientWithGestionnaire extends ViewClient{}
	
	public static class ViewClientWithFormation extends ViewClient{}
	
	public static class ViewClienttWithContact extends ViewClient{}
	
	public static class ViewClientFormation extends ViewCommon{}
	
	public static class ViewPersonne extends ViewCommon{}
	
	public static class ViewAdministrateur extends ViewPersonne{}
	
	public static class ViewContact extends ViewPersonne{}
	
	public static class ViewFormateur extends ViewPersonne{}
	
	public static class ViewFormation extends ViewCommon{}
	
	public static class ViewFormationWithStagiaire extends ViewFormation{}
	
	public static class ViewFormationWithoutStagiaire extends ViewFormation{}
	
	public static class ViewFormationWithGestionnaire extends ViewFormation{}
	
	public static class ViewFormationWithModule extends ViewFormation{}
	
	public static class ViewFormationWithOrdinateur extends ViewFormation{}
	
	public static class ViewFormationWithSalle extends ViewFormation{}
	
	public static class ViewFormationWithVideoprojecteur extends ViewFormation{}
	
	public static class ViewFormationWithMatiere extends ViewFormation{}
	
	public static class ViewFormationWithClient extends ViewFormation{}
	
	public static class ViewFormationWithFormateurReferent extends ViewFormation{}
	
	public static class ViewFormationWithFormateur extends ViewFormation{}
	
	public static class ViewMateriel extends ViewCommon{}
	
	public static class ViewMatiere extends ViewCommon{}
	
	public static class ViewPresence extends ViewCommon{}
	
	public static class ViewMatiereWithModule extends ViewMatiere{}
	
	public static class ViewMatiereWithFormateur extends ViewMatiere{}
	
	public static class ViewModule extends ViewCommon{}
	
	public static class ViewModuleWithMatiere extends ViewModule{}
	
	public static class ViewModuleWithFormation extends ViewModule{}
	
	public static class ViewModuleWithFormateur extends ViewModule{}
	
	public static class ViewSalle extends ViewCommon{}
	
	public static class ViewSalleWithDates extends ViewSalle{}
	
	public static class ViewStagiaire extends ViewPersonne{}
	
	public static class ViewStagiaireWithClient extends ViewStagiaire{}
	
	public static class ViewStagiaireWithFormation extends ViewStagiaire{}
	
	public static class ViewStagiairewithpresence extends ViewStagiaire{}
	
	
	public static class ViewOrdinateur extends ViewCommon{}
	
	public static class ViewOrdinateurWithDates extends ViewOrdinateur{}

	public static class ViewDisponibilite extends ViewCommon{}
	
	public static class ViewVideoProjecteur extends ViewCommon{}
	
	public static class ViewFormateurWithDisponibilite extends ViewFormateur{}
	
	public static class ViewFormateurWithStagiairePresence extends ViewFormateur{}
	
	public static class ViewFormateurWithMatiere extends ViewFormateur{}
	
	public static class ViewMaterielWithFormateur extends ViewFormateur{}

	public static class ViewFormateurWithoutMatiere extends ViewFormateur{}
	
	public static class ViewFormateurWithFormation extends ViewCommon{}
	
	public static class ViewGestionnaire extends ViewPersonne{}
	
	public static class ViewGestionnaireWithFormations extends ViewGestionnaire{}

	public static class ViewUtilisateur extends ViewCommon{}
	
	public static class ViewUtilisateurWithPersonne extends ViewUtilisateur{}
	
	public static class ViewMaterielFormation extends ViewCommon{}
	
	public static class ViewDisponibiliteWithFormateur extends ViewCommon{}
	
	public static class ViewStagiaireFormation extends ViewCommon{}
	
	
	
	
	
	
}
