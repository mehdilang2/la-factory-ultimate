package projet.factory.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonView;


@Entity
public class ClientFormation {
	@Id
	@GeneratedValue
	@JsonView(Views.ViewCommon.class)
	private Long id;
	@Version
	@JsonView(Views.ViewCommon.class)
	private int version;
	@JsonView(Views.ViewCommon.class)
	private Integer demandes;
	@JsonView(Views.ViewCommon.class)
	private String commentaires;
	@JsonView(Views.ViewCommon.class)
	private Boolean statut;
	@JsonView(Views.ViewCommon.class)
	private Integer accordes;
	@ManyToOne
	@JoinColumn(name = "client_id")
	@JsonView(Views.ViewClientFormation.class)
	private Client client;
	@ManyToOne
	@JoinColumn(name = "formation_id")
	@JsonView({Views.ViewClientWithGestionnaire.class, Views.ViewClientFormation.class})
	private Formation formation;
	
	public ClientFormation() {
		super();
	}
	public ClientFormation(Long id, int version, Integer demandes, String commentaires, Boolean statut,
			Integer accordes, Client client, Formation formation) {
		super();
		this.id = id;
		this.version = version;
		this.demandes = demandes;
		this.commentaires = commentaires;
		this.statut = statut;
		this.accordes = accordes;
		this.client = client;
		this.formation = formation;
	}
	public Integer getDemandes() {
		return demandes;
	}
	public void setDemandes(Integer demandes) {
		this.demandes = demandes;
	}
	public Integer getAccordes() {
		return accordes;
	}
	public void setAccordes(Integer accordes) {
		this.accordes = accordes;
	}
	public String getCommentaires() {
		return commentaires;
	}
	public void setCommentaires(String commentaires) {
		this.commentaires = commentaires;
	}
	public Boolean getStatut() {
		return statut;
	}
	public void setStatut(Boolean statut) {
		this.statut = statut;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public Formation getFormation() {
		return formation;
	}
	public void setFormation(Formation formation) {
		this.formation = formation;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	
	
}
