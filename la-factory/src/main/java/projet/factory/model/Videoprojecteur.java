package projet.factory.model;


import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.fasterxml.jackson.annotation.JsonView;

@Entity
@DiscriminatorValue("Videoprojecteur")
public class Videoprojecteur extends Materiel {

	@JsonView(Views.ViewCommon.class)
	private Boolean hdmi;
	@JsonView(Views.ViewCommon.class)
	private Boolean vga;
	@Enumerated(EnumType.STRING)
	@JsonView(Views.ViewCommon.class)
	private Resolution resolution;
	
	
	
	
	public Videoprojecteur() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Videoprojecteur(Boolean hdmi, Boolean vga, Resolution resolution) {
		super();
		this.hdmi = hdmi;
		this.vga = vga;
		this.resolution = resolution;
	}
	public Boolean getHdmi() {
		return hdmi;
	}
	public void setHdmi(Boolean hdmi) {
		this.hdmi = hdmi;
	}
	public Boolean getVga() {
		return vga;
	}
	public void setVga(Boolean vga) {
		this.vga = vga;
	}
	public Resolution getResolution() {
		return resolution;
	}
	public void setResolution(Resolution resolution) {
		this.resolution = resolution;
	}
	
	
}
