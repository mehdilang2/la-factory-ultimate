package projet.factory.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonView;


@Entity
public class MaterielFormation {
	
	@Id
	@GeneratedValue
	@JsonView(Views.ViewCommon.class)
	private Long id;
	@Version
	@JsonView(Views.ViewCommon.class)
	private int version;
	
	@Temporal(TemporalType.DATE)
	@JsonView({Views.ViewSalleWithDates.class,Views.ViewSalle.class,Views.ViewVideoProjecteur.class,Views.ViewMaterielFormation.class,Views.ViewOrdinateur.class,Views.ViewSalle.class})
	private Date dtDebut;
	
	@Temporal(TemporalType.DATE)
	@JsonView({Views.ViewSalleWithDates.class,Views.ViewSalle.class,Views.ViewVideoProjecteur.class,Views.ViewMaterielFormation.class,Views.ViewOrdinateur.class,Views.ViewSalle.class})
	private Date dtFin;
	@ManyToOne
	@JoinColumn(name = "materiel_id")
	@JsonView({Views.ViewFormation.class,Views.ViewFormationWithOrdinateur.class, Views.ViewFormationWithSalle.class, Views.ViewFormationWithVideoprojecteur.class,Views.ViewMaterielFormation.class})
	private Materiel materiel;
	@ManyToOne
	@JoinColumn(name = "formation_id")
	@JsonView({Views.ViewOrdinateur.class,Views.ViewVideoProjecteur.class,Views.ViewSalle.class,Views.ViewMaterielFormation.class})
	private Formation formation;
	
	
	public MaterielFormation() {
		super();
		// TODO Auto-generated constructor stub
	}
	public MaterielFormation(Long id, int version, Date dtDebut, Date dtFin, Materiel materiel, Formation formation) {
		super();
		this.id = id;
		this.version = version;
		this.dtDebut = dtDebut;
		this.dtFin = dtFin;
		this.materiel = materiel;
		this.formation = formation;
	}
	public Date getDtDebut() {
		return dtDebut;
	}
	public void setDtDebut(Date dtDebut) {
		this.dtDebut = dtDebut;
	}
	public Date getDtFin() {
		return dtFin;
	}
	public void setDtFin(Date dtFin) {
		this.dtFin = dtFin;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Materiel getMateriel() {
		return materiel;
	}
	public void setMateriel(Materiel materiel) {
		this.materiel = materiel;
	}
	public Formation getFormation() {
		return formation;
	}
	public void setFormation(Formation formation) {
		this.formation = formation;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	
	
	
}
