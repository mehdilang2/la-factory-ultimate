package projet.factory.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name="disponibilite")
public class Disponibilite  {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonView(Views.ViewCommon.class)
	private Long id;
	@Version
	@JsonView(Views.ViewCommon.class)
	private int version;
	@JsonView(Views.ViewCommon.class)
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date dtDispo;
	@ManyToOne
	@JoinColumn(name = "formateur_id")
	@JsonView(Views.ViewCommon.class)
	private Formateur formateur;

	public Disponibilite() {
		super();
	}

	public Disponibilite(Long id, int version, Date dtDispo, Formateur formateur) {
		super();
		this.id = id;
		this.version = version;
		this.dtDispo = dtDispo;
		this.formateur = formateur;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Date getDtDispo() {
		return dtDispo;
	}

	public void setDtDispo(Date dtDispo) {
		this.dtDispo = dtDispo;
	}

	public Formateur getFormateur() {
		return formateur;
	}

	public void setFormateur(Formateur formateur) {
		this.formateur = formateur;
	}
	
	

}
