package projet.factory.model;

public enum Resolution {
	
	Basse("480"), Moyenne("720p"), Haute("1080p"), UltraHaute("4K");
	
	private final String label;

	private Resolution(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

}
