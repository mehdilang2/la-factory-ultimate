package projet.factory.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonView;


@Entity
@Table(name="module")
public class Module {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonView(Views.ViewCommon.class)
	private Long id;
	@Version
	@JsonView(Views.ViewCommon.class)
	private int version;
	@Temporal(TemporalType.DATE)
	@JsonView(Views.ViewCommon.class)
	private Date dtDebut;
	@Temporal(TemporalType.DATE)
	@JsonView(Views.ViewCommon.class)
	private Date dtFin;
	@OneToOne
	@JoinColumn(name = "matiere_id")
	@JsonView({Views.ViewModuleWithMatiere.class, Views.ViewFormationWithMatiere.class,Views.ViewModule.class,Views.ViewFormationWithModule.class,Views.ViewFormationWithModule.class})
	private Matiere matiere;
	@ManyToOne
	@JoinColumn(name = "formateur_id")
	@JsonView({Views.ViewModuleWithFormateur.class,Views.ViewModule.class,Views.ViewFormationWithModule.class, Views.ViewFormateur.class } )
	private Formateur formateur;
	@ManyToOne
	@JoinColumn(name = "formation_id")
	@JsonView({Views.ViewModuleWithFormation.class,Views.ViewModule.class} )
	private Formation formation;
	
	
	

	public Module() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	public Module(Long id, int version, Date dtDebut, Date dtFin, Matiere matiere, Formateur formateur,
			Formation formation) {
		super();
		this.id = id;
		this.version = version;
		this.dtDebut = dtDebut;
		this.dtFin = dtFin;
		this.matiere = matiere;
		this.formateur = formateur;
		this.formation = formation;
	}



	public Formation getFormation() {
		return formation;
	}

	public void setFormation(Formation formation) {
		this.formation = formation;
	}

	public Matiere getMatiere() {
		return matiere;
	}

	public void setMatiere(Matiere matiere) {
		this.matiere = matiere;
	}

	public Formateur getFormateur() {
		return formateur;
	}

	public void setFormateur(Formateur formateur) {
		this.formateur = formateur;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDtDebut() {
		return dtDebut;
	}

	public void setDtDebut(Date dtDebut) {
		this.dtDebut = dtDebut;
	}

	public Date getDtFin() {
		return dtFin;
	}

	public void setDtFin(Date dtFin) {
		this.dtFin = dtFin;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
	
	
	

}
