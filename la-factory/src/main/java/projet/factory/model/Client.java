package projet.factory.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonView;


@Entity
@Table(name = "Client")
public class Client {

	@Id
	@GeneratedValue
	@JsonView(Views.ViewCommon.class)
	private long id;
	@Version
	@JsonView(Views.ViewCommon.class)
	private int version;
	@Column(name = "NOM_ENTREPRISE", length = 50)
	@JsonView({Views.ViewCommon.class, Views.ViewClientWithContacts.class})
	private String nomEntreprise;
	@Column(name = "NOM_BusinessUnit", length = 50)
	@JsonView(Views.ViewCommon.class)
	private String nomBusinessUnit;
	@Column(name = "NUMERO_SIRET", length = 50)
	@JsonView(Views.ViewCommon.class)
	private String numeroSiret;
	@Enumerated(EnumType.STRING)
	@JsonView(Views.ViewCommon.class)
	private TypeEntreprise typeEntreprise;
	@Column(name = "NUMERO_TVA", length = 50)
	@JsonView(Views.ViewCommon.class)
	private String numTva;
	@Embedded
	@JsonView(Views.ViewCommon.class)
	private Adresse adresse;
	@OneToMany(mappedBy = "client")
	@JsonView({Views.ViewClientWithContacts.class,Views.ViewClient.class})
	private List<Contact> contacts = new ArrayList<Contact>();
	@OneToMany(mappedBy = "client")
	@JsonView({Views.ViewClientWithStagiaires.class, Views.ViewFormationWithClient.class})
	private List<Stagiaire> stagiaires = new ArrayList<Stagiaire>();
	@OneToMany(mappedBy = "client")
	@JsonView(Views.ViewFormationWithClient.class)
	private List<ClientFormation> clientFormations = new ArrayList<ClientFormation>();
	
	public Client() {
		super();
	}
	
	

	public Client(long id, int version, String nomEntreprise, String nomBusinessUnit, String numeroSiret,
			TypeEntreprise typeEntreprise, String numTva, Adresse adresse) {
		super();
		this.id = id;
		this.version = version;
		this.nomEntreprise = nomEntreprise;
		this.nomBusinessUnit = nomBusinessUnit;
		this.numeroSiret = numeroSiret;
		this.typeEntreprise = typeEntreprise;
		this.numTva = numTva;
		this.adresse = adresse;
	}



	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNomEntreprise() {
		return nomEntreprise;
	}

	public void setNomEntreprise(String nomEntreprise) {
		this.nomEntreprise = nomEntreprise;
	}

	public String getNumeroSiret() {
		return numeroSiret;
	}

	public void setNumeroSiret(String numeroSiret) {
		this.numeroSiret = numeroSiret;
	}

	public TypeEntreprise getTypeEntreprise() {
		return typeEntreprise;
	}

	public void setTypeEntreprise(TypeEntreprise typeEntreprise) {
		this.typeEntreprise = typeEntreprise;
	}

	public String getNumTva() {
		return numTva;
	}

	public void setNumTva(String numTva) {
		this.numTva = numTva;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public List<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}

	public List<Stagiaire> getStagiaires() {
		return stagiaires;
	}

	public void setStagiaires(List<Stagiaire> stagiaires) {
		this.stagiaires = stagiaires;
	}

	public List<ClientFormation> getClientFormations() {
		return clientFormations;
	}

	public void setClientFormations(List<ClientFormation> clientFormations) {
		this.clientFormations = clientFormations;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getNomBusinessUnit() {
		return nomBusinessUnit;
	}

	public void setNomBusinessUnit(String nomBusinessUnit) {
		this.nomBusinessUnit = nomBusinessUnit;
	}
	
	

}
