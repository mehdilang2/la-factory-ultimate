package projet.factory.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonView;

@Entity
public class Utilisateur {
	@Id
	@GeneratedValue
	@JsonView(Views.ViewCommon.class)
	private Long id;
	@Version
	@JsonView(Views.ViewCommon.class)
	private int version;
	@Enumerated(EnumType.STRING)
	@JsonView(Views.ViewCommon.class)
	private TypeUser typeUser;
	@Column(length = 100)
	@JsonView(Views.ViewCommon.class)
	private String motDePasse;
	@Column(length = 100)
	@JsonView(Views.ViewCommon.class)
	private String mail;
	@OneToOne(mappedBy = "utilisateur")
	@JsonView(Views.ViewUtilisateurWithPersonne.class)
	private Personne personne;

	public Utilisateur() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	public Utilisateur(Long id, int version, TypeUser typeUser, String motDePasse, String mail, Personne personne) {
		super();
		this.id = id;
		this.version = version;
		this.typeUser = typeUser;
		this.motDePasse = motDePasse;
		this.mail = mail;
		this.personne = personne;
	}



	public TypeUser getTypeUser() {
		return typeUser;
	}

	public void setTypeUser(TypeUser typeUser) {
		this.typeUser = typeUser;
	}

	public String getMotDePasse() {
		return motDePasse;
	}

	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public Personne getPersonne() {
		return personne;
	}

	public void setPersonne(Personne personne) {
		this.personne = personne;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	

}
