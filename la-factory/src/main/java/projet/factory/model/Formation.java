package projet.factory.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;

import projet.factory.model.Views.ViewFormationWithFormateurReferent;
import projet.factory.model.Views.ViewStagiaireWithFormation;
import projet.factory.model.Views.ViewFormationWithoutStagiaire;

@Entity
@Table(name="Formation")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Formation {
	
	@Id
	@GeneratedValue
	@JsonView(Views.ViewCommon.class)
	private Long id;
	
	@Version
	@JsonView(Views.ViewFormation.class)
	private int version;

	@Column(name = "date_debut", length = 100)
	@Temporal(TemporalType.DATE)
	@JsonView(Views.ViewCommon.class)
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date dtDebut;

	@Column(name = "date_fin", length = 100)
	@Temporal(TemporalType.DATE)
	@JsonView(Views.ViewCommon.class)
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date dtFin;	

	@JsonView({Views.ViewCommon.class,Views.ViewOrdinateur.class})
	private String intitule;
	
	@JsonView(Views.ViewCommon.class)
	private Integer capacite;
	
	@JsonView(Views.ViewCommon.class)
	private String ville;
	
	@OneToMany(mappedBy = "formation")
	@JsonView({ Views.ViewFormationWithOrdinateur.class, Views.ViewFormationWithSalle.class, Views.ViewFormationWithVideoprojecteur.class,Views.ViewFormation.class})
	private List<MaterielFormation> materielFormation = new ArrayList<MaterielFormation>();
	
	@OneToMany(mappedBy = "formation")
	@JsonView({Views.ViewOrdinateur.class,Views.ViewFormationWithStagiaire.class, Views.ViewFormationWithoutStagiaire.class,Views.ViewFormation.class})
	private List<StagiaireFormation> stagiairesFormations = new ArrayList<StagiaireFormation>();

	@OneToMany(mappedBy = "formation")
	private List<ClientFormation> clientFormations = new ArrayList<ClientFormation>();
	
	@OneToMany(mappedBy = "formation")
	@JsonView({Views.ViewFormationWithModule.class,Views.ViewFormationWithMatiere.class })
	private List<Module> modules = new ArrayList<Module>();
	
	@ManyToOne
	@JoinColumn(name="gestionnaire")
	@JsonView({Views.ViewClientWithGestionnaire.class, Views.ViewFormationWithGestionnaire.class, Views.ViewFormation.class})
	private Gestionnaire gestionnaire;
	
	@ManyToOne
	@JoinColumn(name="formateurReferent")
	@JsonView({Views.ViewFormationWithFormateurReferent.class,Views.ViewFormation.class,Views.ViewFormationWithGestionnaire.class,Views.ViewFormateurWithFormation.class})
	private Formateur formateurReferent;
	
	
	public Formation() {
		super();
	}

	
	
	public Formation(Long id, int version, Date dtDebut, Date dtFin, String intitule, Integer capacite, String ville,
			Gestionnaire gestionnaire, Formateur formateurReferent) {
		super();
		this.id = id;
		this.version = version;
		this.dtDebut = dtDebut;
		this.dtFin = dtFin;
		this.intitule = intitule;
		this.capacite = capacite;
		this.ville = ville;
		this.gestionnaire = gestionnaire;
		this.formateurReferent = formateurReferent;
	}



	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getDtDebut() {
		return dtDebut;
	}
	public void setDtDebut(Date dtDebut) {
		this.dtDebut = dtDebut;
	}
	public Date getDtFin() {
		return dtFin;
	}
	public void setDtFin(Date dtFin) {
		this.dtFin = dtFin;
	}
	public String getIntitule() {
		return intitule;
	}
	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}
	public Integer getCapacite() {
		return capacite;
	}
	public void setCapacite(Integer capacite) {
		this.capacite = capacite;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public List<MaterielFormation> getMaterielFormation() {
		return materielFormation;
	}
	public void setMaterielFormation(List<MaterielFormation> materielFormation) {
		this.materielFormation = materielFormation;
	}
	
	public List<ClientFormation> getClientFormations() {
		return clientFormations;
	}
	public void setClientFormations(List<ClientFormation> clientFormations) {
		this.clientFormations = clientFormations;
	}
	public List<Module> getModules() {
		return modules;
	}
	public void setModules(List<Module> modules) {
		this.modules = modules;
	}
	public Gestionnaire getGestionnaire() {
		return gestionnaire;
	}
	public void setGestionnaire(Gestionnaire gestionnaire) {
		this.gestionnaire = gestionnaire;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public List<StagiaireFormation> getStagiairesFormations() {
		return stagiairesFormations;
	}
	public void setStagiairesFormations(List<StagiaireFormation> stagiairesFormations) {
		this.stagiairesFormations = stagiairesFormations;
	}
	
	
	public Formateur getFormateurReferent() {
		return formateurReferent;
	}
	public void setFormateurReferent(Formateur formateurReferent) {
		this.formateurReferent = formateurReferent;
	}
}
