package projet.factory.model;

import java.util.Date;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@DiscriminatorValue("Ordinateur")
public class Ordinateur extends Materiel{
	@JsonView(Views.ViewCommon.class)
	private String processeur;
	@JsonView(Views.ViewCommon.class)
	private int ram;
	@JsonView(Views.ViewCommon.class)
	private int quantiteDD;
	@JsonView(Views.ViewCommon.class)
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date dtAchat;
	@OneToMany(mappedBy = "ordinateur")
	private  List<Stagiaire> stagiaire;
	
	
	
	public Ordinateur() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public Ordinateur(String processeur, int ram, int quantiteDD, Date dtAchat) {
		super();
		this.processeur = processeur;
		this.ram = ram;
		this.quantiteDD = quantiteDD;
		this.dtAchat = dtAchat;
	}


	public String getProcesseur() {
		return processeur;
	}
	public void setProcesseur(String processeur) {
		this.processeur = processeur;
	}
	public int getRam() {
		return ram;
	}
	public void setRam(int ram) {
		this.ram = ram;
	}
	public int getQuantiteDD() {
		return quantiteDD;
	}
	public void setQuantiteDD(int quantiteDD) {
		this.quantiteDD = quantiteDD;
	}
	public Date getDtAchat() {
		return dtAchat;
	}
	public void setDtAchat(Date dtAchat) {
		this.dtAchat = dtAchat;
	}
	public List<Stagiaire> getStagiaire() {
		return stagiaire;
	}
	public void setStagiaire(List<Stagiaire> stagiaire) {
		this.stagiaire = stagiaire;
	}
	
	
	

}
