package projet.factory.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonView;

@Entity
@DiscriminatorValue("Contact")
public class Contact extends Personne {
	@ManyToOne
	@JoinColumn(name = "id_client")
	@JsonView(Views.ViewContact.class)
	private Client client;

	public Contact() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Contact(Client client) {
		super();
		this.client = client;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

}
