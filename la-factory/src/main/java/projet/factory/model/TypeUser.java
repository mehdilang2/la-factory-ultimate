package projet.factory.model;

public enum TypeUser {
	
	Administrateur, Gestionnaire, Formateur, FormateurExt,  Client, Stagiaire, Contact;
	
}
