package projet.factory.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "presence")
public class Presence {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonView(Views.ViewCommon.class)
	private Long id;
	
	@Version
	@JsonView(Views.ViewCommon.class)
	private int version;
	@Temporal(TemporalType.DATE)
	@JsonView(Views.ViewCommon.class)
	private Date date; 
	
	@JsonView(Views.ViewCommon.class)
	private boolean presenceMatin;
	@JsonView(Views.ViewCommon.class)
	private  boolean presenceApresMidi;
	private  boolean ouvertureSignature;
	@ManyToOne
	@JoinColumn(name="id_stagiaire")
	private Stagiaire stagiaire;

	
	
	public Presence() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	public Presence(Long id, int version, Date date, boolean presenceMatin, boolean presenceApresMidi,
			boolean ouvertureSignature, Stagiaire stagiaire) {
		super();
		this.id = id;
		this.version = version;
		this.date = date;
		this.presenceMatin = presenceMatin;
		this.presenceApresMidi = presenceApresMidi;
		this.ouvertureSignature = ouvertureSignature;
		this.stagiaire = stagiaire;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public boolean isPresenceMatin() {
		return presenceMatin;
	}

	public void setPresenceMatin(boolean presenceMatin) {
		this.presenceMatin = presenceMatin;
	}

	public boolean isPresenceApresMidi() {
		return presenceApresMidi;
	}

	public void setPresenceApresMidi(boolean presenceApresMidi) {
		this.presenceApresMidi = presenceApresMidi;
	}

	public Stagiaire getStagiaire() {
		return stagiaire;
	}

	public void setStagiaire(Stagiaire stagiaire) {
		this.stagiaire = stagiaire;
	}

	public boolean isOuvertureSignature() {
		return ouvertureSignature;
	}

	public void setOuvertureSignature(boolean ouvertureSignature) {
		this.ouvertureSignature = ouvertureSignature;
	}
	
}
