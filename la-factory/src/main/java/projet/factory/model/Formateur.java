package projet.factory.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.UniqueConstraint;

import org.springframework.web.bind.annotation.GetMapping;

import com.fasterxml.jackson.annotation.JsonView;


@Entity
public class Formateur extends Personne {
	
	@OneToMany(mappedBy = "formateur")
	private List<Module> modules = new ArrayList<Module>();
	
	@JsonView({Views.ViewFormateurWithMatiere.class,Views.ViewFormateur.class, Views.ViewFormateurWithoutMatiere.class})
	@ManyToMany
	@JoinTable(name = "formateur_matiere", uniqueConstraints = @UniqueConstraint(columnNames = { "formateur_id",
			"matiere_id" }), joinColumns = @JoinColumn(name = "formateur_id"), inverseJoinColumns = @JoinColumn(name = "matiere_id"))
	private List<Matiere> matieres = new ArrayList<Matiere>();
	@JsonView(Views.ViewFormateurWithDisponibilite.class)
	@OneToMany(mappedBy = "formateur")
	private List<Disponibilite> disponibilites =new ArrayList<Disponibilite>();
	@JsonView(Views.ViewFormateur.class)
	@OneToMany(mappedBy = "formateurReferent")
	private List<Formation> formations = new ArrayList<Formation>();

	public Formateur() {
		super();
	}
	
	

	public List<Module> getModules() {
		return modules;
	}

	public void setModules(List<Module> modules) {
		this.modules = modules;
	}

	public List<Matiere> getMatieres() {
		return matieres;
	}

	public void setMatieres(List<Matiere> matieres) {
		this.matieres = matieres;
	}

	public List<Disponibilite> getDisponibilite() {
		return disponibilites;
	}

	public void setDisponibilite(List<Disponibilite> disponibilite) {
		this.disponibilites = disponibilite;
	}

	@GetMapping("/civilites")
	public Civilite[] civilites() {
		return Civilite.values();
	}

	public List<Formation> getFormations() {
		return formations;
	}

	public void setFormations(List<Formation> formations) {
		this.formations = formations;
	}

}
