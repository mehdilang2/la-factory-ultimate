package projet.factory.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "Type_materiel")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type", visible = true)
@JsonSubTypes({ @Type(value = Videoprojecteur.class, name = "Videoprojecteur"),
		@Type(value = Ordinateur.class, name = "Ordinateur"), @Type(value = Salle.class, name = "Salle") })
public abstract class Materiel {

	@Id
	@GeneratedValue
	@JsonView(Views.ViewCommon.class)
	private Long id;

	@Version
	@JsonView(Views.ViewCommon.class)
	private int version;
	@JsonView(Views.ViewCommon.class)
	private Long code;
	@JsonView(Views.ViewCommon.class)
	private Float coutUtilisation;
	@JsonView({Views.ViewOrdinateur.class,Views.ViewVideoProjecteur.class,Views.ViewSalle.class})
	@OneToMany(mappedBy = "materiel")
	private List<MaterielFormation> materielFormation = new ArrayList<MaterielFormation>();

	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	public Float getCoutUtilisation() {
		return coutUtilisation;
	}

	public void setCoutUtilisation(Float coutUtilisation) {
		this.coutUtilisation = coutUtilisation;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public List<MaterielFormation> getMaterielFormation() {
		return materielFormation;
	}

	public void setMaterielFormation(List<MaterielFormation> materielFormation) {
		this.materielFormation = materielFormation;
	}

}
