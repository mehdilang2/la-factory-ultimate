package projet.factory.model;


import java.io.Serializable;

import javax.persistence.Embeddable;

import com.fasterxml.jackson.annotation.JsonView;


	@Embeddable
	public class Adresse implements Serializable{
		
		@JsonView(Views.ViewCommon.class)
		private String voie;
		@JsonView(Views.ViewCommon.class)
		private String complement;
		@JsonView(Views.ViewCommon.class)
		private String codePostal;
		@JsonView(Views.ViewCommon.class)
		private String ville;
		@JsonView(Views.ViewCommon.class)
		private String pays;

		public Adresse() {
			super();
		}

		public Adresse(String voie, String complement, String codePostal, String ville, String pays) {
			super();
			this.voie = voie;
			this.complement = complement;
			this.codePostal = codePostal;
			this.ville = ville;
			this.pays = pays;
		}

		public String getVoie() {
			return voie;
		}

		public void setVoie(String voie) {
			this.voie = voie;
		}

		public String getComplement() {
			return complement;
		}

		public void setComplement(String complement) {
			this.complement = complement;
		}

		public String getCodePostal() {
			return codePostal;
		}

		public void setCodePostal(String codePostal) {
			this.codePostal = codePostal;
		}

		public String getVille() {
			return ville;
		}

		public void setVille(String ville) {
			this.ville = ville;
		}

		public String getPays() {
			return pays;
		}

		public void setPays(String pays) {
			this.pays = pays;
		}
}
