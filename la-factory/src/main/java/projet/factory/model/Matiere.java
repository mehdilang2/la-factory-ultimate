package projet.factory.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "matiere")
public class Matiere {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonView(Views.ViewCommon.class)
	private Long id;
	
	@Version
	@JsonView(Views.ViewCommon.class)
	private int version;
	
	@Column(name = "titre", length = 100)
	@JsonView(Views.ViewCommon.class)
	private String titre;
	
	@JsonView(Views.ViewCommon.class)
	@Column(name = "duree")
	private Integer duree;
	
	@Column(name = "objectifs", length = 1000)
	@JsonView(Views.ViewCommon.class)
	private String objectifs;
	
	@Column(name = "prerequis", length = 1000)
	@JsonView(Views.ViewCommon.class)
	private String prerequis;
	
	@Column(name = "contenu", length = 1000)
	@JsonView(Views.ViewCommon.class)
	private String contenu;
	
	@OneToOne(mappedBy = "matiere")
	@JsonView(Views.ViewMatiereWithModule.class)
	private Module module;
	
	@ManyToMany(mappedBy = "matieres")
	@JsonView({Views.ViewMatiereWithFormateur.class, Views.ViewFormationWithMatiere.class,Views.ViewMatiere.class})
	private List<Formateur> formateurs = new ArrayList<Formateur>();

	public Matiere() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	public Matiere(Long id, int version, String titre, Integer duree, String objectifs, String prerequis,
			String contenu, Module module) {
		super();
		this.id = id;
		this.version = version;
		this.titre = titre;
		this.duree = duree;
		this.objectifs = objectifs;
		this.prerequis = prerequis;
		this.contenu = contenu;
		this.module = module;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public Integer getDuree() {
		return duree;
	}

	public void setDuree(Integer duree) {
		this.duree = duree;
	}

	public String getObjectifs() {
		return objectifs;
	}

	public void setObjectifs(String objectifs) {
		this.objectifs = objectifs;
	}

	public String getPrerequis() {
		return prerequis;
	}

	public void setPrerequis(String prerequis) {
		this.prerequis = prerequis;
	}

	public String getContenu() {
		return contenu;
	}

	public void setContenu(String contenu) {
		this.contenu = contenu;
	}

	public Module getModule() {
		return module;
	}

	public void setModule(Module module) {
		this.module = module;
	}

	public List<Formateur> getFormateurs() {
		return formateurs;
	}

	public void setFormateurs(List<Formateur> formateurs) {
		this.formateurs = formateurs;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}
