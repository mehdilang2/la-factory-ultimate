package projet.factory.model;


import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonView;


@Entity
@DiscriminatorValue("Salle")
public class Salle extends Materiel {
	
	@JsonView(Views.ViewCommon.class)
	private int placeMax;
	@Embedded
	@JsonView(Views.ViewCommon.class)
	private Adresse adresse;
	
	
	
	public Salle() {
		super();
	}
	
	
	
	public Salle(int placeMax, Adresse adresse) {
		super();
		this.placeMax = placeMax;
		this.adresse = adresse;
	}



	public int getPlaceMax() {
		return placeMax;
	}

	public void setPlaceMax(int placeMax) {
		this.placeMax = placeMax;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}
	
	
	

}
