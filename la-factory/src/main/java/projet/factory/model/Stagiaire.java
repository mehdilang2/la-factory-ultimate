package projet.factory.model;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonView;

@Entity
public class Stagiaire extends Personne {
	@Column(length = 1000)
	@JsonView(Views.ViewCommon.class)
	private String commentaires;
	
	@ManyToOne
	@JoinColumn(name = "id_client")
	@JsonView(Views.ViewStagiaire.class)
	private Client client;
	
	@OneToMany(mappedBy = "stagiaire")
	@JsonView({Views.ViewStagiaire.class,Views.ViewStagiaireWithFormation.class})
	private List<StagiaireFormation> stagiaireFormations=new ArrayList<StagiaireFormation>();
	
	@ManyToOne
	@JoinColumn(name="id_ordinateur")
	@JsonView(Views.ViewCommon.class)
	private Ordinateur ordinateur;
	
	@OneToMany(mappedBy = "stagiaire")
	@JsonView({Views.ViewFormateurWithStagiairePresence.class,Views.ViewStagiairewithpresence.class })
	private List<Presence> presences= new ArrayList<Presence>();
	
	public Stagiaire() {
		super();
	}
	
	

	public Stagiaire(String commentaires, Client client, Ordinateur ordinateur) {
		super();
		this.commentaires = commentaires;
		this.client = client;
		this.ordinateur = ordinateur;
	}



	public String getCommentaires() {
		return commentaires;
	}

	public void setCommentaires(String commentaires) {
		this.commentaires = commentaires;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	

	public Ordinateur getOrdinateur() {
		return ordinateur;
	}

	public void setOrdinateur(Ordinateur ordinateur) {
		this.ordinateur = ordinateur;
	}

	public List<Presence> getPresences() {
		return presences;
	}

	public void setPresences(List<Presence> presences) {
		this.presences = presences;
	}

	public List<StagiaireFormation> getStagiaireFormations() {
		return stagiaireFormations;
	}

	public void setStagiaireFormation(List<StagiaireFormation> stagiaireFormations) {
		this.stagiaireFormations = stagiaireFormations;
	}
	
	

	

}
