package projet.factory.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import projet.factory.model.Civilite;
import projet.factory.model.Gestionnaire;
import projet.factory.model.Views;
import projet.factory.repository.IPersonneRepository;
import projet.factory.repository.IUtilisateurRepository;

@RestController
@RequestMapping("/api/gestionnaire")
@CrossOrigin("*")
public class GestionnaireRestController {

	@Autowired
	private IPersonneRepository personneRepo;
	@Autowired
	private IUtilisateurRepository utilisateurRepo;

	@GetMapping("")
	@JsonView(Views.ViewGestionnaire.class)
	public List<Gestionnaire> list() {
		return personneRepo.findAllGestionnaire();
	}

	@GetMapping("/{id}")
	@JsonView(Views.ViewGestionnaire.class)
	public Gestionnaire find(@PathVariable Long id) {
		return (Gestionnaire) personneRepo.findById(id).get();
	}
	
	
	@GetMapping("/{id}/formations")
	@JsonView(Views.ViewGestionnaireWithFormations.class)
	public Gestionnaire findGestionnaireWithFormation(@PathVariable Long id) {
		return personneRepo.findGestionnaireWithFormations(id);
	}

	@PostMapping("")
	@JsonView(Views.ViewGestionnaire.class)
	public void create(@RequestBody Gestionnaire gestionnaire) {
		utilisateurRepo.save(gestionnaire.getUtilisateur());
		personneRepo.save(gestionnaire);
	}

	@PutMapping("/{id}")
	@JsonView(Views.ViewGestionnaire.class)
	public void update(@PathVariable Long id, @RequestBody Gestionnaire gestionnaire) {
		utilisateurRepo.save(gestionnaire.getUtilisateur());
		personneRepo.save(gestionnaire);
	}

	@DeleteMapping("/{id}")
	public void remove(@PathVariable Long id) {
		Long idUtilisateur = personneRepo.findById(id).get().getUtilisateur().getId();
		personneRepo.deleteById(id);
		utilisateurRepo.deleteById(idUtilisateur);
	}
	
	@GetMapping("/civilites")
	public Civilite[] civilites() {
		return Civilite.values();
	}
	
	

}
