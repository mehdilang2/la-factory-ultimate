package projet.factory.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import projet.factory.model.Civilite;
import projet.factory.model.Disponibilite;
import projet.factory.model.Formateur;
import projet.factory.model.Formation;
import projet.factory.model.Matiere;
import projet.factory.model.Stagiaire;
import projet.factory.model.Views;
import projet.factory.repository.IFormationRepository;
import projet.factory.repository.IMatiereRepository;
import projet.factory.repository.IPersonneRepository;
import projet.factory.repository.IUtilisateurRepository;


@RestController
@RequestMapping("/api/formateur")
@CrossOrigin("*")
public class FormateurRestController {

	@Autowired
	private IPersonneRepository personneRepo;
	@Autowired
	private IMatiereRepository matiereRepo;
	@Autowired
	private IUtilisateurRepository utilisateurRepo;
	@Autowired
	private IFormationRepository formationRepo;

	@GetMapping("")
	@JsonView(Views.ViewFormateur.class)
	public List<Formateur> list() {
		return personneRepo.findAllFormateur();
	}

	@GetMapping("/{id}")
	@JsonView(Views.ViewFormateur.class)
	public Formateur find(@PathVariable Long id) {
		return (Formateur) personneRepo.findById(id).get();
	}
	

	@GetMapping("/{id}/presence")
	@JsonView(Views.ViewFormateurWithStagiairePresence.class)
	public List<Stagiaire> findStagiairesPresence(@PathVariable Long id) {
	return personneRepo.findStagiairesPresenceByFormateur(id);
	}
	

	@GetMapping("/{id}/disponibilite")
	@JsonView(Views.ViewFormateurWithDisponibilite.class)
	public List<Disponibilite> findDisponibilite(@PathVariable Long id) {
		return personneRepo.findDisponibiliteByformateur(id);
	}

	
	@GetMapping("/{id}/matieres")
    @JsonView(Views.ViewFormateurWithMatiere.class)
    public List<Matiere> findMatiere(@PathVariable Long id) {
        return matiereRepo.findAllMatiereByFormateurId(id);
        
    }
	
	@GetMapping("/{id}/formations")
    @JsonView(Views.ViewFormateurWithFormation.class)
    public List<Formation> findFormationByIdFormateur(@PathVariable Long id) {
        return formationRepo.findFormationByIdFormateur(id);
       
    }
	
	@GetMapping("/{id}/matieresAll")
    @JsonView(Views.ViewFormateurWithoutMatiere.class)
    public List<Matiere> findMatiereAll(@PathVariable Long id) {
        return matiereRepo.findAllMatiereWithoutFormateurId(id);
        
    }

	@PostMapping("")
	@JsonView(Views.ViewFormateur.class)
	public void create(@RequestBody Formateur formateur) {
		utilisateurRepo.save(formateur.getUtilisateur());
		personneRepo.save(formateur);
	}

	@PutMapping("/{id}")
	@JsonView(Views.ViewFormateur.class)
	public void update(@PathVariable Long id, @RequestBody Formateur formateur) {
		utilisateurRepo.save(formateur.getUtilisateur());
		personneRepo.save(formateur);
	}

	@PutMapping("/{id}/matieres")
	@JsonView(Views.ViewFormateurWithMatiere.class)
	public void updateFormateur(@PathVariable Long id, @RequestBody Formateur formateur) {
		personneRepo.save(formateur);
	}

	@DeleteMapping("/{id}")
	public void remove(@PathVariable Long id) {
		Long idUtilisateur = personneRepo.findById(id).get().getUtilisateur().getId();
		personneRepo.deleteById(id);
		utilisateurRepo.deleteById(idUtilisateur);
	}
	
	@GetMapping("/civilites")
	public Civilite[] civilites() {
		return Civilite.values();
	}

}

