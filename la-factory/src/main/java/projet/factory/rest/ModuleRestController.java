package projet.factory.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import projet.factory.model.Module;
import projet.factory.model.Views;
import projet.factory.repository.IModuleRepository;


@RestController
@RequestMapping("/api/module")
@CrossOrigin("*")
public class ModuleRestController {

	@Autowired
	private IModuleRepository moduleRepo;

	@GetMapping("")
	@JsonView(Views.ViewModule.class)
	public List<Module> list() {
		return moduleRepo.findAll();
	}

	@GetMapping("/{id}")
	@JsonView(Views.ViewModule.class)
	public Module find(@PathVariable Long id) {
		return moduleRepo.findById(id).get();
	}
	
	@GetMapping("/{id}/matieres")
	@JsonView(Views.ViewModuleWithMatiere.class)
	public Module findModuleWithMatieres(@PathVariable Long id) {
		return moduleRepo.findById(id).get();
	}

	@PostMapping("")
	@JsonView(Views.ViewModule.class)
	public void create(@RequestBody Module module) {
		moduleRepo.save(module);
	}

	@PutMapping("/{id}")
	@JsonView(Views.ViewModule.class)
	public void update(@PathVariable Long id, @RequestBody Module module) {
		moduleRepo.save(module);
	}


	@DeleteMapping("/{id}")
	public void remove(@PathVariable Long id) {
		moduleRepo.deleteById(id);
	}
	
	

}

