	package projet.factory.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import projet.factory.model.Client;
import projet.factory.model.Contact;
import projet.factory.model.Formation;
import projet.factory.model.Gestionnaire;
import projet.factory.model.Stagiaire;
import projet.factory.model.TypeEntreprise;
import projet.factory.model.Views;
import projet.factory.repository.IClientFormationRepository;
import projet.factory.repository.IClientRepository;
import projet.factory.repository.IFormationRepository;
import projet.factory.repository.IPersonneRepository;
import projet.factory.repository.IUtilisateurRepository;

@RestController
@RequestMapping("/api/client")
@CrossOrigin("*")
public class ClientRestController {

	@Autowired
	private IClientRepository clientRepo;
	
	@Autowired
	private IPersonneRepository personneRepo;
	
	@Autowired
	private IFormationRepository formationRepo;
	
	@Autowired
	private IUtilisateurRepository utilisateurRepo;
	
	@Autowired
	private IClientFormationRepository clientFormationRepo;

	@GetMapping("")
	@JsonView(Views.ViewClient.class)
	public List<Client> list() {
		return clientRepo.findAll();
	}
	
	@GetMapping("/{id}")
	@JsonView(Views.ViewClient.class)
	public Client findById(@PathVariable Long id) {
		return clientRepo.findById(id).get();
	}
	
	@GetMapping("/{id}/stagiaires")
	@JsonView(Views.ViewClientWithStagiaires.class)
	public List<Stagiaire> findStagiaire(@PathVariable Long id) {
		return personneRepo.findStagiaireByIdClient(id);
	}
	
	@GetMapping("/{id}/gestionnaires")
	@JsonView(Views.ViewClientWithGestionnaire.class)
	public List<Gestionnaire> findGestionnaire(@PathVariable Long id) {
		return personneRepo.findGestionnaireByIdClient(id);
	}
	

	@GetMapping("/{id}/formations")
	@JsonView(Views.ViewClientWithFormation.class)
	public List<Formation> listFormation(@PathVariable Long id) {
		return formationRepo.findFormationByIdClient(id);
	}
	
	@GetMapping("/{id}/contact")
	@JsonView(Views.ViewClienttWithContact.class)
	public List<Contact> findClient(@PathVariable Long id) {
	return personneRepo.findContactByIdClient(id);
	}
	
	@PostMapping("")
	@JsonView(Views.ViewClient.class)
	public void create(@RequestBody Client client) {
		clientRepo.save(client);
	}

	@PutMapping("/{id}")
	@JsonView(Views.ViewClient.class)
	public void update(@PathVariable Long id, @RequestBody Client client) {
		clientRepo.save(client);
	}

	@DeleteMapping("/{id}")
	public void remove(@PathVariable Long id) {
		Long idContact = personneRepo.findById(id).get().getId();
		Long idUtilisateur = personneRepo.findById(idContact).get().getUtilisateur().getId();
		Long idClientFormation = clientFormationRepo.findByIdClient(id).getId();
		clientFormationRepo.deleteById(idClientFormation);
		personneRepo.deleteById(idContact);
		utilisateurRepo.deleteById(idUtilisateur);
		clientRepo.deleteById(id);
	}
	
	@GetMapping("/typeentreprise")
	public TypeEntreprise[] typeentreprise() {
		return TypeEntreprise.values();
	}

}
