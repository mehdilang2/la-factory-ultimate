package projet.factory.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import projet.factory.model.Administrateur;
import projet.factory.model.Civilite;
import projet.factory.model.Views;
import projet.factory.repository.IPersonneRepository;
import projet.factory.repository.IUtilisateurRepository;

@RestController
@RequestMapping("/api/administrateur")
@CrossOrigin("*")
public class AdministrateurRestController {

	@Autowired
	private IPersonneRepository personneRepo;
	
	@Autowired
	private IUtilisateurRepository utilisateurRepo;

	@GetMapping("")
	@JsonView(Views.ViewAdministrateur.class)
	public List<Administrateur> list() {
		return personneRepo.findAllAdministrateur();
	}
	

	@GetMapping("/{id}")
	@JsonView(Views.ViewAdministrateur.class)
	public Administrateur find(@PathVariable Long id) {
		return (Administrateur) personneRepo.findById(id).get();
	}

	@PostMapping("")
	@JsonView(Views.ViewAdministrateur.class)
	public void create(@RequestBody Administrateur adminstrateur) {
		utilisateurRepo.save(adminstrateur.getUtilisateur());
		personneRepo.save(adminstrateur);
	}

	@PutMapping("/{id}")
	@JsonView(Views.ViewAdministrateur.class)
	public void update(@PathVariable Long id, @RequestBody Administrateur administrateur) {
		utilisateurRepo.save(administrateur.getUtilisateur());
		personneRepo.save(administrateur);
	}

	@DeleteMapping("/{id}")
	public void remove(@PathVariable Long id) {
		Long idUtilisateur = personneRepo.findById(id).get().getUtilisateur().getId();
		personneRepo.deleteById(id);
		utilisateurRepo.deleteById(idUtilisateur);
	}
	@GetMapping("/civilites")
	public Civilite[] civilites() {
		return Civilite.values();
	}
	

}
