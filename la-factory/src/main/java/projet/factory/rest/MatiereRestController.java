package projet.factory.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import projet.factory.model.Formateur;
import projet.factory.model.Matiere;
import projet.factory.model.Views;
import projet.factory.repository.IMatiereRepository;
import projet.factory.repository.IPersonneRepository;


@RestController
@RequestMapping("/api/matiere")
@CrossOrigin("*")
public class MatiereRestController {

	@Autowired
	private IMatiereRepository matiereRepo;
	@Autowired
	private IPersonneRepository personneRepo;


	@GetMapping("")
	@JsonView(Views.ViewMatiere.class)
	public List<Matiere> list() {
		return matiereRepo.findAll();
	}

	@GetMapping("/{id}")
	@JsonView(Views.ViewMatiere.class)
	public Matiere find(@PathVariable Long id) {
		return matiereRepo.findById(id).get();
	}

	@PostMapping("")
	@JsonView(Views.ViewMatiere.class)
	public void create(@RequestBody Matiere matiere) {
		matiereRepo.save(matiere);
	}

	@PutMapping("/{id}")
	@JsonView(Views.ViewMatiere.class)
	public void update(@PathVariable Long id, @RequestBody Matiere matiere) {
		matiereRepo.save(matiere);
	}


	@DeleteMapping("/{id}")
	public void remove(@PathVariable Long id) {
		matiereRepo.deleteById(id);
	}
	
	@GetMapping("/{id}/formateurs")
	@JsonView(Views.ViewMatiereWithFormateur.class)
	public List<Formateur> findMatiereByFormateur(@PathVariable Long id) {
		return personneRepo.findFormateurByMatiere(id);
	}
	
	
	
}

