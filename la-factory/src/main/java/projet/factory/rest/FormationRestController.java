package projet.factory.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import projet.factory.model.Client;
import projet.factory.model.Formateur;
import projet.factory.model.Formation;
import projet.factory.model.Gestionnaire;
import projet.factory.model.Matiere;
import projet.factory.model.Module;
import projet.factory.model.Ordinateur;
import projet.factory.model.Salle;
import projet.factory.model.Stagiaire;
import projet.factory.model.Videoprojecteur;
import projet.factory.model.Views;
import projet.factory.repository.IClientRepository;
import projet.factory.repository.IFormationRepository;
import projet.factory.repository.IMaterielRepository;
import projet.factory.repository.IMatiereRepository;
import projet.factory.repository.IModuleRepository;
import projet.factory.repository.IPersonneRepository;
import projet.factory.repository.IStagiaireFormationRepository;

@RestController
@RequestMapping("/api/formation")
@CrossOrigin("*")
public class FormationRestController {

	@Autowired
	private IFormationRepository formationRepo;
	
	@Autowired
	private IPersonneRepository personneRepo;
	
	@Autowired
	private IModuleRepository moduleRepo;
	
	@Autowired
	private IMaterielRepository materielRepo;
	
	@Autowired
	private IMatiereRepository matiereRepo;
	
	@Autowired
	private IClientRepository clientRepo;

	@Autowired
	private IStagiaireFormationRepository stagiaireFormationRepo;
	
	@GetMapping("")
	@JsonView(Views.ViewFormation.class)
	public List<Formation> list() {
		return formationRepo.findAll();
	}
	
	@GetMapping("/{id}")
	@JsonView(Views.ViewFormation.class)
	public Formation findFormation(@PathVariable Long id) {
		return formationRepo.findById(id).get();
	}
	
	@GetMapping("/{id}/formateur")
	@JsonView(Views.ViewFormateur.class)
	public List<Formateur> findFormateurByFormation(@PathVariable Long id) {
		return (List<Formateur>)personneRepo.findFormateurByFormation(id);
	}
	
	@GetMapping("/{id}/formateurReferent")
	@JsonView(Views.ViewFormationWithFormateurReferent.class)
	public Formateur findFormateurReferentByFormation(@PathVariable Long id) {
		return (Formateur) personneRepo.findFormateurReferentByFormation(id);
	}
	
	@GetMapping("/{id}/stagiaires")
	@JsonView(Views.ViewFormationWithStagiaire.class)
	public List<Stagiaire> findStagiairesByFormation(@PathVariable Long id) {
		return personneRepo.findStagiairesByFormation(id);
	}
	
	@GetMapping("/{id}/stagiairesAll")
	@JsonView(Views.ViewFormationWithStagiaire.class)
	public List<Stagiaire> findStagiairesWithoutFormation(@PathVariable Long id) {
		return personneRepo.findStagiairesWithoutFormation(id);
	}
	
	@GetMapping("/stagiairesAll")
	@JsonView(Views.ViewStagiaire.class)
	public List<Stagiaire> FindAllStagiaire() {
		return personneRepo.findAllStagiaire();
	}
	
	@GetMapping("/{id}/modules")
	@JsonView(Views.ViewFormationWithModule.class)
	public List<Module> findModulesByFormation(@PathVariable Long id) {
		return moduleRepo.findModulesByFormation(id);
	}
	
	@GetMapping("/{id}/matieres")
	@JsonView(Views.ViewFormationWithMatiere.class)
	public List<Matiere> findMatieresByFormation(@PathVariable Long id) {
		return matiereRepo.findMatieresFormateursByFormation(id);
	}
	
	@GetMapping("/{id}/materiel/ordinateur")
	@JsonView(Views.ViewFormationWithOrdinateur.class)
	public List<Ordinateur> findFormationWithOrdinateur(@PathVariable Long id) {
		return materielRepo.findOrdinateursWithFormation(id);
	}
	
	@GetMapping("/{id}/materiel/salle")
	@JsonView(Views.ViewFormationWithSalle.class)
	public List<Salle> findFormationWithSalle(@PathVariable Long id) {
		return materielRepo.findSallesWithFormation(id);
	}
	
	@GetMapping("/{id}/materiel/videoprojecteur")
	@JsonView(Views.ViewFormationWithVideoprojecteur.class)
	public List<Videoprojecteur> findFormationWithVideoprojecteur(@PathVariable Long id) {
		return materielRepo.findVideoprojecteursWithFormation(id);
	}
	
	@GetMapping("/{id}/clients")
	@JsonView(Views.ViewFormationWithClient.class)
	public List<Client> findFormationWithClient(@PathVariable Long id) {
		return clientRepo.findClientsByFormation(id);
	}

	@PostMapping("")
	@JsonView(Views.ViewFormation.class)
	public void create(@RequestBody Formation formation) {
		formationRepo.save(formation);
	}

	@PutMapping("/{id}")
	@JsonView(Views.ViewFormation.class)
	public void update(@PathVariable Long id, @RequestBody Formation formation) {
		formationRepo.save(formation);
	}
	
	@PutMapping("/{id}/formateurReferent")
	@JsonView(Views.ViewFormation.class)
	public void updateFormateur(@PathVariable Long id, @RequestBody Formation formation) {
		formationRepo.save(formation);
	}
	
	@PutMapping("/{id}/stagiaires")
	@JsonView(Views.ViewFormation.class)
	public void updateStagiaires(@PathVariable Long id, @RequestBody Formation formation) {
		formationRepo.save(formation);
	}

	@DeleteMapping("/{id}")
	public void remove(@PathVariable Long id) {
		formationRepo.deleteById(id);
	}
	
	@DeleteMapping("/{id}/byFormation")
	public void removeByFormation(@PathVariable Long id) {
		stagiaireFormationRepo.deleteStagiaireFormationByFormation(id);
	}
	
	@GetMapping("/{id}/gestionnaire")
	@JsonView(Views.ViewFormationWithGestionnaire.class)
	public Gestionnaire findGestionnaireByFormation(@PathVariable Long id) {
		return (Gestionnaire) personneRepo.findGestionnaireByFormation(id);
	}

}
