package projet.factory.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import projet.factory.model.Administrateur;
import projet.factory.model.TypeUser;
import projet.factory.model.Utilisateur;
import projet.factory.model.Views;
import projet.factory.repository.IPersonneRepository;
import projet.factory.repository.IUtilisateurRepository;


@RestController
@RequestMapping("/api/utilisateur")
@CrossOrigin("*")
public class UtilisateurRestController {

	@Autowired
	private IUtilisateurRepository utilisateurRepo;
	
//	@Autowired
//	private IPersonneRepository personneRepo;
	
	@GetMapping("")
	@JsonView(Views.ViewUtilisateur.class)
	public List<Utilisateur> list() {
		return utilisateurRepo.findAll();
	}

	@GetMapping("/{id}")
	@JsonView(Views.ViewUtilisateur.class)
	public Utilisateur find(@PathVariable Long id) {
		return  utilisateurRepo.findById(id).get();
	}
	
	@GetMapping("/mail/{mail}")
	@JsonView(Views.ViewUtilisateurWithPersonne.class)
	public Utilisateur findByMail(@PathVariable String mail) {
		return utilisateurRepo.findUtilisateursByMail(mail);
	}

	@PostMapping("")
	@JsonView(Views.ViewUtilisateur.class)
	public void create(@RequestBody Utilisateur utilisateur) {
		utilisateurRepo.save(utilisateur);
	}

	@PutMapping("/{id}")
	@JsonView(Views.ViewUtilisateur.class)
	public void update(@PathVariable Long id, @RequestBody Utilisateur utilisateur) {
		utilisateurRepo.save(utilisateur);
	}


	@DeleteMapping("/{id}")
	public void remove(@PathVariable Long id) {
		utilisateurRepo.deleteById(id);
	}
	
	@GetMapping("/typeutilisateur")
	public TypeUser[] typeutilisateur() {
		return TypeUser.values();
	}
	
//	@GetMapping("/administrateurs")
//	public List<Administrateur> findAdministrateurs(){
//		return personneRepo.findAllAdministrateur();
//	}

}

