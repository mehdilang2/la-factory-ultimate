package projet.factory.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import projet.factory.model.Formation;
import projet.factory.model.Presence;
import projet.factory.model.Module;
import projet.factory.model.Presence;
import projet.factory.model.Salle;
import projet.factory.model.Stagiaire;
import projet.factory.model.Views;
import projet.factory.repository.IPresenceRepository;

@RestController
@RequestMapping("/api/presence")
@CrossOrigin("*")
public class PresenceRestController {

	@Autowired
	private IPresenceRepository presenceRepo;
	
	@GetMapping("")
	@JsonView(Views.ViewPresence.class)
	public List<Presence> list() {
		return presenceRepo.findAll();
	}

	@GetMapping("/{id}")
	@JsonView(Views.ViewPresence.class)
	public Presence find(@PathVariable Long id) {
		return presenceRepo.findById(id).get();
	}

	@PostMapping("")
	@JsonView(Views.ViewPresence.class)
	public void create(@RequestBody Presence presence) {
		presenceRepo.save(presence);
	}

	@PutMapping("/{id}")
	@JsonView(Views.ViewPresence.class)
	public void update(@PathVariable Long id, @RequestBody Presence presence) {
		presenceRepo.save(presence);
	}


	@DeleteMapping("/{id}")
	public void remove(@PathVariable Long id) {
		presenceRepo.deleteById(id);
	}
	
	
	
	

}
