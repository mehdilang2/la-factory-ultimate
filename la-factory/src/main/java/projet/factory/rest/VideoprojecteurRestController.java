package projet.factory.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import projet.factory.model.MaterielFormation;
import projet.factory.model.Resolution;
import projet.factory.model.Videoprojecteur;
import projet.factory.model.Views;
import projet.factory.repository.IMaterielFormationRepository;
import projet.factory.repository.IMaterielRepository;


@RestController
@RequestMapping("/api/videoprojecteur")
@CrossOrigin("*")
public class VideoprojecteurRestController {

	@Autowired
	private IMaterielRepository materielRepo;
	@Autowired
	private IMaterielFormationRepository materielFormationRepo;

	@GetMapping("")
	@JsonView(Views.ViewVideoProjecteur.class)
	public List<Videoprojecteur> list() {
		return materielRepo.findAllVideoprojecteur();
	}

	@GetMapping("/{id}")
	@JsonView(Views.ViewVideoProjecteur.class)
	public Videoprojecteur find(@PathVariable Long id) {
		return (Videoprojecteur) materielRepo.findById(id).get();
	}
	
	@GetMapping("/{id}/materielFormation")
	@JsonView(Views.ViewVideoProjecteur.class)
	public List<MaterielFormation> findMaterielFormation(@PathVariable Long id) {
		return materielFormationRepo.findMaterielFormationByVideoProjecteur(id);
	}

	@PostMapping("")
	@JsonView(Views.ViewVideoProjecteur.class)
	public void create(@RequestBody Videoprojecteur videoprojecteur) {
		materielRepo.save(videoprojecteur);
	}

	@PutMapping("/{id}")
	@JsonView(Views.ViewVideoProjecteur.class)
	public void update(@PathVariable Long id, @RequestBody Videoprojecteur videoprojecteur) {
		materielRepo.save(videoprojecteur);
	}


	@DeleteMapping("/{id}")
	public void remove(@PathVariable Long id) {
		materielRepo.deleteById(id);
	}
	
	@GetMapping("/resolution")
	public Resolution[] resolution(){
		return Resolution.values();
	}
	
	

}

