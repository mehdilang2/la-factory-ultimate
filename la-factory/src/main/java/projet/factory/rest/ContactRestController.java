package projet.factory.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import projet.factory.model.Civilite;
import projet.factory.model.Client;
import projet.factory.model.Contact;
import projet.factory.model.Views;
import projet.factory.repository.IClientRepository;
import projet.factory.repository.IPersonneRepository;
import projet.factory.repository.IUtilisateurRepository;

@RestController
@RequestMapping("/api/contact")
@CrossOrigin("*")
public class ContactRestController {
	
	@Autowired
	private IPersonneRepository personneRepo;
	@Autowired
	private IUtilisateurRepository utilisateurRepo;
	@Autowired
	private IClientRepository clientRepo;

	@GetMapping("")
	@JsonView(Views.ViewContact.class)
	public List<Contact> list() {
		return personneRepo.findAllContact();
	}

	@GetMapping("/{id}")
	@JsonView(Views.ViewContact.class)
	public Contact find(@PathVariable Long id) {
		return (Contact) personneRepo.findById(id).get();
	}
	
	@GetMapping("/{id}/client")
	@JsonView(Views.ViewContact.class)
	public Client findClient(@PathVariable Long id) {
		return  clientRepo.findClientByContact(id);
	}
	

	@PostMapping("")
	@JsonView(Views.ViewContact.class)
	public void create(@RequestBody Contact contact) {
		utilisateurRepo.save(contact.getUtilisateur());
		personneRepo.save(contact);
	}

	@PutMapping("/{id}")
	@JsonView(Views.ViewContact.class)
	public void update(@PathVariable Long id, @RequestBody Contact contact) {
		utilisateurRepo.save(contact.getUtilisateur());
		personneRepo.save(contact);
	}


	@DeleteMapping("/{id}")
	public void remove(@PathVariable Long id) {
		Long idUtilisateur = personneRepo.findById(id).get().getUtilisateur().getId();
		personneRepo.deleteById(id);
		utilisateurRepo.deleteById(idUtilisateur);
	}
	
	@GetMapping("/civilites")
	public Civilite[] civilites(){
		return Civilite.values();
	}

}
