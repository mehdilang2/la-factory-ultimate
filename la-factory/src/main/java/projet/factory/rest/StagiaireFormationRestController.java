package projet.factory.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import projet.factory.model.StagiaireFormation;
import projet.factory.model.Views;
import projet.factory.model.StagiaireFormation;
import projet.factory.repository.IMaterielFormationRepository;
import projet.factory.repository.IStagiaireFormationRepository;

@RestController
@RequestMapping("/api/stagiaireFormation")
@CrossOrigin("*")
public class StagiaireFormationRestController {

	@Autowired
	private IStagiaireFormationRepository StagiaireFormationRepo;

	@GetMapping("")
	@JsonView(Views.ViewStagiaireFormation.class)
	public List<StagiaireFormation> list() {
		return StagiaireFormationRepo.findAll();
	}

	@GetMapping("/{id}")
	@JsonView(Views.ViewStagiaireFormation.class)
	public StagiaireFormation find(@PathVariable Long id) {
		return StagiaireFormationRepo.findById(id).get();
	}

	@PostMapping("")
	@JsonView(Views.ViewStagiaireFormation.class)
	public void create(@RequestBody StagiaireFormation stagiareFormation) {
		StagiaireFormationRepo.save(stagiareFormation);
	}

	@PutMapping("/{id}")
	@JsonView(Views.ViewStagiaireFormation.class)
	public void update(@PathVariable Long id, @RequestBody StagiaireFormation stagiaireFormation) {
		StagiaireFormationRepo.save(stagiaireFormation);
	}

	
	
	@DeleteMapping("/{id}")
	public void remove(@PathVariable Long id) {
		StagiaireFormationRepo.deleteById(id);
	}
	
	
	
	
	
	

}
