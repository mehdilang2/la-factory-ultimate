package projet.factory.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import projet.factory.model.ClientFormation;
import projet.factory.model.Views;
import projet.factory.repository.IClientFormationRepository;

@RestController
@RequestMapping("/api/clientFormation")
@CrossOrigin("*")
public class ClientFormationRestController {

	@Autowired
	private IClientFormationRepository clientFormationRepo;

	@GetMapping("")
	@JsonView(Views.ViewClientFormation.class)
	public List<ClientFormation> list() {
		return clientFormationRepo.findAll();
	}	

	@GetMapping("/{id}")
	@JsonView(Views.ViewClientFormation.class)
	public ClientFormation find(@PathVariable Long id) {
		return clientFormationRepo.findById(id).get();
	}

	@PostMapping("")
	@JsonView(Views.ViewClientFormation.class)
	public void create(@RequestBody ClientFormation clientFormation) {
		clientFormationRepo.save(clientFormation);
	}

	@PutMapping("/{id}")
	@JsonView(Views.ViewClientFormation.class)
	public void update(@PathVariable Long id, @RequestBody ClientFormation clientFormation) {
		clientFormationRepo.save(clientFormation);
	}

	@DeleteMapping("/{id}")
	public void remove(@PathVariable Long id) {
		clientFormationRepo.deleteById(id);
	}
	
	

}
