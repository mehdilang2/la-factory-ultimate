package projet.factory.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import projet.factory.model.MaterielFormation;
import projet.factory.model.Ordinateur;
import projet.factory.model.Views;
import projet.factory.repository.IMaterielFormationRepository;
import projet.factory.repository.IMaterielRepository;

@RestController
@RequestMapping("/api/ordinateur")
@CrossOrigin("*")
public class OrdinateurRestController {

	@Autowired
	private IMaterielRepository materielRepo;
	@Autowired
	private IMaterielFormationRepository materielFormationRepo;
	
	@GetMapping("")
	@JsonView(Views.ViewOrdinateur.class)
	public List<Ordinateur> list() {
		return materielRepo.findAllOrdinateursWithFormation();
	}

	@GetMapping("/{id}")
	@JsonView(Views.ViewOrdinateur.class)
	public Ordinateur find(@PathVariable Long id) {
		return (Ordinateur) materielRepo.findById(id).get();
	}

	@GetMapping("/{id}/dates")
	@JsonView(Views.ViewOrdinateurWithDates.class)
	public Ordinateur findOrdinateurByFormation(@PathVariable Long id) {
		return materielRepo.findOrdinateurWithDates(id);
	}
	
	@GetMapping("/{id}/materielFormation")
	@JsonView(Views.ViewOrdinateur.class)
	public List<MaterielFormation> findMaterielFormation(@PathVariable Long id) {
		return materielFormationRepo.findMaterielFormationByVideoProjecteur(id);
	}

	@PostMapping("")
	@JsonView(Views.ViewOrdinateur.class)
	public void create(@RequestBody Ordinateur ordinateur) {
		materielRepo.save(ordinateur);
	}

	@PutMapping("/{id}")
	@JsonView(Views.ViewOrdinateur.class)
	public void update(@PathVariable Long id, @RequestBody Ordinateur ordinateur) {
		materielRepo.save(ordinateur);
	}

	@DeleteMapping("/{id}")
	public void remove(@PathVariable Long id) {
		materielRepo.deleteById(id);
	}

}
