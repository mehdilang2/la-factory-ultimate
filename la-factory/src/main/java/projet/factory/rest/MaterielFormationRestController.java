package projet.factory.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import projet.factory.model.Formation;
import projet.factory.model.MaterielFormation;
import projet.factory.model.Views;
import projet.factory.repository.IFormationRepository;
import projet.factory.repository.IMaterielFormationRepository;

@RestController
@RequestMapping("/api/materielFormation")
@CrossOrigin("*")
public class MaterielFormationRestController {

	@Autowired
	private IMaterielFormationRepository materielFormationRepo;


	@GetMapping("")
	@JsonView(Views.ViewMaterielFormation.class)
	public List<MaterielFormation> list() {
		return materielFormationRepo.findAll();
	}
	

	@GetMapping("/{id}")
	@JsonView(Views.ViewMaterielFormation.class)
	public MaterielFormation find(@PathVariable Long id) {
		return materielFormationRepo.findById(id).get();
	}
	


	@PostMapping("")
	@JsonView(Views.ViewMaterielFormation.class)
	public void create(@RequestBody MaterielFormation materielFormation) {
		
		materielFormationRepo.save(materielFormation);
	}

	@PutMapping("/{id}")
	@JsonView(Views.ViewMaterielFormation.class)
	public void update(@PathVariable Long id, @RequestBody MaterielFormation materielFormation) {
		materielFormationRepo.save(materielFormation);
	}

	@DeleteMapping("/{id}")
	public void remove(@PathVariable Long id) {
		materielFormationRepo.deleteById(id);
	}
	
	

}
