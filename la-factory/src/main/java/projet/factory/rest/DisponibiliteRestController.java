package projet.factory.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import projet.factory.model.Disponibilite;
import projet.factory.model.Views;
import projet.factory.repository.IDisponibiliteRepository;

@RestController
@RequestMapping("/api/disponibilite")
@CrossOrigin("*")
public class DisponibiliteRestController {

	@Autowired
	private IDisponibiliteRepository disponibiliteRepo;
	
	@GetMapping("")
	@JsonView(Views.ViewDisponibilite.class)
	public List<Disponibilite> list() {
		return disponibiliteRepo.findAll();
	}
	
	@GetMapping("/{id}")
	@JsonView(Views.ViewDisponibilite.class)
	public Disponibilite find(@PathVariable Long id) {
		return disponibiliteRepo.findById(id).get();
	}
		
	
	@PostMapping("")
	@JsonView(Views.ViewDisponibilite.class)
	public void create(@RequestBody Disponibilite disponibilite) {
		disponibiliteRepo.save(disponibilite);
	}

	@GetMapping("/{id}/formateur")
	@JsonView(Views.ViewDisponibiliteWithFormateur.class)
	public List<Disponibilite> findDisponibilite(@PathVariable Long id) {
		return disponibiliteRepo.findDisponibiliteByformateur(id);
	}
	
	@PutMapping("/{id}")
	@JsonView(Views.ViewDisponibilite.class)
	public void update(@PathVariable Long id, @RequestBody Disponibilite disponibilite) {
		disponibiliteRepo.save(disponibilite);
	}


	@DeleteMapping("/{id}")
	public void remove(@PathVariable Long id) {
		disponibiliteRepo.deleteById(id);
	}
	
	
	
}
