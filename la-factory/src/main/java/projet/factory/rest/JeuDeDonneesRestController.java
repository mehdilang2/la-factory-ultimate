package projet.factory.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import projet.factory.model.Administrateur;
import projet.factory.model.Adresse;
import projet.factory.model.Civilite;
import projet.factory.model.Client;
import projet.factory.model.ClientFormation;
import projet.factory.model.Contact;
import projet.factory.model.Disponibilite;
import projet.factory.model.Formateur;
import projet.factory.model.Formation;
import projet.factory.model.Gestionnaire;
import projet.factory.model.MaterielFormation;
import projet.factory.model.Matiere;
import projet.factory.model.Module;
import projet.factory.model.Ordinateur;
import projet.factory.model.Resolution;
import projet.factory.model.Salle;
import projet.factory.model.Stagiaire;
import projet.factory.model.StagiaireFormation;
import projet.factory.model.TypeEntreprise;
import projet.factory.model.TypeUser;
import projet.factory.model.Utilisateur;
import projet.factory.model.Videoprojecteur;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class JeuDeDonneesRestController {
	@Autowired
	private ClientFormationRestController clientFormationRest;
	@Autowired
	private ClientRestController clientRest;
	@Autowired
	private DisponibiliteRestController disponibiliteRest;
	@Autowired
	private FormationRestController formationRest;
	@Autowired
	private MaterielFormationRestController materielFormationRest;
	@Autowired
	private MaterielRestController materielRest;
	@Autowired
	private MatiereRestController matiereRest;
	@Autowired
	private ModuleRestController moduleRest;
	@Autowired
	private AdministrateurRestController adminRest;
	@Autowired
	private GestionnaireRestController gestRest;
	@Autowired
	private StagiaireRestController stagiaireRest;
	@Autowired
	private FormateurRestController formateurRest;
	@Autowired
	private ContactRestController contactRest;
	@Autowired
	private PresenceRestController presenceRest;
	@Autowired
	private StagiaireFormationRestController stagiaireFormationRest;
	@Autowired
	private UtilisateurRestController utilisateurRest;
	
	@GetMapping("/creation")
	public void upload() {
		Adresse adr1 = new Adresse("25 Allée des Eyquems", "Bâtiment A", "33700", "Mérignac", "France");
		Adresse adr2 = new Adresse("3 Avenue des chèvrefeuilles", "Immeuble B", "33120", "Arcachon", "France");
		Adresse adr3 = new Adresse("33 Route Sainte Catherine", "Place clémenceau", "33100", "Bordeaux", "France");
		Adresse adr4 = new Adresse("50 Allée des champs élysées", "Bâtiment C", "75000", "Paris", "France");
		Adresse adr5 = new Adresse("46 Route de l'aviation", "", "64600", "Anglet", "France");
		Adresse adr6 = new Adresse("28 Rue de gravillon", "Étage 3", "35000", "Rennes", "France");
		Adresse adr7 = new Adresse("Voie d'onyx", "", "19000", "Tulle", "France");
		Adresse adr8 = new Adresse("30 Rue du juge", "a", "33000", "Bordeaux", "France");
		Adresse adr9 = new Adresse("a", "a", "a", "a", "France");
		Adresse adr10 = new Adresse("a", "a", "a", "a", "France");
		Adresse adr11 = new Adresse("a", "a", "a", "a", "France");
		Adresse adr12 = new Adresse("a", "a", "a", "a", "France");
		Adresse adr13 = new Adresse("76 Rue du pionnier", "Bâtiment B", "33700", "Mérignac", "France");
		Adresse adr14 = new Adresse("a", "a", "a", "a", "France");
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		Utilisateur utilisateur1 = new Utilisateur();
		Utilisateur utilisateur2 = new Utilisateur();
		Utilisateur utilisateur3 = new Utilisateur();
		Utilisateur utilisateur4 = new Utilisateur();
		Utilisateur utilisateur5 = new Utilisateur();
		Utilisateur utilisateur6 = new Utilisateur();
		Utilisateur utilisateur7 = new Utilisateur();
		Utilisateur utilisateur8 = new Utilisateur();
		Utilisateur utilisateur9 = new Utilisateur();
		Utilisateur utilisateur10 = new Utilisateur();
		Utilisateur utilisateur11 = new Utilisateur();
		Utilisateur utilisateur12 = new Utilisateur();
		Utilisateur utilisateur13 = new Utilisateur();
		Utilisateur utilisateur14 = new Utilisateur();
		
		
		Formateur formateur1 = new Formateur();
		Formateur formateur2 = new Formateur();
		Formateur formateur3 = new Formateur();
		Formateur formateur4 = new Formateur();
		
		Stagiaire stagiaire1 = new Stagiaire();
		Stagiaire stagiaire2 = new Stagiaire();
		Stagiaire stagiaire3 = new Stagiaire();
		Stagiaire stagiaire4 = new Stagiaire();
		
		Contact contact1 = new Contact();
		Contact contact2 = new Contact();
		Contact contact3 = new Contact();
		Contact contact4 = new Contact();
		
		Administrateur admin1 = new Administrateur();
		
		Gestionnaire gestionnaire1 = new Gestionnaire();
	
		
		Client client1 = new Client();
		Client client2 = new Client();
		Client client3 = new Client();
		Client client4 = new Client();
		
		
		Disponibilite dispo1 = new Disponibilite();
		
		
		Formation formation1  = new Formation();
		Formation formation2  = new Formation();
		Formation formation3  = new Formation();
		
		
		Matiere matiere1 = new Matiere();
		Matiere matiere2 = new Matiere();
		Matiere matiere3 = new Matiere();
		Matiere matiere4 = new Matiere();
		
		
		Module module1 = new Module();
		Module module2 = new Module();
		
		
		Ordinateur ordi1 = new Ordinateur();
		Ordinateur ordi2 = new Ordinateur();
		Ordinateur ordi3 = new Ordinateur();
		Ordinateur ordi4 = new Ordinateur();
		
		Salle salle1 = new Salle();
		Salle salle2 = new Salle();
		Salle salle3 = new Salle();
		Salle salle4 = new Salle();
		Salle salle5 = new Salle();
		
		Videoprojecteur videoproj1 = new Videoprojecteur();
		Videoprojecteur videoproj2 = new Videoprojecteur();
		Videoprojecteur videoproj3 = new Videoprojecteur();
		Videoprojecteur videoproj4 = new Videoprojecteur();
		
		ClientFormation clientForma1 = new ClientFormation();
		ClientFormation clientForma2 = new ClientFormation();
		
		MaterielFormation materielForma1 = new MaterielFormation();
		MaterielFormation materielForma2 = new MaterielFormation();
		MaterielFormation materielForma3 = new MaterielFormation();
		MaterielFormation materielForma4 = new MaterielFormation();
		
		StagiaireFormation stagiaireForma1 = new StagiaireFormation();
		StagiaireFormation stagiaireForma2 = new StagiaireFormation();
		
		//Formateur 1
		formateur1.setCivilite(Civilite.MME);
		formateur1.setNom("SEGATO");
		formateur1.setPrenom("Elena");
		formateur1.setTelephone("0625431262");
		formateur1.setUtilisateur(utilisateur1);
		formateur1.setAdresse(adr1);
		
		//Formateur 2
		formateur2.setCivilite(Civilite.M);
		formateur2.setNom("KHAIDA");
		formateur2.setPrenom("Armand");
		formateur2.setTelephone("0645852315");
		formateur2.setUtilisateur(utilisateur2);
		formateur2.setAdresse(adr2);
		
		//Formateur 3
		formateur3.setCivilite(Civilite.M);
		formateur3.setNom("PERROUAULT");
		formateur3.setPrenom("Jérémy");
		formateur3.setTelephone("0605214897");
		formateur3.setUtilisateur(utilisateur3);
		formateur3.setAdresse(adr3);
		
		//Formateur 4
		formateur4.setCivilite(Civilite.M);
		formateur4.setNom("SULTAN");
		formateur4.setPrenom("Eric");
		formateur4.setTelephone("0625431262");
		formateur4.setUtilisateur(utilisateur4);
		formateur4.setAdresse(adr4);
		
		//Stagiaire 1
		stagiaire1.setVersion(0);
		stagiaire1.setCivilite(Civilite.MLLE);
		stagiaire1.setNom("SABAROT");
		stagiaire1.setPrenom("Sophie");
		stagiaire1.setTelephone("0748521639");
		stagiaire1.setUtilisateur(utilisateur5);
		stagiaire1.setAdresse(adr5);
		stagiaire1.setOrdinateur(ordi1);
		
		//Stagiaire 2
		stagiaire2.setVersion(0);
		stagiaire2.setCivilite(Civilite.M);
		stagiaire2.setNom("Ortolan");
		stagiaire2.setPrenom("Marc");
		stagiaire2.setTelephone("0743498390");
		stagiaire2.setUtilisateur(utilisateur6);
		stagiaire2.setAdresse(adr6);
		stagiaire2.setOrdinateur(ordi2);
		
//		//Stagiaire 4
//		stagiaire1.setId(7L);
//		stagiaire1.setVersion(0);
//		stagiaire1.setCivilite(Civilite.MLLE);
//		stagiaire1.setNom("SABAROT");
//		stagiaire1.setPrenom("SOphie");
//		stagiaire1.setTelephone("0748521639");
//		stagiaire1.setUtilisateur(utilisateur7);
//		stagiaire1.setAdresse(adr7);
//		
//		//Stagiaire 5
//		stagiaire1.setId(8L);
//		stagiaire1.setVersion(0);
//		stagiaire1.setCivilite(Civilite.MLLE);
//		stagiaire1.setNom("SABAROT");
//		stagiaire1.setPrenom("SOphie");
//		stagiaire1.setTelephone("0748521639");
//		stagiaire1.setUtilisateur(utilisateur8);
//		stagiaire1.setAdresse(adr8);
				
		//Contact 1
		contact1.setVersion(0);
		contact1.setCivilite(Civilite.MME);
		contact1.setNom("SAVARD");
		contact1.setPrenom("Isabelle");
		contact1.setTelephone("0725418975");
		contact1.setUtilisateur(utilisateur9);
		contact1.setAdresse(adr3);
		contact1.setClient(client1);
		
		//Contact 1
		contact2.setVersion(0);
		contact2.setCivilite(Civilite.M);
		contact2.setNom("BAMBELLE");
		contact2.setPrenom("Larry");
		contact2.setTelephone("0751287314");
		contact2.setUtilisateur(utilisateur10);
		contact2.setAdresse(adr5);
		contact2.setClient(client2);
		
//		//Contact 1
//		contact1.setId(6L);
//		contact1.setVersion(0);
//		contact1.setCivilite(Civilite.MME);
//		contact1.setNom("MOI");
//		contact1.setPrenom("Moi");
//		contact1.setTelephone("0505050505");
//		contact1.setUtilisateur(utilisateur2);
//		contact1.setAdresse(adr2);
//		contact1.setClient(client1);
//		
//		//Contact 1
//		contact1.setId(6L);
//		contact1.setVersion(0);
//		contact1.setCivilite(Civilite.MME);
//		contact1.setNom("MOI");
//		contact1.setPrenom("Moi");
//		contact1.setTelephone("0505050505");
//		contact1.setUtilisateur(utilisateur2);
//		contact1.setAdresse(adr2);
//		contact1.setClient(client1);
		
		//Administrateur 1
		admin1.setVersion(0);
		admin1.setCivilite(Civilite.MME);
		admin1.setNom("BONSAINT");
		admin1.setPrenom("Sabine");
		admin1.setTelephone("0632524125");
		admin1.setUtilisateur(utilisateur13);
		admin1.setAdresse(adr13);
		
		//Gestionnaire 1
		gestionnaire1.setVersion(0);
		gestionnaire1.setCivilite(Civilite.M);
		gestionnaire1.setNom("MEURDESOIF");
		gestionnaire1.setPrenom("Jean");
		gestionnaire1.setTelephone("0623164895");
		gestionnaire1.setUtilisateur(utilisateur14);
		gestionnaire1.setAdresse(adr13);
		
		//Utilisateur 1
		utilisateur1.setTypeUser(TypeUser.Formateur);
		utilisateur1.setMotDePasse("pmoilukj525");
		utilisateur1.setMail("e.segato@gmail.com");
		
		//Utilisateur 2
		utilisateur2.setTypeUser(TypeUser.Formateur);
		utilisateur2.setMotDePasse("htg52hjyutu");
		utilisateur2.setMail("a.khaida@gmail.com");
		
		//Utilisateur 3
		utilisateur3.setTypeUser(TypeUser.Formateur);
		utilisateur3.setMotDePasse("azerty");
		utilisateur3.setMail("j.perroualt@gmail.com");
		
		//Utilisateur 4
		utilisateur4.setTypeUser(TypeUser.Formateur);
		utilisateur4.setMotDePasse("azerty");
		utilisateur4.setMail("e.sultan@gmail.com");
		
		//Utilisateur 5
		utilisateur5.setTypeUser(TypeUser.Stagiaire);
		utilisateur5.setMotDePasse("azerty");
		utilisateur5.setMail("s.sabarot@gmail.com");
		
		//Utilisateur 6
		utilisateur6.setTypeUser(TypeUser.Stagiaire);
		utilisateur6.setMotDePasse("azerty");
		utilisateur6.setMail("m.ortolan@gmail.com");
		
//		//Utilisateur 7
//		utilisateur7.setId(21L);
//		utilisateur7.setVersion(0);
//		utilisateur7.setTypeUser(TypeUser.Stagiaire);
//		utilisateur7.setMotDePasse("azerty");
//		utilisateur7.setMail("admin1@gmail.com");
//		
//		//Utilisateur 8
//		utilisateur8.setId(22L);
//		utilisateur8.setVersion(0);
//		utilisateur8.setTypeUser(TypeUser.Stagiaire);
//		utilisateur8.setMotDePasse("azerty");
//		utilisateur8.setMail("admin@gmail.com");
		
		//Utilisateur 9
		utilisateur9.setTypeUser(TypeUser.Contact);
		utilisateur9.setMotDePasse("azerty");
		utilisateur9.setMail("i.savard@gmail.com");
		
		//Utilisateur 10
		utilisateur10.setTypeUser(TypeUser.Contact);
		utilisateur10.setMotDePasse("azerty");
		utilisateur10.setMail("i.bambelle@hotmail.fr");
		
//		//Utilisateur 11
//		utilisateur11.setId(25L);
//		utilisateur11.setVersion(0);
//		utilisateur11.setTypeUser(TypeUser.Formateur);
//		utilisateur11.setMotDePasse("azerty");
//		utilisateur11.setMail("admin@gmail.com");
//		
//		//Utilisateur 12
//		utilisateur12.setId(26L);
//		utilisateur12.setVersion(0);
//		utilisateur12.setTypeUser(TypeUser.Formateur);
//		utilisateur12.setMotDePasse("azerty");
//		utilisateur12.setMail("admin@gmail.com");
		
		//Utilisateur 13
		utilisateur13.setTypeUser(TypeUser.Administrateur);
		utilisateur13.setMotDePasse("vbf56ghg15d5");
		utilisateur13.setMail("s.bonsaint@hotmail.com");
		
		//Utilisateur 14
		utilisateur14.setTypeUser(TypeUser.Gestionnaire);
		utilisateur14.setMotDePasse("azerty");
		utilisateur14.setMail("j.meurdesoif@gmail.com");
				
		
		//Client 1
		client1.setNomEntreprise("Info Machine");
		client1.setNomBusinessUnit("Arcachon");
		client1.setNumeroSiret("12734598742365");
		client1.setTypeEntreprise(TypeEntreprise.SARL);
		client1.setNumTva("FR248596431");
		client1.setAdresse(adr2);
		
		//Client 2
		client2.setNomEntreprise("Speculos");
		client2.setNomBusinessUnit("Rennes");
		client2.setNumeroSiret("58946234875961");
		client2.setTypeEntreprise(TypeEntreprise.SA);
		client2.setNumTva("FR785431956");
		client2.setAdresse(adr6);
						
		//Ordi 1
		ordi1.setCode(245L);
		ordi1.setCoutUtilisation(12f);
		ordi1.setProcesseur("Intel Core i5");
		ordi1.setRam(8);
		ordi1.setQuantiteDD(250);
		try {
			ordi1.setDtAchat(sdf.parse("2019-03-18"));
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//Ordi 2
		ordi2.setCode(324L);
		ordi2.setCoutUtilisation(52f);
		ordi2.setProcesseur("Intel Core i5");
		ordi2.setRam(8);
		ordi2.setQuantiteDD(480);
		try {
			ordi2.setDtAchat(sdf.parse("2019-03-18"));
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//Salle 1
		salle1.setCode(235L);
		salle1.setCoutUtilisation(95f);
		salle1.setPlaceMax(22);
		salle1.setAdresse(adr8);
		
		//Salle 2
		salle2.setCode(162L);
		salle2.setCoutUtilisation(86f);
		salle2.setPlaceMax(18);
		salle2.setAdresse(adr1);
		
		//Salle 3
		salle3.setCode(124L);
		salle3.setCoutUtilisation(37f);
		salle3.setPlaceMax(5);
		salle3.setAdresse(adr3);
		
		//Salle 4
		salle4.setCode(425L);
		salle4.setCoutUtilisation(49f);
		salle4.setPlaceMax(10);
		salle4.setAdresse(adr4);
		
		//Vidéoprojecteur 1
		videoproj1.setCode(231L);
		videoproj1.setCoutUtilisation(12f);
		videoproj1.setHdmi(true);
		videoproj1.setVga(true);
		videoproj1.setResolution(Resolution.Haute);
		
		//Vidéoprojecteur 2
		videoproj2.setCode(784L);
		videoproj2.setCoutUtilisation(32f);
		videoproj2.setHdmi(true);
		videoproj2.setVga(false);
		videoproj2.setResolution(Resolution.Moyenne);
		
		//Vidéoprojecteur 3
		videoproj3.setCode(125L);
		videoproj3.setCoutUtilisation(41f);
		videoproj3.setHdmi(false);
		videoproj3.setVga(true);
		videoproj3.setResolution(Resolution.Basse);
		
		//Vidéoprojecteur 4
		videoproj4.setCode(215L);
		videoproj4.setCoutUtilisation(25f);
		videoproj4.setHdmi(false);
		videoproj4.setVga(true);
		videoproj4.setResolution(Resolution.UltraHaute);
		
		//Formation 1
		try {
			formation1.setDtDebut(sdf.parse("2019-06-19"));
			formation1.setDtFin(sdf.parse("2019-09-20"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		formation1.setIntitule("Info Machine Juin 2019");
		formation1.setCapacite(26);
		
		//Formation 2
		try {
			formation2.setDtDebut(sdf.parse("2020-02-23"));
			formation2.setDtFin(sdf.parse("2020-04-30"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		formation2.setIntitule("Golden Nugget Février 2020");
		formation2.setCapacite(12);
		
		//Formation 3
		try {
			formation3.setDtDebut(sdf.parse("2021-03-8"));
			formation3.setDtFin(sdf.parse("2021-03-12"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		formation3.setIntitule("Speculos Mars 2021");
		formation3.setCapacite(20);
		
		//Matière 1
		matiere1.setTitre("Typescript");
		matiere1.setDuree(4);
		matiere1.setObjectifs("Découvrir et maîtriser le langage TypeScript.");
		matiere1.setPrerequis("Aucun");
		
		//Matière 2
		matiere2.setTitre("Spring");
		matiere2.setDuree(8);
		matiere2.setObjectifs("Comprendre les principes de Spring.");
		matiere2.setPrerequis("Aucun");
		
		//Matière 3
		matiere3.setTitre("JPA");
		matiere3.setDuree(6);
		matiere3.setObjectifs("Comprendre le fonctionnement de JPA en détail.");
		matiere3.setPrerequis("Aucun");
		
		//Matière 4
		matiere4.setTitre("Hibernate");
		matiere4.setDuree(2);
		matiere4.setObjectifs("Réaliser la persistance des objets avec le Framework Hibernate.");
		matiere4.setPrerequis("Aucun");
		
		//Module 1
		try {
			module1.setDtDebut(sdf.parse("2019-06-19"));
			module1.setDtFin(sdf.parse("2019-06-23"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		module1.setMatiere(matiere1);
		module1.setFormateur(formateur1);
		module1.setFormation(formation1);
		
		//Module 2
		try {
			module2.setDtDebut(sdf.parse("2019-06-24"));
			module2.setDtFin(sdf.parse("2019-07-01"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		module2.setMatiere(matiere2);
		module2.setFormateur(formateur2);
		module2.setFormation(formation1);
		
		//StagiaireFormation 1
		stagiaireForma1.setStagiaire(stagiaire1);
		stagiaireForma1.setFormation(formation1);
		
		//StagiaireFormation 2
		stagiaireForma2.setStagiaire(stagiaire2);
		stagiaireForma2.setFormation(formation1);
		
		//MaterielFormation 1
		try {
			materielForma1.setDtDebut(sdf.parse("2019-06-19"));
			materielForma1.setDtFin(sdf.parse("2019-09-20"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		materielForma1.setMateriel(salle1);
		materielForma1.setFormation(formation1);
		
		//MaterielFormation 2
		try {
			materielForma2.setDtDebut(sdf.parse("2019-06-19"));
			materielForma2.setDtFin(sdf.parse("2019-09-20"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		materielForma2.setMateriel(videoproj1);
		materielForma2.setFormation(formation1);
		
		//MaterielFormation 3
		try {
			materielForma3.setDtDebut(sdf.parse("2019-06-19"));
			materielForma3.setDtFin(sdf.parse("2019-09-20"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		materielForma3.setMateriel(ordi1);
		materielForma3.setFormation(formation1);
		
		//MaterielFormation 4
		try {
			materielForma4.setDtDebut(sdf.parse("2019-06-19"));
			materielForma4.setDtFin(sdf.parse("2019-09-20"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		materielForma4.setMateriel(ordi2);
		materielForma4.setFormation(formation1);
		
		//ClientFormation 1
		clientForma1.setDemandes(10);
		clientForma1.setCommentaires("RAS");
		clientForma1.setStatut(false);
		clientForma1.setAccordes(8);
		clientForma1.setClient(client1);
		clientForma1.setFormation(formation1);
		
		//ClientFormation 2
		clientForma2.setDemandes(5);
		clientForma2.setCommentaires("RAS");
		clientForma2.setStatut(true);
		clientForma2.setAccordes(5);
		clientForma2.setClient(client2);
		clientForma2.setFormation(formation1);
		
//		//Disponibilité 1
//		dispo1.setId(7L);
//		dispo1.setVersion(0);
//		dispo1.setDtDispo(new Date("2019-09-20"));
//		dispo1.setFormateur(formateur1);
		
		utilisateurRest.create(utilisateur1);
		utilisateurRest.create(utilisateur2);
		utilisateurRest.create(utilisateur3);
		utilisateurRest.create(utilisateur4);
		utilisateurRest.create(utilisateur5);
		utilisateurRest.create(utilisateur6);
//		utilisateurRest.create(utilisateur7);
//		utilisateurRest.create(utilisateur8);
		utilisateurRest.create(utilisateur9);
		utilisateurRest.create(utilisateur10);
//		utilisateurRest.create(utilisateur11);
//		utilisateurRest.create(utilisateur12);
		utilisateurRest.create(utilisateur13);
		utilisateurRest.create(utilisateur14);
		matiereRest.create(matiere1);
		matiereRest.create(matiere2);
		matiereRest.create(matiere3);
		matiereRest.create(matiere4);
		materielRest.create(ordi1);
		materielRest.create(ordi2);
		materielRest.create(salle1);
		materielRest.create(salle2);
		materielRest.create(salle3);
		materielRest.create(salle4);
		materielRest.create(videoproj1);
		materielRest.create(videoproj2);
		materielRest.create(videoproj3);
		materielRest.create(videoproj4);
		formationRest.create(formation1);
		formationRest.create(formation2);
		formationRest.create(formation3);
		clientRest.create(client1);
		clientRest.create(client2);
		formateurRest.create(formateur1);
		formateurRest.create(formateur2);
		formateurRest.create(formateur3);
		formateurRest.create(formateur4);
		stagiaireRest.create(stagiaire1);
		stagiaireRest.create(stagiaire2);
		contactRest.create(contact1);
		contactRest.create(contact2);
		adminRest.create(admin1);
		gestRest.create(gestionnaire1);
		moduleRest.create(module1);
		moduleRest.create(module2);
		stagiaireFormationRest.create(stagiaireForma1);
		stagiaireFormationRest.create(stagiaireForma2);
		materielFormationRest.create(materielForma1);
		materielFormationRest.create(materielForma2);
		materielFormationRest.create(materielForma3);
		materielFormationRest.create(materielForma4);
		clientFormationRest.create(clientForma1);
		clientFormationRest.create(clientForma2);
		
	}
	
}
