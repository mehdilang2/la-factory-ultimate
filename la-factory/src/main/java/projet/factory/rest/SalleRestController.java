package projet.factory.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import projet.factory.model.Materiel;
import projet.factory.model.MaterielFormation;
import projet.factory.model.Salle;
import projet.factory.model.Views;
import projet.factory.repository.IMaterielFormationRepository;
import projet.factory.repository.IMaterielRepository;

// A REVOIR !!!

@RestController
@RequestMapping("/api/salle")
@CrossOrigin("*")
public class SalleRestController {

	@Autowired
	private IMaterielRepository materielRepo;
	@Autowired
	private IMaterielFormationRepository materielFormationRepo;

	@GetMapping("")
	@JsonView(Views.ViewSalle.class)
	public List<Salle> list() {
		return materielRepo.findAllSalle();
	}

	@GetMapping("/{id}")
	@JsonView(Views.ViewSalle.class)
	public Materiel find(@PathVariable Long id) {
		return materielRepo.findById(id).get();
	}
	
	@GetMapping("/{id}/materielFormation")
	@JsonView(Views.ViewSalle.class)
	public List<MaterielFormation> findMaterielFormation(@PathVariable Long id) {
		return materielFormationRepo.findMaterielFormationByVideoProjecteur(id);
	}
	
	@GetMapping("/{id}/dates")
	@JsonView(Views.ViewSalleWithDates .class)
	public Salle findSalleByFormation(@PathVariable Long id) {
		return materielRepo.findSalleWithDates(id);
	}

	@PostMapping("")
	@JsonView(Views.ViewSalle.class)
	public void create(@RequestBody Salle salle) {
		materielRepo.save(salle);
	}

	@PutMapping("/{id}")
	@JsonView(Views.ViewSalle.class)
	public void update(@PathVariable Long id, @RequestBody Salle salle) {
		materielRepo.save(salle);
	}


	@DeleteMapping("/{id}")
	public void remove(@PathVariable Long id) {
		materielRepo.deleteById(id);
	}
	
	

}

