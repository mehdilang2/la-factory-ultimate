package projet.factory.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import projet.factory.model.Civilite;
import projet.factory.model.Contact;
import projet.factory.model.Formation;
import projet.factory.model.Gestionnaire;
import projet.factory.model.Stagiaire;
import projet.factory.model.Views;
import projet.factory.repository.IFormationRepository;
import projet.factory.repository.IPersonneRepository;
import projet.factory.repository.IStagiaireFormationRepository;
import projet.factory.repository.IUtilisateurRepository;


@RestController
@RequestMapping("/api/stagiaire")
@CrossOrigin("*")
public class StagiaireRestController {

	@Autowired
	private IPersonneRepository personneRepo;
	
	@Autowired
	private IUtilisateurRepository utilisateurRepo;
	
	@Autowired
	private IStagiaireFormationRepository stagiaireFormationRepo;
	
	@Autowired
	private IFormationRepository formationRepo;
	

	@GetMapping("")
	@JsonView(Views.ViewStagiaire.class)
	public List<Stagiaire> list() {
		return personneRepo.findAllStagiaire();
	}
	@GetMapping("/formation")
	@JsonView(Views.ViewStagiaireWithFormation.class)
	public List<Stagiaire> listforamtion() {
		return personneRepo.findStagiairesWithFormation();
	}

	@GetMapping("/{id}")
	@JsonView(Views.ViewStagiaire.class)
	public Stagiaire find(@PathVariable Long id) {
		return (Stagiaire) personneRepo.findById(id).get();
	}
	
	@GetMapping("/{id}/contact")
	@JsonView(Views.ViewStagiaire.class)
	public Contact findContact(@PathVariable Long id) {
		return personneRepo.findContactByStagiaire(id);
	}
	
	

	
	
	@GetMapping("/{id}/gestionnaire")
	@JsonView(Views.ViewStagiaire.class)
	public Gestionnaire findGestionnaireByStagiaire(@PathVariable Long id) {
		return (Gestionnaire) personneRepo.findGestionnaireByStagiaire(id);
	}

	@PostMapping("")
	@JsonView(Views.ViewStagiaire.class)
	public void create(@RequestBody Stagiaire stagiaire) {
		utilisateurRepo.save(stagiaire.getUtilisateur());
		personneRepo.save(stagiaire);
	}

	@PutMapping("/{id}")
	@JsonView(Views.ViewStagiaire.class)
	public void update(@PathVariable Long id, @RequestBody Stagiaire stagiaire) {
		utilisateurRepo.save(stagiaire.getUtilisateur());
		personneRepo.save(stagiaire);
	}

	@DeleteMapping("/{id}")
	public void remove(@PathVariable Long id) {
		Long idUtilisateur = personneRepo.findById(id).get().getUtilisateur().getId();
		stagiaireFormationRepo.deleteStagiaireFormationByStagiaire(id);
		personneRepo.deleteById(id);
		utilisateurRepo.deleteById(idUtilisateur);
			
	}
	
	@GetMapping("/civilites")
	public Civilite[] civilites() {
		return Civilite.values();
	}
	
	@GetMapping("/{id}/presence")
	@JsonView(Views.ViewStagiairewithpresence.class)
	public Stagiaire findStagiaireWithPresence(@PathVariable Long id) {
		return (Stagiaire) personneRepo.findStagiaireWithPresence(id);
	}
	
	@GetMapping("/{id}/formations")
	@JsonView(Views.ViewFormationWithStagiaire.class)
	public List <Formation> findFormationByIdStagiaire(@PathVariable Long id) {
		return formationRepo.findFormationbyIdStagiaire(id);
	}
}

