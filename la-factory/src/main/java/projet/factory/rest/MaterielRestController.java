package projet.factory.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import projet.factory.model.Materiel;
import projet.factory.model.Matiere;
import projet.factory.model.Views;
import projet.factory.repository.IMaterielRepository;


@RestController
@RequestMapping("/api/materiel")
@CrossOrigin("*")
public class MaterielRestController {

	@Autowired
	private IMaterielRepository materielRepo;

	@GetMapping("")
	@JsonView(Views.ViewMateriel.class)
	public List<Materiel> list() {
		return materielRepo.findAll();
	}

	@GetMapping("/{id}")
	@JsonView(Views.ViewMateriel.class)
	public Materiel find(@PathVariable Long id) {
		return materielRepo.findById(id).get();
	}

	@PostMapping("")
	@JsonView(Views.ViewMateriel.class)
	public void create(@RequestBody Materiel materiel) {
		materielRepo.save(materiel);
	}

	@PutMapping("/{id}")
	@JsonView(Views.ViewMateriel.class)
	public void update(@PathVariable Long id, @RequestBody Materiel materiel) {
		materielRepo.save(materiel);
	}


	@DeleteMapping("/{id}")
	public void remove(@PathVariable Long id) {
		materielRepo.deleteById(id);
	}
	
	

}

