import {Injectable} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UtilisateurHttpService} from '../../utilisateur/utilisateur-http.service';

@Injectable()

export class AuthService {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private utilisateurHttpService: UtilisateurHttpService
  ) {
  }

  login(loginForm: any) {
    console.log('Tentative de connexion');
    let rolesUser = [];
    let idUser: number;
    let utilisateur: any;
    this.utilisateurHttpService.findByMail(loginForm.username).subscribe(resp => {
      utilisateur = resp;
      console.log("Récupération");
      if (utilisateur.typeUser === 'Administrateur') {
        rolesUser = ['administrateur'];
        idUser = utilisateur.personne.id;
      } else if (utilisateur.typeUser === 'Gestionnaire') {
        rolesUser = ['gestionnaire'];
        idUser = utilisateur.personne.id;
      } else if (utilisateur.typeUser === 'Formateur') {
        rolesUser = ['formateur'];
        idUser = utilisateur.personne.id;
      } else if (utilisateur.typeUser === 'Contact') {
        rolesUser = ['client'];
        idUser = utilisateur.personne.id;
      } else if (utilisateur.typeUser === 'Stagiaire') {
        rolesUser = ['stagiaire'];
        idUser = utilisateur.personne.id;
      }
      this.setUser({login: utilisateur.personne.prenom + " " + utilisateur.personne.nom, roles: rolesUser, id: idUser, mail: loginForm.username});
      // On récupère l'url de redirection
      const redirectUrl = this.route.snapshot.queryParams['redirectUrl'] || '/home';
      // On accède à la page souhaitée
      this.router.navigate([redirectUrl]);
    }, err => console.log(err));




  }

  hasAnyRole(roles: string[]) {
    const user = this.getUser();
    if (user) {
      for (const role of user.roles) {
        if (roles.includes(role)) {
          return true;
        }
      }
    }
    return false;
  }

  logout() {
    console.log('Tentative de déconnexion');
    this.clearUser();
    this.router.navigate(['/login']);
  }

  getUser() {
    return JSON.parse(localStorage.getItem('user'));
  }

  setUser(user: any) {
    console.log(user);
    localStorage.setItem('user', JSON.stringify(user));
  }

  clearUser() {
    localStorage.removeItem('user');
  }
}
