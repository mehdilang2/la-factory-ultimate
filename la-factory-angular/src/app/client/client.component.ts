import {Component, OnInit} from '@angular/core';
import {Client} from '../model/client';
import {ClientHttpService} from './client-http.service';
import {Adresse} from '../model/adresse';
import {HttpClient} from '@angular/common/http';
import {Contact} from '../model/contact';
import {Utilisateur} from '../model/utilisateur';
import {AuthService} from '../service/auth/auth.service';
import {Gestionnaire} from '../model/gestionnaire';
import {ContactHttpService} from '../contact/contact-http.service';
import {GestionnaireHttpService} from '../gestionnaire/gestionnaire-http.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css'],
  providers: [AuthService]
})
export class ClientComponent implements OnInit {
  private search: string;
  private selectedClient: any = null;
  private Adresse: any = null;
  private types: any = null;
  private  selectedClientContact: any = null;
  private  contactForm: any =null;
  private clientselctContact : any = null;
  private civilites: any = null;
  private editField: string;
  private personList: Array<any>;

  private utilisateur: any = null;

  constructor(private clientHttpService: ClientHttpService, private http: HttpClient, private authService: AuthService, private contacthttpService: ContactHttpService, private gestionnaireHttpService : GestionnaireHttpService, ) {
    this.typeEntreprise();
    this.civilite();

  }

  ngOnInit() {
  }
  public filter(): Array<Client> {
    if (this.search) {
      return this.clientHttpService.findAll().filter(f => f.nomEntreprise.toLowerCase().indexOf(this.search.toLowerCase()) !== -1);
    } else {
      return this.clientHttpService.findAll();
    }
  }

  add() {
    this.selectedClient = new Client();
  }

  clientContact(id: number){
    this.clientHttpService.findContactsByIdClient(id).subscribe(resp =>{ this.selectedClientContact = resp;
     this.clientHttpService.findById(id).subscribe(resp=>this.clientselctContact =resp);
    for (let i in this.selectedClientContact){
      console.log( this.selectedClientContact[i]);
      this.selectedClientContact[i].mail= this.selectedClientContact[i].utilisateur.mail;
    }
    }, err => console.log(err));
  }



  edit(id: number) {
    this.clientHttpService.findById(id).subscribe(resp => {
      this.selectedClient = resp;
      if (this.selectedClient.adresse == null) {
        this.selectedClient.adresse = new Adresse();
      }
    } , err => console.log(err));
  }

  delete(id: number) {
    this.clientHttpService.delete(id);
  }

  deleteContact(id: number) {
    this.contacthttpService.delete( id);
  }

  save() {
    this.clientHttpService.save(this.selectedClient);
    this.selectedClient = null;
  }

  saveContact(contact: Contact) {

    this.contacthttpService.save(contact);
    this.contactForm=null;
    this.clientContact(this.clientselctContact.id);
  }

  cancel() {
    this.selectedClient = null;
  }

  cancelContact() {
    this.selectedClientContact = null;
  }

  typeEntreprise() {
    this.clientHttpService.findTypeEntreprise().subscribe(resp => this.types = resp, err => console.log(err));

  }

  updateList(contact: Contact, property: string, event: any) {
    contact[property] = event.target.textContent;
  }

  addContact() {
    this.contactForm = new Contact();
    console.log(this.clientselctContact);
  this.contactForm.client =this.clientselctContact;

  }
  editContact(id: number) {

  }
  civilite() {
    this.gestionnaireHttpService.findCivilite().subscribe(resp => this.civilites = resp, err => console.log(err));
  }
  changeValue(id: number, property: string, event: any) {
    // console.log("changeValue()");
    // this.editField = event.target.textContent;
  }

  hasAnyRole(roles: string[]) {
    return this.authService.hasAnyRole(roles);
  }
}
