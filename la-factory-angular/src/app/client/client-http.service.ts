import {Injectable} from '@angular/core';

import {Client} from '../model/client';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class ClientHttpService {

  private clients: any;

  constructor(private http: HttpClient) {
    this.load();
  }

  load() {
    this.http.get('http://localhost:8080/api/client').subscribe(resp => {
      this.clients = resp;
    }, err => console.log(err));
  }

  findAll(): Array<Client> {
    return this.clients;
  }

  findById(id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/client/' + id);
  }

  findContactsByIdClient(id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/client/' + id + '/contact/');
  }

  save(client: Client) {
    if (client) {
      if (client.id) {
        this.http.put('http://localhost:8080/api/client/' + client.id, client).subscribe(resp => this.load(),
          err => console.log(err)
        );
      } else {
        this.http.post('http://localhost:8080/api/client', client).subscribe(resp => this.load(),
          err => console.log(err));
      }
    }
  }

  delete(id: number) {
    this.http.delete('http://localhost:8080/api/client/' + id).subscribe(resp => this.load(),
      err => console.log(err));

  }
  findTypeEntreprise() : Observable<Object> {
    return this.http.get('http://localhost:8080/api/client/typeentreprise');
  }

}
