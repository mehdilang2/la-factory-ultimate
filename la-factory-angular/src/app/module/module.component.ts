import { Component, OnInit } from '@angular/core';
import {Module} from '../model/module';
import {ModuleHttpService} from './module-http.service';
import {FormationHttpService} from '../formation/formation-http.service';
import {MatiereHttpService} from '../matiere/matiere-http.service';
import {Adresse} from '../model/adresse';
import {Formation} from '../model/formation';
import {Ordinateur} from '../model/ordinateur';
import {Client} from '../model/client';
import {Formateur} from '../model/formateur';
import {Matiere} from '../model/matiere';
import {FormateurHttpService} from '../formateur/formateur-http.service';


@Component({
  selector: 'app-module',
  templateUrl: './module.component.html',
  styleUrls: ['./module.component.css'],
  providers: [FormateurHttpService, FormationHttpService]
})
export class ModuleComponent implements OnInit {

  // private modules: Array<module> = new Array<module>();
  private search: string;
  private moduleForm: any = null;
  private searchmatiere: string;
  private searchformation: string;
  private searchformateur: string;


  constructor(private moduleHttpService: ModuleHttpService, private matiereHttpService: MatiereHttpService, private formationHttpService: FormationHttpService, private formateurHttpService: FormateurHttpService) {
  }

  ngOnInit() {
  }

  list(): Array<Module> {
    return this.moduleHttpService.findAll();
  }

  add() {
    this.moduleForm = new Module();

    if(this.moduleForm.formation == null){
      this.moduleForm.formation = new Formation();
    }
    if(this.moduleForm.formateur == null){
      this.moduleForm.formateur = new Formateur();
    }
  }


  edit(id: number) {
    this.moduleHttpService.findById(id).subscribe(resp => {this.moduleForm = resp;
    if (this.moduleForm.formateur == null){
      this.moduleForm.formateur = new Formateur();
    }
    if (this.moduleForm.formation == null){
      this.moduleForm.formation = new Formation();
    }
    if (this.moduleForm.matiere == null){
      this.moduleForm.matiere = new Matiere();
    }}, err => console.log(err));
  }

  delete(id: number) {
    this.moduleHttpService.delete(id);
  }

  save() {
    this.moduleHttpService.save(this.moduleForm);
    this.moduleForm = null;
  }

  cancel() {
    this.moduleForm = null;
  }

  filtermatiere(){
    if (this.searchmatiere) {
      return this.matiereHttpService.findAll().filter(m => m.titre.toLowerCase().indexOf(this.search.toLowerCase()) !== -1);
    } else {
      return this.matiereHttpService.findAll();
    }


  }

  filterformation(){
    if (this.searchformation) {
      return this.formationHttpService.findAll().filter(f => f.intitule.toLowerCase().indexOf(this.search.toLowerCase()) !== -1);
    } else {
      return this.formationHttpService.findAll();
    }
  }

  filterformateur(){
    if (this.searchformateur) {
      return this.formateurHttpService.findAll().filter(g => g.nom.toLowerCase().indexOf(this.search.toLowerCase()) !== -1);
    } else {
      return this.formateurHttpService.findAll();
    }
  }



}

