import {Injectable} from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Module} from '../model/module';
import {MatiereHttpService} from '../matiere/matiere-http.service';

@Injectable()
export class ModuleHttpService {

  private modules: any;


  constructor(private http: HttpClient,private matiereHttpService:MatiereHttpService) {
    this.load();
  }

  load() {
    this.http.get('http://localhost:8080/api/module').subscribe(resp => {
      this.modules = resp;
    }, err => console.log(err));
  }

  findAll(): Array<Module> {
    return this.modules;
  }

  findById(id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/module/' + id);
  }

  save(module:Module) {
    if (module) {
      if (module.id) {
        this.http.put('http://localhost:8080/api/module/' + module.id, module).subscribe(resp => {
          this.load()
          this.matiereHttpService.load();

          },
          err => console.log(err)
        );
      } else {
        this.http.post('http://localhost:8080/api/module/', module).subscribe(resp => this.load(),
          err => console.log(err));
      }
    }
  }

  delete(id:number) {
    this.http.delete('http://localhost:8080/api/module/' + id).subscribe(resp => this.load(),
      err => console.log(err));

  }

}
