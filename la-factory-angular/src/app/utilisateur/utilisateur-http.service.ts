import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Utilisateur} from '../model/utilisateur';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UtilisateurHttpService {

  private utilisateurs: any;

  constructor(private http: HttpClient) {
    this.load();
  }

  load() {
    this.http.get('http://localhost:8080/api/utilisateur').subscribe(resp => {
      this.utilisateurs = resp;
    }, err => console.log(err));
  }

  findAll(): Array<Utilisateur> {
    return this.utilisateurs;
  }

  findById(id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/utilisateur/' + id);
  }

  findByMail(mail: string): Observable<Object> {
    return this.http.get('http://localhost:8080/api/utilisateur/mail/' + mail);

  }

  save(utilisateur: Utilisateur) {
    if (utilisateur) {
      if (utilisateur.id) {
        this.http.put('http://localhost:8080/api/utilisateur/' + utilisateur.id, utilisateur).subscribe(resp => this.load(),
          err => console.log(err)
        );
      } else {
        this.http.post('http://localhost:8080/api/utilisateur/', utilisateur).subscribe(resp => this.load(),
          err => console.log(err));
      }
    }
  }

  delete(id: number) {
    this.http.delete('http://localhost:8080/api/utilisateur/' + id).subscribe(resp => this.load(),
      err => console.log(err));

  }
  findTypeUtilisateur(): Observable<Object> {
    return this.http.get('http://localhost:8080/api/utilisateur/typeutilisateur');
  }

}
