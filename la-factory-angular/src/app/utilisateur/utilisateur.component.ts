import { Component, OnInit } from '@angular/core';
import {Utilisateur} from '../model/utilisateur';
import {UtilisateurHttpService} from './utilisateur-http.service';

@Component({
  selector: 'utilisateur',
  templateUrl: './utilisateur.component.html',
  styleUrls: ['./utilisateur.component.css']
})
export class UtilisateurComponent implements OnInit {
  private search: string;
  private selectedUtilisateur: any = null;
  private types: any = null;
  private administrateurs: any = null;

  constructor(private utilisateurHttpService: UtilisateurHttpService) {
    this.typeUtilisateur();
  }

  ngOnInit() {
  }

  public filter(): Array<Utilisateur> {
    if (this.search) {
      return this.utilisateurHttpService.findAll().filter(f => f.typeUser.toLowerCase().indexOf(this.search.toLowerCase()) !== -1);
    } else {
      return this.utilisateurHttpService.findAll();
    }
  }

  add() {
    this.selectedUtilisateur = new Utilisateur();
  }

  edit(id: number) {
    this.utilisateurHttpService.findById(id).subscribe(resp => this.selectedUtilisateur = resp, err => console.log(err));
  }

  delete(id: number) {
    this.utilisateurHttpService.delete(id);
  }

  save() {
    this.utilisateurHttpService.save(this.selectedUtilisateur);
    this.selectedUtilisateur = null;
  }

  cancel() {
    this.selectedUtilisateur = null;
  }
  typeUtilisateur() {
    this.utilisateurHttpService.findTypeUtilisateur().subscribe(resp => this.types = resp, err => console.log(err));
  }
  // listAdministrateurs() {
  //   this.utilisateurHttpService.findAdministrateurs().subscribe(resp => this.administrateurs = resp, err => console.log(err));
  // }

}
