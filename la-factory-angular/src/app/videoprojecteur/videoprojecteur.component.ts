import {Component, OnInit} from '@angular/core';
import {Videoprojecteur} from '../model/videoprojecteur';
import {VideoprojecteurHttpService} from './videoprojecteur-http.service';
import {Formation} from '../model/formation';
import {MaterielFormation} from '../model/materielFormation';
import {FormationHttpService} from '../formation/formation-http.service';
import {MaterielFormationHttpService} from '../materiel-formation/materielFormation-http.service';


@Component({
  selector: 'videoprojecteur',
  templateUrl: './videoprojecteur.component.html',
  styleUrls: ['./videoprojecteur.component.css'],
  providers: [FormationHttpService]
})
export class VideoprojecteurComponent implements OnInit {
  private videoprojecteurForm: any = null;
  private videoprojecteurView: any = null;
  private videoprojecteurResa: any = null;
  private materielFormationCalendar: any = null;
  private resolutions: any = null;
  private videoprojecteurId: number;
  private formations: any = null;



  constructor(private videoprojecteurHttpService: VideoprojecteurHttpService, private formationHttpService: FormationHttpService, private materielFormationHttpService: MaterielFormationHttpService) {
    this.resolution();
    this.listFormation();
  }

  ngOnInit() {
  }

  list(): Array<Videoprojecteur> {
    return this.videoprojecteurHttpService.findAll();
  }

  listReservationById(id: number) {
    this.videoprojecteurId = id;

    return this.videoprojecteurHttpService.findMaterielFormationById(id).subscribe(resp => {
        this.videoprojecteurResa = resp;
    }, err => console.log(err));


  }

  add() {
    this.videoprojecteurForm = new Videoprojecteur();
  }

  addResa() {
    this.materielFormationCalendar = new MaterielFormation();
    if (this.materielFormationCalendar.formation == null) {
      this.materielFormationCalendar.formation = new Formation();
      }
    console.log(this.videoprojecteurId);
    if (this.materielFormationCalendar.materiel == null) {
      this.materielFormationCalendar.materiel = new Videoprojecteur();
      this.materielFormationCalendar.materiel.id = this.videoprojecteurId;
    }
  }

  edit(id: number) {
    this.videoprojecteurHttpService.findById(id).subscribe(resp => {
      this.videoprojecteurForm = resp;
      if (this.videoprojecteurForm.formation == null) {
        this.videoprojecteurForm.formation = new Formation();
      }
      if (this.videoprojecteurForm.materielFormation == null) {
        this.videoprojecteurForm.materielFormation = new MaterielFormation();
      }
    }, err => console.log(err));
  }

  editResa(id: number) {
    this.materielFormationHttpService.findById(id).subscribe(resp => {
      this.materielFormationCalendar = resp;
    }, err => console.log(err));

  }

  view(id: number) {
    this.videoprojecteurHttpService.findById(id).subscribe(resp => this.videoprojecteurView = resp, err => console.log(err));
  }

  delete(id: number) {
    this.videoprojecteurHttpService.delete(id);
  }

  deleteResa(id: number) {
    this.materielFormationHttpService.delete(id);
  }

  save() {
    this.videoprojecteurHttpService.save(this.videoprojecteurForm);
    this.materielFormationHttpService.save(this.materielFormationCalendar);
    this.videoprojecteurForm = null;

  }

  reserv(id: number) {

    this.materielFormationHttpService.save(this.materielFormationCalendar);

    this.materielFormationCalendar = null;
    this.videoprojecteurResa = this.videoprojecteurHttpService.findMaterielFormationById(id).subscribe(resp => {
      this.videoprojecteurResa = resp;
    }, err => console.log(err));
  }



  cancel() {
    this.videoprojecteurForm = null;
  }

  cancelResa() {
    this.materielFormationCalendar = null;
  }

  resolution() {
    this.videoprojecteurHttpService.findResolution().subscribe(resp => this.resolutions = resp, err => console.log(err));
  }

  listFormation() {
    this.formationHttpService.findAllFormation().subscribe(resp => this.formations = resp, err => console.log(err));
  console.log(this.formations);
  }



}
