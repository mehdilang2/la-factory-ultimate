import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Videoprojecteur} from '../model/videoprojecteur';
import {HttpClientModule} from '@angular/common/http';
import {MaterielFormation} from '../model/materielFormation';
import {MaterielFormationHttpService} from "../materiel-formation/materielFormation-http.service";

@Injectable()
export class VideoprojecteurHttpService {

  private videoprojecteurs: any;


  constructor(private http: HttpClient ) {
    this.load();
  }

  load() {
    this.http.get('http://localhost:8080/api/videoprojecteur').subscribe(resp => {
      this.videoprojecteurs = resp;
    }, err => console.log(err));
  }

  findAll(): Array<Videoprojecteur> {
    return this.videoprojecteurs;
  }

  findById(id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/videoprojecteur/' + id);
  }

  findMaterielFormationById(id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/videoprojecteur/' + id + '/materielFormation');
  }

  save(videoprojecteur: Videoprojecteur) {

    if (videoprojecteur) {
      if (videoprojecteur.id) {
        this.http.put('http://localhost:8080/api/videoprojecteur/' + videoprojecteur.id, videoprojecteur).subscribe(resp =>{

            this.load();
          },
          err => console.log(err)
        );
      } else {
        this.http.post('http://localhost:8080/api/videoprojecteur', videoprojecteur).subscribe(resp => this.load(),
          err => console.log(err));
      }
    }
  }

  delete(id: number) {
    this.http.delete('http://localhost:8080/api/videoprojecteur/' + id).subscribe(resp => this.load(),
      err => console.log(err));

  }
  findResolution(): Observable<object>{
    return this.http.get('http://localhost:8080/api/videoprojecteur/resolution');

  }

  reserv(materielFormation: MaterielFormation) {

    if (materielFormation) {
      if (materielFormation.id) {
        this.http.put('http://localhost:8080/api/materielFormation/' + materielFormation.id, materielFormation).subscribe(resp => this.load(),
          err => console.log(err)
        );
      } else {
        this.http.post('http://localhost:8080/api/materielFormation', materielFormation).subscribe(resp => this.load(),
          err => console.log(err));
      }
    }
  }

}
