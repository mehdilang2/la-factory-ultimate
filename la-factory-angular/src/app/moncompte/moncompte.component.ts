import { Component, OnInit } from '@angular/core';
import {UtilisateurHttpService} from '../utilisateur/utilisateur-http.service';
import {Utilisateur} from '../model/utilisateur';
import {TranslateService} from '@ngx-translate/core';
import {AuthService} from '../service/auth/auth.service';
import {ClientHttpService} from '../client/client-http.service';
import {Formation} from '../model/formation';
import {FormationHttpService} from '../formation/formation-http.service';

@Component({
  selector: 'moncompte',
  templateUrl: './moncompte.component.html',
  styleUrls: ['./moncompte.component.css'],
  providers: [AuthService]
})
export class MoncompteComponent implements OnInit {
  utilisateur: any;
  selectedClientContact: any;
  selectedFormation: any;
  private moduleList: any = null;

  constructor(public translate: TranslateService, private authService: AuthService, private utilisateurHttpService: UtilisateurHttpService, private clientHttpService: ClientHttpService, private formationHttpService: FormationHttpService) {
    translate.addLangs(['en', 'fr']);
    translate.setDefaultLang('fr');

    const browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/en|fr/) ? browserLang : 'fr');
    this.findContacts(this.getIdUser());
    this.findFormation(this.getIdUser());
  }

  ngOnInit() {

  }
  list(): Array<Formation> {
    return this.formationHttpService.findAll();
  }

  getIdUser() {
    return this.authService.getUser().id;
  }

  getTypeUser() {
    return this.authService.getUser().roles[0];
  }

  getMailUser() {
    return this.authService.getUser().mail;
  }

  getIdentite() {
    return this.authService.getUser().login;
  }

  hasAnyRole(roles: string[]) {
    return this.authService.hasAnyRole(roles);
  }

  listModules(id: number) {
    this.formationHttpService.findModules(id).subscribe(resp => this.moduleList = resp, err => console.log(err));
  }

  cancelContact() {
    this.moduleList = null;
  }

  findFormation(id: number) {
    console.log(id);
    this.formationHttpService.FindByIdFormateur(id).subscribe(resp =>{
      this.selectedFormation = resp;
      console.log(this.selectedFormation);
      }, err => console.log(err));
  }

  findContacts(id: number){
    this.clientHttpService.findContactsByIdClient(id).subscribe(resp =>{
      this.selectedClientContact = resp;
      console.log(this.selectedClientContact);
    }, err => console.log(err));

  }

}
