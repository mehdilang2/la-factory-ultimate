import { Component, OnInit } from '@angular/core';
import {ClientFormation} from '../model/client-formation';
import {ClientFormationHttpService} from './client-formation-http.service';
import {Client} from '../model/client';
import {FormationHttpService} from '../formation/formation-http.service';
import {ClientHttpService} from '../client/client-http.service';
import {AuthGuard} from "../service/auth/auth.guard";
import {AuthService} from "../service/auth/auth.service";
import {FormationComponent} from "../formation/formation.component";

@Component({
  selector: 'app-client-formation',
  templateUrl: './client-formation.component.html',
  styleUrls: ['./client-formation.component.css'],
  providers: [FormationHttpService, ClientHttpService,AuthService],
})
export class ClientFormationComponent implements OnInit {

  private clientFormationForm: any = null;
  private clientFormationCommentaire: any = null;
  private search: string;
  private searchformation: string;
  private searchclient: string;
  private clientList: any = null;


  constructor(private clientFormationHttpService: ClientFormationHttpService, private formationHttpService: FormationHttpService, private clientHttpService: ClientHttpService, private authService:AuthService, private formationComponent:FormationComponent) {
  }

  ngOnInit() {
  }

  list(): Array<ClientFormation> {
    return this.clientFormationHttpService.findAll();
  }

  add() {
    this.clientFormationForm = new ClientFormation();
  }

  edit(id: number) {
    this.clientFormationHttpService.findById(id).subscribe(resp => {
      this.clientFormationForm = resp;
      if (this.clientFormationForm.client == null) {
        this.clientFormationForm.client = new Client ();
      }
    }, err => console.log(err));
  }
  commentaire(id: number) {
    this.clientFormationHttpService.findById(id).subscribe(resp => {
      this.clientFormationCommentaire = resp;
      if (this.clientFormationCommentaire.client == null) {
        this.clientFormationCommentaire.client = new Client ();
      }
    }, err => console.log(err));
  }
  editCommentaire(id: number) {
    this.clientFormationHttpService.findById(id).subscribe(resp => {
      this.clientFormationForm = resp;
      if (this.clientFormationForm.client == null) {
        this.clientFormationForm.client = new Client ();
      }
    }, err => console.log(err));
  }

  filterformation() {
    if (this.searchformation) {
      return this.formationHttpService.findAll().filter(f => f.intitule.toLowerCase().indexOf(this.search.toLowerCase()) !== -1);
    } else {
      return this.formationHttpService.findAll();
    }
  }

  filterclient() {
    return this.clientHttpService.findAll();
    if (this.searchclient) {
      return this.clientHttpService.findAll().filter(c => c.nomEntreprise.toLowerCase().indexOf(this.searchclient.toLowerCase()) !== -1 ||  c.nomBusinessUnit.toLowerCase().indexOf(this.searchclient .toLowerCase()) !== -1 );
    } else {
      return this.clientHttpService.findAll();
    }
  }

  delete(id: number) {
    this.clientFormationHttpService.delete(id);
  }

  save() {
    this.clientFormationHttpService.save(this.clientFormationForm);
    this.clientFormationForm = null;
  }

  cancel() {
    this.clientFormationForm = null;
  }
  cancelViewClientFormation() {
   this.formationComponent.afficherClientFormation = false;
  }

  hasAnyRole(roles: string[]) {
    return this.authService.hasAnyRole(roles);
  }

}
