import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {AdministrateurComponent} from './administrateur/administrateur.component';
import {GestionnaireComponent} from './gestionnaire/gestionnaire.component';
import {SalleComponent} from './salle/salle.component';
import {VideoprojecteurComponent} from './videoprojecteur/videoprojecteur.component';
import {MaterielFormationComponent} from './materiel-formation/materiel-formation.component';
import {OrdinateurComponent} from './ordinateur/ordinateur.component';
import {MatiereComponent} from './matiere/matiere.component';
import {ClientComponent} from './client/client.component';
import {FormationComponent} from './formation/formation.component';
import {ModuleComponent} from './module/module.component';
import {FormateurComponent} from './formateur/formateur.component';
import {DisponibiliteComponent} from './disponibilite/disponibilite.component';
import {StagiaireComponent} from './stagiaire/stagiaire.component';
import {ClientFormationComponent} from './client-formation/client-formation.component';
import {ContactComponent} from './contact/contact.component';
import {UtilisateurComponent} from './utilisateur/utilisateur.component';
import {PresenceComponent} from './presence/presence.component';
import {GestionnaireFormationComponent} from './gestionnaire-formation/gestionnaire-formation.component';
import {PresenceStagiaireComponent} from './presence-stagiaire/presence-stagiaire.component';
import {MoncompteComponent} from './moncompte/moncompte.component';
import {LoginComponent} from './login/login.component';
import {AuthGuard} from './service/auth/auth.guard';
import {ResetmdpComponent} from './resetmdp/resetmdp.component';
import {MdpforgetComponent} from './mdpforget/mdpforget.component';
import {StagiaireFormationComponent} from "./stagiaire-formation/stagiaire-formation.component";


const routes: Routes = [
  {path: '', redirectTo:'/login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'home',canActivate: [AuthGuard], component: HomeComponent},
  {path: 'administrateur',canActivate:[AuthGuard], component: AdministrateurComponent, data: {roles:['administrateur']}},
  {path: 'gestionnaire',canActivate:[AuthGuard], component: GestionnaireComponent , data: {roles:['administrateur']}},
  {path: 'matiere', canActivate:[AuthGuard], component: MatiereComponent , data:{roles:['administrateur', 'formateur','gestionnaire']}},
  {path: 'client', canActivate:[AuthGuard], component: ClientComponent, data: {roles:['administrateur', 'gestionnaire']}},
  {path: 'formation',canActivate:[AuthGuard], component: FormationComponent, data: {roles:['stagiaire', 'administrateur', 'gestionnaire','client']}},
  {path: 'module', component: ModuleComponent},
  {path: 'formateur',canActivate:[AuthGuard], component: FormateurComponent, data: {roles:['administrateur', 'gestionnaire']}},
  {path: 'stagiaire',canActivate:[AuthGuard], component: StagiaireComponent, data: {roles:['administrateur', 'gestionnaire', 'client']}},
  {path: 'materielFormation', component: MaterielFormationComponent},
  {path: 'ordinateur',canActivate:[AuthGuard], component: OrdinateurComponent, data: {roles:['administrateur', 'gestionnaire']}},
  {path: 'salle',canActivate:[AuthGuard], component: SalleComponent, data: {roles:['administrateur', 'gestionnaire']}},
  {path: 'videoprojecteur',canActivate:[AuthGuard], component: VideoprojecteurComponent, data: {roles:['administrateur', 'gestionnaire']}},
  {path: 'formateur', component: FormateurComponent},
  {path: 'disponibilite', component: DisponibiliteComponent},
  {path: 'client-formation',canActivate:[AuthGuard], component: ClientFormationComponent,data: {roles:['administrateur', 'gestionnaire','client']}},
  {path: 'utilisateur',canActivate:[AuthGuard], component: UtilisateurComponent, data: {roles:['administrateur']}},
  {path: 'contact', component: ContactComponent},
  {path : 'presence', component : PresenceComponent},
  {path : 'presenceStagiaire', component : PresenceStagiaireComponent},
  {path : 'gestionnaire-formation', component : GestionnaireFormationComponent},
  {path : 'moncompte', canActivate:[AuthGuard], component : MoncompteComponent , data: {roles:['administrateur', 'gestionnaire','formateur', 'client','stagiaire']}},
  {path : 'gestionnaire-formation', component : GestionnaireFormationComponent},
  {path : 'resetmdp', component: ResetmdpComponent},
  {path : 'mdpforget', component: MdpforgetComponent},
  {path : 'stagiaire-formation', canActivate:[AuthGuard], component: StagiaireFormationComponent, data: {roles:['stagiaire']}},
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule { }
