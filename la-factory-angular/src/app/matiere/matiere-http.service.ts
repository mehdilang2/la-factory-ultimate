import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Matiere} from '../model/matiere';
import {Observable} from 'rxjs';
import {Formateur} from '../model/formateur';

@Injectable()
export class MatiereHttpService {

  private matieres: any;

  constructor(private http: HttpClient) {
    this.load();
  }

  load() {
    this.http.get('http://localhost:8080/api/matiere').subscribe(resp => {
      this.matieres = resp;
    }, err => console.log(err));
  }

  findAll(): Array<Matiere> {
    return this.matieres;
  }

  findById(id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/matiere/' + id);
  }

  findByIdFormateur(id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/formateur/' + id + '/matieres');
  }

  save(matiere: Matiere) {
    if (matiere) {
      if (matiere.id) {
        this.http.put('http://localhost:8080/api/matiere/' + matiere.id, matiere).subscribe(resp => this.load(),
          err => console.log(err)
        );
      } else {
        this.http.post('http://localhost:8080/api/matiere', matiere).subscribe(resp => this.load(),
          err => console.log(err));
      }
    }
  }

  delete(id: number) {
    this.http.delete('http://localhost:8080/api/matiere/' + id).subscribe(resp => this.load(),
      err => console.log(err));

  }

  findFormateurs(id: number) {
    return this.http.get('http://localhost:8080/api/matiere/' + id + '/formateurs');
  }

}
