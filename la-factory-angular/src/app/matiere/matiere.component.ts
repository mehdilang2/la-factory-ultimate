import { Component, OnInit } from '@angular/core';
import {Matiere} from '../model/matiere';
import {MatiereHttpService} from './matiere-http.service';
import {FormateurHttpService} from '../formateur/formateur-http.service';
import {Formateur} from '../model/formateur';
import {Module} from '../model/module';
import {Formation} from '../model/formation';
import {AuthService} from '../service/auth/auth.service';
import {FormateurComponent} from "../formateur/formateur.component";

@Component({
  selector: 'app-matiere',
  templateUrl: './matiere.component.html',
  styleUrls: ['./matiere.component.css'],
  providers: [AuthService,FormateurComponent]
})
export class MatiereComponent implements OnInit {

  private matiereForm: any = null
  private matiereDetail: any = null;
  private search: string;
  private formateurForm: any = null
  private formateurList: any = null;
  private matiereList:any = null;


  constructor(private matiereHttpService: MatiereHttpService, private formateurHttpService: FormateurHttpService, private authService: AuthService, private formateurComponent: FormateurComponent) {
    this.listByIdFormateur(this.getIdUser());
    this.formateurComponent.detail(this.getIdUser());
  }

  ngOnInit() {
  }

  list(): Array<Matiere> {
    return this.matiereHttpService.findAll();
  }



  getIdUser() {
    return this.authService.getUser().id;
  }

  listByIdFormateur(id:number) {
    return this.matiereHttpService.findByIdFormateur(id).subscribe(resp => this.matiereList = resp, err => console.log(err));;
  }

  add() {
    this.matiereForm = new Matiere();
  }

  detail(id: number) {
    this.matiereHttpService.findById(id).subscribe(resp => this.matiereDetail = resp, err => console.log(err));
  }

  edit(id: number) {
    this.matiereHttpService.findById(id).subscribe(resp => this.matiereForm = resp, err => console.log(err));
  }

  delete(id: number) {
    this.matiereHttpService.delete(id);
  }

  save() {
    this.matiereHttpService.save(this.matiereForm);
    this.matiereForm = null;
  }

  cancel() {
    this.matiereForm = null;
  }

  filter() {
    if (this.search) {
      // tslint:disable-next-line:max-line-length
      return this.formateurHttpService.findAll().filter(f => f.nom.toLowerCase().indexOf(this.search.toLowerCase()) !== -1 ||  f.prenom.toLowerCase().indexOf(this.search.toLowerCase()) !== -1 );
    } else {
      return this.formateurHttpService.findAll();
    }
  }

  listFormateur(id: number){
    this.matiereHttpService.findFormateurs(id).subscribe(resp => this.formateurList = resp, err => console.log(err));
  }


  hasAnyRole(roles: string[]) {
    return this.authService.hasAnyRole(roles);
  }

  showEditMatiere(id:number){
    console.log(this.getIdUser());
    // this.formateurComponent.detail(this.getIdUser());
    this.formateurHttpService.findById(id).subscribe(resp => this.formateurComponent.formateurDetail = resp, err => console.log(err));
    this.formateurComponent.matieresSelect(id);
    this.formateurHttpService.findAllForDragAndDrop(id).subscribe(resp => {
      this.formateurComponent.allCompetences = resp;
    }, err => console.log(err));
    this.formateurComponent.formateurCalendar = null;
    this.formateurForm = null;
  }
  cancelformateurlist(){
    this.formateurList = null;
  }

}
