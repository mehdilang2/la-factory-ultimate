import {AfterViewInit, Component, ElementRef, OnInit, ViewChild,} from '@angular/core';
import {DisponibiliteHttpService} from './disponibilite-http.service';
import {Disponibilite} from '../model/disponibilite';
import {FullCalendarComponent} from '@fullcalendar/angular';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin, {Draggable} from '@fullcalendar/interaction';
import {EventInput} from '@fullcalendar/core/structs/event';
import timeGrigPlugin from '@fullcalendar/timegrid';
import {FormationHttpService} from "../formation/formation-http.service";
import {AuthService} from "../service/auth/auth.service";
import {FormateurHttpService} from "../formateur/formateur-http.service";
import {Subject} from "rxjs";
import {startOfDay} from "@fullcalendar/core";

// @ts-ignore
@Component({
  selector: 'app-disponibilite',
  templateUrl: './disponibilite.component.html',
  styleUrls: ['./disponibilite.component.css'],
  providers: [AuthService]
})
export class DisponibiliteComponent implements OnInit, AfterViewInit {

  private disponibilite: Array<Disponibilite> = new Array<Disponibilite>();
  private search: string;
  private disponibiliteForm: any = null;
  private Formateur: any = null;
  options: any;
  eventsModel: any = null;
  events: any = new Array;
  private dispoFormateur: any

  private formateurCalendar: any = null;


  el: any;
  dayDispo: Array<Date> = [];
  @ViewChild('fullcalendar', {static: false}) fullcalendar: FullCalendarComponent;
  @ViewChild('external', {static: false}) external: ElementRef;

  calendarVisible = true;
  calendarPlugins = [dayGridPlugin, timeGrigPlugin, interactionPlugin];
  calendarWeekends = true;
  calendarEvents: EventInput[] = [
    {'start': new Date()}
  ];

  constructor(private disponibiliteHttpService: DisponibiliteHttpService,
              private formateurHttpService: FormateurHttpService,
              private authService: AuthService) {
  }

  ngAfterViewInit() {
    this.options = {
      editable: true,
      selectable: true,
      customButtons: {
        myCustomButton: {
          text: 'custom!',
          click: function () {
            alert('clicked the custom button!');
          }
        }
      },
      theme: 'standart',
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay'
      },
      // add other plugins
      plugins: [dayGridPlugin, interactionPlugin, timeGridPlugin]
    };
  }

  ngOnInit() {


  }


  list(): Array<Disponibilite> {
    return this.disponibiliteHttpService.findAll();
  }

  add() {
    this.disponibiliteForm = new Disponibilite();
  }

  edit(id: number) {
    this.disponibiliteHttpService.findById(id).subscribe(resp => this.disponibiliteForm = resp, err => console.log(err));
  }

  delete(id: number) {
    this.disponibiliteHttpService.delete(id);
  }

  //save() {
  // for(let i = 0; i< this.dayDispo.length ; i++) {
  // let dispo: Disponibilite =  new Disponibilite(null,null,this.dayDispo[i]);
  // this.disponibiliteHttpService.save(dispo);
  //  console.log('............');
  //console.log(dispo);
  // }
  //  this.dayDispo = null;
  //}

  cancel() {
    this.disponibiliteForm = null;
  }

  hasAnyRole(roles: string[]) {
    return this.authService.hasAnyRole(roles);
  }

  getIdUser() {
    return this.authService.getUser().id;
  }

  //FONCTIONS DU CALENDRIER

  cancelCalendar() {
    this.formateurCalendar = null;
  }


  calendar(id: number) {

    this.disponibiliteHttpService.findByFormateur(id).subscribe(resp => {
      this.dispoFormateur = resp;
      for (let i of this.dispoFormateur) {
        this.events.push({ start : i.dtDispo, title : 'Disponible'});
      }

    }, err => console.log(err))


    this.formateurHttpService.findById(id).subscribe(resp => {
      this.formateurCalendar = resp;
      console.log(this.formateurCalendar);
    }, err => console.log(err));

  }

  saveCalendar(idFormateur: number) {
    for (let i = 0; i < this.dayDispo.length; i++) {
      let dispo: Disponibilite = new Disponibilite(null, null, this.dayDispo[i], this.formateurCalendar);
      this.disponibiliteHttpService.save(dispo);
      console.log(dispo);
    }
    this.dayDispo = null;
    this.formateurCalendar = null;
  }

  selectCalendar(obj) {

    let trouducStart1 = obj.start.getFullYear();
    let trouducStart2 = obj.start.getMonth();
    let trouducStart3 = obj.start.getDate() + 1;
    let dateStart: Date = new Date(trouducStart1, trouducStart2, trouducStart3);

    let trouducEnd1 = obj.end.getFullYear();
    let trouducEnd2 = obj.end.getMonth();
    let trouducEnd3 = obj.end.getDate() + 1;
    let dateEnd: Date = new Date(trouducEnd1, trouducEnd2, trouducEnd3);

    let d: any = null;
    for (d = dateStart; d < dateEnd; d.setDate(d.getDate() + 1)) {
      this.dayDispo.push(new Date(d));
    }
    console.log(this.dayDispo);
  }

  eventDragStop(model) {
  }


  dateClick(model) {
    console.log(model);
  }

  updateHeader() {
    this.options.header = {
      left: 'prev,next myCustomButton',
      center: 'title',
      right: ''
    };
  }

  updateEvents() {
    this.eventsModel = [{
      title: 'Updaten Event',
      start: this.yearMonth + '-08',
      end: this.yearMonth + '-10'
    }];
  }

  get yearMonth(): string {
    const dateObj = new Date();
    return dateObj.getUTCFullYear() + '-' + (dateObj.getUTCMonth() + 1) + (dateObj.getUTCDay() + 1);
  }

  dayRender(ev) {
    ev.el.addEventListener('dblclick', () => {
      alert('double click!');
    });
  }

  eventClick(eventsModel) {
    alert(eventsModel);
  }

}
