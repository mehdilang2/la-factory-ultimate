import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Disponibilite} from '../model/disponibilite';

@Injectable()
export class DisponibiliteHttpService {

  private disponibilites: any;

  constructor(private http: HttpClient) {
    this.load();
  }

  load() {
    this.http.get('http://localhost:8080/api/disponibilite').subscribe(resp => {
      this.disponibilites = resp;
    }, err => console.log(err));
  }

  findAll(): Array<Disponibilite> {
    return this.disponibilites;
  }

  findById(id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/disponibilite/' + id);
  }

  findByFormateur(id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/disponibilite/' + id + '/formateur');
  }

  save(disponibilite: Disponibilite) {
    if (disponibilite) {
      if (disponibilite.id) {
        this.http.put('http://localhost:8080/api/disponibilite/' + disponibilite.id, disponibilite).subscribe(resp => this.load(),
          err => console.log(err)
        );
      } else {
        this.http.post('http://localhost:8080/api/disponibilite', disponibilite).subscribe(resp => this.load(),
          err => console.log(err));
      }
    }
  }

  delete(id: number) {
    this.http.delete('http://localhost:8080/api/disponibilite/' + id).subscribe(resp => this.load(),
      err => console.log(err));

  }
}
