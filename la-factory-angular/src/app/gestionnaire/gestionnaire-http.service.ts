import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Gestionnaire} from '../model/gestionnaire';

@Injectable()
export class GestionnaireHttpService {

  private gestionnaires: any;

  constructor(private http: HttpClient) {
    this.load();
  }

  load() {
    this.http.get('http://localhost:8080/api/gestionnaire').subscribe(resp => {
      this.gestionnaires = resp;
    }, err => console.log(err));
  }

  findAll(): Array<Gestionnaire> {
    return this.gestionnaires;
  }

  findById(id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/gestionnaire/' + id);
  }

  save(gestionnaire: Gestionnaire) {
    if (gestionnaire) {
      if (gestionnaire.id) {
        this.http.put('http://localhost:8080/api/gestionnaire/' + gestionnaire.id, gestionnaire).subscribe(resp => this.load(),
          err => console.log(err)
        );
      } else {
        this.http.post('http://localhost:8080/api/gestionnaire', gestionnaire).subscribe(resp => this.load(),
          err => console.log(err));
      }
    }
  }

  delete(id: number) {
    this.http.delete('http://localhost:8080/api/gestionnaire/' + id).subscribe(resp => this.load(),
      err => console.log(err));

  }
  findCivilite(): Observable<Object> {
    return this.http.get('http://localhost:8080/api/gestionnaire/civilites');
  }

}
