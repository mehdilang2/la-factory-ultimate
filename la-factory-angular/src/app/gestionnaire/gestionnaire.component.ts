import { Component, OnInit } from '@angular/core';
import {GestionnaireHttpService} from './gestionnaire-http.service';
import {Gestionnaire} from '../model/gestionnaire';
import {Adresse} from '../model/adresse';
import {Utilisateur} from '../model/utilisateur';

@Component({
  selector: 'gestionnaire',
  templateUrl: './gestionnaire.component.html',
  styleUrls: ['./gestionnaire.component.css']
})
export class GestionnaireComponent implements OnInit {
  private utilisateur: any = null;
  private gestionnaireForm: any = null;
  private civilites: any = null;
  private  search: string;

  constructor(private gestionnaireHttpService: GestionnaireHttpService) {
    this.civilite();
    console.log(this.civilites);
  }
  ngOnInit() {
  }

  public filtergestionnaire() {
    if (this.search) {
      return this.gestionnaireHttpService.findAll().filter(s => s.nom.toLowerCase().indexOf(this.search.toLowerCase()) !== -1 || s.prenom.toLowerCase().indexOf(this.search.toLowerCase()) !== -1);
    } else {
      return this.gestionnaireHttpService.findAll();
    }
  }

  list(): Array<Gestionnaire> {
    return this.gestionnaireHttpService.findAll();
  }

  add() {
    this.gestionnaireForm = new Gestionnaire();
  }

  edit(id: number) {
    this.gestionnaireHttpService.findById(id).subscribe(resp => {this.gestionnaireForm = resp;
    if (this.gestionnaireForm.adresse == null) {
    this.gestionnaireForm.adresse = new Adresse();
    }}, err => console.log(err));
  }

  delete(id: number) {
    this.gestionnaireHttpService.delete(id);
  }

  save() {
    if(!this.gestionnaireForm.utilisateur){
      this.utilisateur = new Utilisateur('Gestionnaire');
      this.gestionnaireForm.utilisateur = this.utilisateur;
      this.utilisateur = null;
    }
    this.gestionnaireHttpService.save(this.gestionnaireForm);
    this.gestionnaireForm = null;
  }

  cancel() {
    this.gestionnaireForm = null;
  }
  civilite() {
    this.gestionnaireHttpService.findCivilite().subscribe(resp => this.civilites = resp, err => console.log(err));
  }

}
