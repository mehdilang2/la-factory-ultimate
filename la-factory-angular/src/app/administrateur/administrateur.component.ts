import { Component, OnInit } from '@angular/core';
import {Administrateur} from '../model/administrateur';
import {AdministrateurHttpService} from './administrateur-http.service';
import {Adresse} from '../model/adresse';
import {Utilisateur} from '../model/utilisateur';


@Component({
  selector: 'app-administrateur',
  templateUrl: './administrateur.component.html',
  styleUrls: ['./administrateur.component.css']
})
export class AdministrateurComponent implements OnInit {
  private utilisateur: any = null;
  private administrateurForm: any = null;
  private civilites: any = null;

  private  search: string;
  constructor(private administrateurHttpService: AdministrateurHttpService) {
    this.civilite();
  }
  ngOnInit() {
  }
  public filterAdministrateur() {
    if (this.search) {
      return this.administrateurHttpService.findAll().filter(s => s.nom.toLowerCase().indexOf(this.search.toLowerCase()) !== -1 || s.prenom.toLowerCase().indexOf(this.search.toLowerCase()) !== -1);
    } else {
      return this.administrateurHttpService.findAll();
    }
  }

  list(): Array<Administrateur> {
    return this.administrateurHttpService.findAll();
  }

  add() {
    this.administrateurForm = new Administrateur();
  }

  edit(id: number) {
    this.administrateurHttpService.findById(id).subscribe(resp => {this.administrateurForm = resp;
    if (this.administrateurForm.adresse == null){
    this.administrateurForm.adresse = new Adresse();
    }}, err => console.log(err));
  }

  delete(id: number) {
    this.administrateurHttpService.delete(id);
  }

  save() {
    if(!this.administrateurForm.utilisateur){
      this.utilisateur = new Utilisateur('Administrateur');
      this.administrateurForm.utilisateur = this.utilisateur;
      this.utilisateur = null;
    }
    this.administrateurHttpService.save(this.administrateurForm);
    this.administrateurForm = null;
  }

  cancel() {
    this.administrateurForm = null;
  }

  civilite() {
    this.administrateurHttpService.findCivilite().subscribe(resp => this.civilites = resp, err => console.log(err));
  }

}
