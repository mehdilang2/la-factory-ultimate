import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Administrateur} from '../model/administrateur';

@Injectable()
export class AdministrateurHttpService {

  private administrateurs: any;


  constructor(private http: HttpClient) {
    this.load();
  }

  load() {
    this.http.get('http://localhost:8080/api/administrateur').subscribe(resp => {
      this.administrateurs = resp;
    }, err => console.log(err));
  }

  findAll(): Array<Administrateur> {
    return this.administrateurs;
  }

  findById(id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/administrateur/' + id);
  }

  findCivilite(): Observable<Object> {
    return this.http.get('http://localhost:8080/api/administrateur/civilites');
  }

  save(administrateur: Administrateur) {
    if (administrateur) {
      if (administrateur.id) {
        this.http.put('http://localhost:8080/api/administrateur/' + administrateur.id, administrateur).subscribe(resp => this.load(),
          err => console.log(err)
        );
      } else {
        this.http.post('http://localhost:8080/api/administrateur', administrateur).subscribe(resp => this.load(),
          err => console.log(err));
      }
    }
  }

  delete(id: number) {
    this.http.delete('http://localhost:8080/api/administrateur/' + id).subscribe(resp => this.load(),
      err => console.log(err));

  }

}
