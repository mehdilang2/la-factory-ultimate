import { Component, OnInit } from '@angular/core';
import {ClientFormation} from '../model/client-formation';
import {ClientFormationHttpService} from '../client-formation/client-formation-http.service';
import {Client} from "../model/client";


@Component({
  selector: 'app-gestionnaire-formation',
  templateUrl: './gestionnaire-formation.component.html',
  styleUrls: ['./gestionnaire-formation.component.css']
})
export class GestionnaireFormationComponent implements OnInit {

  private placesvalidees: any = null;
  private commentaire: any = null;
  constructor(private gestionnaireFormationHttpService: ClientFormationHttpService) {
  }

  ngOnInit() {
  }
  list(): Array<ClientFormation> {
    return this.gestionnaireFormationHttpService.findAll();
  }

  edit(id: number) {
    this.gestionnaireFormationHttpService.findById(id).subscribe(resp => this.placesvalidees = resp, err => console.log(err));
  }
  save() {
    this.gestionnaireFormationHttpService.save(this.placesvalidees);
    this.placesvalidees = null;
  }
  cancel() {
    this.placesvalidees = null;
  }
  affichage(id: number){
    this.gestionnaireFormationHttpService.findById(id).subscribe(resp => {
      this.commentaire = resp;
      if (this.commentaire.client == null) {
        this.commentaire.client = new Client ();
      }
    }, err => console.log(err));
  }
}
