import {Injectable} from '@angular/core';

import {ClientFormation} from '../model/client-formation';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class GestionnaireFormationHttpService {

  private gestionnaireFormations: any;

  constructor(private http: HttpClient) {
    this.load();
  }

  load() {
    this.http.get('http://localhost:8080/api/clientFormation').subscribe(resp => {
      this.gestionnaireFormations = resp;
    }, err => console.log(err));
  }

  findAll(): Array<ClientFormation> {
    return this.gestionnaireFormations;
  }

  findById(id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/clientFormation/' + id);
  }

  save(clientFormation: ClientFormation) {
    if (clientFormation) {
      if (clientFormation.id) {
        this.http.put('http://localhost:8080/api/clientFormation/' + clientFormation.id, clientFormation).subscribe(resp => this.load(),
          err => console.log(err)
        );
      } else {
        this.http.post('http://localhost:8080/api/clientFormation', clientFormation).subscribe(resp => this.load(),
          err => console.log(err));
      }
    }
  }

  delete(id:number) {
    this.http.delete('http://localhost:8080/api/clientFormation/' + id).subscribe(resp => this.load(),
      err => console.log(err));

  }

}
