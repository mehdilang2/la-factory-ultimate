import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {AppRoutingModule} from './app-routing.module';
import {HomeComponent} from './home/home.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { AdministrateurComponent } from './administrateur/administrateur.component';
import { GestionnaireComponent } from './gestionnaire/gestionnaire.component';
import {GestionnaireHttpService} from './gestionnaire/gestionnaire-http.service';
import {AdministrateurHttpService} from './administrateur/administrateur-http.service';
import { VideoprojecteurComponent } from './videoprojecteur/videoprojecteur.component';
import { OrdinateurComponent } from './ordinateur/ordinateur.component';
import { SalleComponent } from './salle/salle.component';
import { MaterielFormationComponent } from './materiel-formation/materiel-formation.component';
import {MatiereComponent} from './matiere/matiere.component';
import {MatiereHttpService} from './matiere/matiere-http.service';
import { ClientComponent } from './client/client.component';
import {ClientHttpService} from './client/client-http.service';
import { StagiaireComponent } from './stagiaire/stagiaire.component';
import {FormationComponent} from './formation/formation.component';
import {FormationHttpService} from './formation/formation-http.service';
import {ModuleHttpService} from './module/module-http.service';
import {ModuleComponent} from './module/module.component';
import {FormateurComponent} from './formateur/formateur.component';
import {FormateurHttpService} from './formateur/formateur-http.service';
import {DisponibiliteHttpService} from './disponibilite/disponibilite-http.service';
import {DisponibiliteComponent} from './disponibilite/disponibilite.component';
import {VideoprojecteurHttpService} from './videoprojecteur/videoprojecteur-http.service';
import {OrdinateurHttpService} from './ordinateur/ordinateur-http.service';
import {SalleHttpService} from './salle/salle-http.service';
import {MaterielFormationHttpService} from './materiel-formation/materielFormation-http.service';
import { ClientFormationComponent } from './client-formation/client-formation.component';
import {ClientFormationHttpService} from './client-formation/client-formation-http.service';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {StagiaireHttpService} from './stagiaire/stagiaire-http.service';
import { UtilisateurComponent } from './utilisateur/utilisateur.component';
import { ContactComponent } from './contact/contact.component';
import {ContactHttpService} from './contact/contact-http.service';
import {MatPaginatorModule, MatTableModule} from '@angular/material';
import {NgbDatepickerModule} from '@ng-bootstrap/ng-bootstrap';
import {CdkTableExporterModule, MatTableExporterModule} from 'mat-table-exporter';
import {PresenceComponent} from './presence/presence.component';
import {PresenceHttpService} from './presence/presence-http.service';
import { FullCalendarModule } from '@fullcalendar/angular';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { GestionnaireFormationComponent } from './gestionnaire-formation/gestionnaire-formation.component';
import { PresenceStagiaireComponent } from './presence-stagiaire/presence-stagiaire.component';
import {PresenceStagiaireHttpService} from './presence-stagiaire/presence-stagiaire-http.service';
import {ResetmdpComponent } from './resetmdp/resetmdp.component';
import { LoginComponent } from './login/login.component';
import {AuthService} from './service/auth/auth.service';
import { MoncompteComponent } from './moncompte/moncompte.component';
import { MdpforgetComponent } from './mdpforget/mdpforget.component';
import { StagiaireFormationComponent } from './stagiaire-formation/stagiaire-formation.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AdministrateurComponent,
    GestionnaireComponent,
    VideoprojecteurComponent,
    OrdinateurComponent,
    SalleComponent,
    MaterielFormationComponent,
    MatiereComponent,
    FormationComponent,
    ModuleComponent,
    ClientComponent,
    StagiaireComponent,
    FormateurComponent,
    DisponibiliteComponent,
    ClientFormationComponent,
    UtilisateurComponent,
    PresenceComponent,
    ContactComponent,
    GestionnaireFormationComponent,
    PresenceStagiaireComponent,
    ResetmdpComponent,
    PresenceStagiaireComponent,
    LoginComponent,
    MoncompteComponent,
    MdpforgetComponent,
    StagiaireFormationComponent
  ],
  imports: [
    BrowserModule,
    FullCalendarModule, // import FullCalendar module
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    MatPaginatorModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    NgbDatepickerModule,
    CdkTableExporterModule,
    MatTableExporterModule,
    MatTableModule,
    DragDropModule
  ],

  providers: [MatiereHttpService,
    FormationHttpService,
    ModuleHttpService,
    FormateurHttpService,
    ClientHttpService,
    GestionnaireHttpService,
    AdministrateurHttpService,
    DisponibiliteHttpService,
    VideoprojecteurHttpService,
    OrdinateurHttpService,
    SalleHttpService,
    MaterielFormationHttpService,
    StagiaireHttpService,
    ClientFormationHttpService,
    ContactHttpService,
    PresenceHttpService,
    PresenceStagiaireHttpService,
    AuthService
  ],

  bootstrap: [AppComponent]
})
export class AppModule {
  private static HttpLoaderFactory: any;
}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
