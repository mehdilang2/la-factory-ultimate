import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Formation} from '../model/formation';
import {Formateur} from "../model/formateur";

@Injectable()
export class FormationHttpService {

  private formations: any;
  private datejour = new Date();
  private dtdebut: any;
  private dtfin: any;
  private stagiaires: any;
  private id: any;

  constructor(private http: HttpClient) {
    this.load();
  }

  load() {
    this.http.get('http://localhost:8080/api/formation').subscribe(resp => {
      this.formations = resp;
      for (let i in this.formations) {
        if (this.formations[i].dtDebut != null) {
          this.dtdebut = new Date(this.formations[i].dtDebut);
          this.dtfin = new Date(this.formations[i].dtFin);
          if (this.dtdebut > this.datejour) {
            this.formations[i].statut = 'formation à venir';
          } else if (this.dtdebut < this.datejour && this.datejour < this.dtfin) {
            this.formations[i].statut = 'formation en cours';
          } else {
            this.formations[i].statut = 'formation terminée';
          }
          // this.http.put('http://localhost:8080/api/formation/' + this.formations[i].id, this.formations[i]);
        }
      }
      console.log(this.formations);
    }, err => console.log(err));
  }

  findAll(): Array<Formation> {
    return this.formations;
  }

  findAllFormation(): Observable<Object> {
    return this.http.get('http://localhost:8080/api/formation/');
  }

  findById(id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/formation/' + id);
  }

  FindByIdFormateur(id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/formateur/' + id + '/formations');
  }

  findAllWithoutFormationForDragAndDrop(id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/formation/' + id + '/stagiairesAll');
  }

  findStagiaires (id: number) {
    return this.http.get('http://localhost:8080/api/formation/' + id + '/stagiaires');
  }

  findFormateur (id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/formation/' + id + '/formateur');
  }

  save(formation: Formation) {
    if (formation) {
      if (formation.id) {
        this.http.put('http://localhost:8080/api/formation/' + formation.id, formation).subscribe(resp => {
          this.load()
          },
          err => console.log(err)
        );
      } else {
        this.http.post('http://localhost:8080/api/formation', formation).subscribe(resp => this.load(),
          err => console.log(err));
      }
    }
  }

  updateFormation(formation: Formation) {

    this.http.put('http://localhost:8080/api/formation/' + formation.id, formation).subscribe(resp => this.load(),
      err => console.log(err)
    );
  }

  delete(id: number) {
    this.http.delete('http://localhost:8080/api/formation/' + id).subscribe(resp => this.load(),
      err => console.log(err));

  }

  deleteStagiaireFormation(id: number) {
    this.http.delete('http://localhost:8080/api/formation/' + id + '/byFormation').subscribe(resp => this.load(),
      err => console.log(err));

  }

  findClients(id: number) {
    return this.http.get('http://localhost:8080/api/formation/' + id + '/clients');
  }

  findModules(id: number) {
    return this.http.get('http://localhost:8080/api/formation/' + id + '/modules');
  }

  findAllByFormation(id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/formation/' + id + '/stagiaires');
  }
  findVideoprojecteur(id: number): Observable<Object>  {
    return this.http.get('http://localhost:8080/api/formation/' + id + '/materiel/videoprojecteur');
  }
  findSalle(id: number): Observable<Object>  {
    return this.http.get('http://localhost:8080/api/formation/' + id + '/materiel/salle');
  }
  findOrdinateur(id: number): Observable<Object>  {
    console.log(id);
    return this.http.get('http://localhost:8080/api/formation/' + id + '/materiel/ordinateur');
  }

}
