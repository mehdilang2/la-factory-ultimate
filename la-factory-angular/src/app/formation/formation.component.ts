import { Component, OnInit } from '@angular/core';
import {FormationHttpService} from './formation-http.service';
import {StagiaireHttpService} from 'src/app/stagiaire/stagiaire-http.service';
import {Formation} from '../model/formation';
import {Stagiaire} from '../model/stagiaire';
import {OrdinateurHttpService} from '../ordinateur/ordinateur-http.service';
import {FormateurHttpService} from '../formateur/formateur-http.service';
import {Client} from '../model/client';
import {Formateur} from '../model/formateur';
import {ClientHttpService} from '../client/client-http.service';
import {ClientFormation} from '../model/client-formation';
import {ClientFormationHttpService} from '../client-formation/client-formation-http.service';
import {ModuleHttpService} from '../module/module-http.service';
import {Module} from '../model/module';
import {MatiereComponent} from '../matiere/matiere.component';
import {MatiereHttpService} from '../matiere/matiere-http.service';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from "@angular/cdk/drag-drop";
import {StagiaireFormation} from "../model/stagiaire-formation";
import {MaterielFormation} from "../model/materielFormation";
import {StagiaireFormationHttpService} from "../stagiaire-formation/stagiaire-formation-http.service";
import {AuthService} from '../service/auth/auth.service';
import {Gestionnaire} from '../model/gestionnaire';
import {GestionnaireHttpService} from '../gestionnaire/gestionnaire-http.service';
import {Adresse} from '../model/adresse';
import {Ordinateur} from '../model/ordinateur';

@Component({
  selector: 'app-formation',
  templateUrl: './formation.component.html',
  styleUrls: ['./formation.component.css'],
  providers: [FormationHttpService, FormationHttpService, AuthService, StagiaireFormationHttpService],
})
export class FormationComponent implements OnInit {

  private formations: Array<Formation> = new Array<Formation>();
  private search: string;
  private searchformateurRef: string;
  private searchformateur: string;
  private formationModule: any = null;
  private formationForm: any = null;
  private stagiaireForm: any = null;
  private clientList: any = null;
  private materielListSalle: any = null;
  private materielListOrdinateur: any = null;
  private materielListVideoprojecteur: any = null;
  private moduleList: any = null;
  private moduleForm: any = null;
  private searchMatiere: any = null;
  private stagiaires: any;
  private allStagiaires: any = null;
  private formationStagiaires: any = null;
  private idFormation: any = null;
  private searchgestionnaireRef: string;
  public afficherClientFormation: boolean = false;
  private formationView: any = null;


  constructor(private gestionnaireFormationHttpService: ClientFormationHttpService,
              private formationHttpService: FormationHttpService,
              private formateurHttpService: FormateurHttpService,
              private clientHttpService: ClientHttpService,
              private moduleHttpService: ModuleHttpService,
              private matiereHttpService: MatiereHttpService,
              private stagiaireFormationHttpService: StagiaireFormationHttpService,
              private authService: AuthService,
              private gestionnaireHttpService: GestionnaireHttpService) {
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

  ngOnInit() {
  }


  listClientFormation(): Array<ClientFormation> {
    return this.gestionnaireFormationHttpService.findAll();
  }
  list(): Array<Formation> {
    return this.formationHttpService.findAll();
  }

  listStagiaires(id: number){
    this.formationHttpService.findAllByFormation(id).subscribe(resp => {
        this.stagiaires = resp;
      }
      , err => console.log(err));
  }

  listClients(id: number) {
    this.formationForm = null;
    this.moduleList = null;
    this.moduleForm = null;
    this.materielListSalle = null;
    this.materielListVideoprojecteur = null;
    this.materielListOrdinateur = null;
    this.formationHttpService.findClients(id).subscribe(resp =>
      this.clientList = resp,
        err => console.log(err));
  }

  listVideoProjecteur(id: number) {
    this.materielListSalle = null;
    this.materielListOrdinateur = null;
    this.formationForm = null;
    this.clientList = null;
    this.moduleForm = null;
    this.moduleList = null;
    this.formationHttpService.findVideoprojecteur(id).subscribe(resp =>
        this.materielListVideoprojecteur = resp,
      err => console.log(err));
  }
  listSalle(id: number) {
    this.materielListVideoprojecteur = null;
    this.materielListOrdinateur = null;
    this.formationForm = null;
    this.clientList = null;
    this.moduleForm = null;
    this.moduleList = null;
    this.formationHttpService.findSalle(id).subscribe(resp =>
        this.materielListSalle = resp,
      err => console.log(err));
  }
  listOrdinateur(id: number) {
    this.materielListSalle = null;
    this.materielListVideoprojecteur = null;
    this.formationForm = null;
    this.clientList = null;
    this.moduleForm = null;
    this.moduleList = null;
    this.formationHttpService.findOrdinateur(id).subscribe(resp =>
        this.materielListOrdinateur = resp,
      err => console.log(err));
  }

  listModules(id: number) {
    this.formationForm = null;
    this.clientList = null;
    this.moduleForm = null;
    this.materielListSalle = null;
    this.materielListVideoprojecteur = null;
    this.materielListOrdinateur = null;
    this.formationHttpService.findModules(id).subscribe(resp => this.moduleList = resp, err => console.log(err));
  }

  view(id: number) {
    this.formationHttpService.findById(id).subscribe(resp => {this.formationView = resp;
      if (this.formationView.gestionnaire == null) {
        this.formationView.gestionnaire = new Gestionnaire();
      }
      if (this.formationView.formateur == null) {
        this.formationView.formateur = new Formateur();
      }}, err => console.log(err));
  }

  filtermatiere(){
    if (this.searchMatiere) {
      return this.matiereHttpService.findAll().filter(m => m.titre.toLowerCase().indexOf(this.search.toLowerCase()) !== -1);
    } else {
      return this.matiereHttpService.findAll();
    }
  }

  add() {
    this.stagiaireForm = null;
    this.clientList = null;
    this.moduleList = null;
    this.materielListSalle = null;
    this.materielListVideoprojecteur = null;
    this.materielListOrdinateur = null;
    this.formationForm = new Formation();
  }

  addModule() {
    this.formationForm = null;
    this.clientList = null;
    this.moduleList = null;
    this.materielListSalle = null;
    this.materielListVideoprojecteur = null;
    this.materielListOrdinateur = null;
    this.moduleForm = new Module();
    if(this.moduleForm.formation == null){
      this.moduleForm.formation = new Formation();
    }
    if(this.moduleForm.formateur == null){
      this.moduleForm.formateur = new Formateur();
    }
  }


  edit(id: number) {
    this.moduleList = null;
    this.clientList = null;
    this.materielListSalle = null;
    this.materielListVideoprojecteur = null;
    this.materielListOrdinateur = null;
    this.formationHttpService.findById(id).subscribe(resp => {
      this.formationForm = resp;
      this.stagiaireForm = resp;
      if (this.formationForm.formateurReferent == null) {
        this.formationForm.formateurReferent = new Formateur();
      }
      if (this.formationForm.gestionnaire == null) {
        this.formationForm.gestionnaire = new Gestionnaire();
      }
      console.log(this.formationForm);
      this.listStagiaires(this.formationForm.id);
    }, err => console.log(err));
    this.formationHttpService.findAllWithoutFormationForDragAndDrop(id).subscribe(resp => {
      this.allStagiaires = resp;
    }, err => console.log(err));
    this.stagiaireSelect(id);
  }

  stagiaireSelect(id:number) {
    this.formationStagiaires = new Array<string>();
    this.formationHttpService.findStagiaires(id).subscribe(resp => {
      this.formationStagiaires = resp;
    }, err => console.log(err));
  }
  editmodule(id: number) {
    this.moduleHttpService.findById(id).subscribe(resp => {
      this.moduleForm = resp;
      if (this.moduleForm.formateur == null) {
        this.moduleForm.formateur = new Formateur();
      }
    }, err => console.log(err));
  }


  delete(id: number) {
    this.formationHttpService.delete(id);
  }

  deletemodule(id: number) {
    this.moduleHttpService.delete(id);
  }

  save() {
    // if(this.formationForm.formateurReferent == {}){
    //
    //   this.formationForm.formateurReferent = null;
    // }

    // this.formationForm.formateurReferent = null;

    if(this.stagiaireForm){

    this.formationHttpService.deleteStagiaireFormation(this.stagiaireForm.id);

    for(let i = 0; i < this.formationStagiaires.length; i++) {


        let sf: StagiaireFormation = new StagiaireFormation();
        sf.stagiaire = new Stagiaire();
        sf.stagiaire = this.formationStagiaires[i];
        sf.formation = new Formation();
        sf.formation.id = this.formationForm.id;


        this.stagiaireFormationHttpService.save(sf);


    }



  }

    this.formationHttpService.save(this.formationForm);
    this.formationForm = null;
    this.formationStagiaires = null;
    this.stagiaireForm = null;
  }



  savemodule() {
    this.moduleHttpService.save(this.moduleForm);
    this.moduleForm = null;
  }

  cancel() {
    this.moduleList = null;
    this.formationForm = null;
    this.stagiaireForm = null;
  }

  cancelClient() {
    this.clientList = null;
  }

  cancelmodule() {
    this.moduleForm = null;
  }

  cancelMaterielListVideoprojecteur() {
    this.materielListVideoprojecteur = null;
  }

  cancelclientList() {
    this.clientList = null;
  }

  cancelmaterielListSalle() {
    this.materielListSalle = null;
  }

  cancelmaterielListOrdinateur() {
    this.materielListOrdinateur = null;
  }

  cancelView() {
    this.formationView = null;
  }

  filterformateurRef() {
    if (this.searchformateurRef) {
      return this.formateurHttpService.findAll().filter(f => f.nom.toLowerCase().indexOf(this.search.toLowerCase()) !== -1);
    } else {
      return this.formateurHttpService.findAll();
    }
  }

  filterGestionnaireRef() {
    if (this.searchgestionnaireRef) {
      return this.gestionnaireHttpService.findAll().filter(g => g.nom.toLowerCase().indexOf(this.search.toLowerCase()) !== -1);
    } else {
      return this.gestionnaireHttpService.findAll();
    }
  }

  filterformateur() {
    if (this.searchformateur) {
      return this.formateurHttpService.findAll().filter(f => f.nom.toLowerCase().indexOf(this.search.toLowerCase()) !== -1);
    } else {
      return this.formateurHttpService.findAll();
    }
  }

  hasAnyRole(roles: string[]) {
    return this.authService.hasAnyRole(roles);
  }


  // listmodule(id: number){
  //   this.moduleHttpService.findById(id).subscribe(resp => {this.formationModule = resp; err => console.log(err)});
  // }

}
