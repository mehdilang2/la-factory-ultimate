import {Formateur} from "./formateur";

export class Disponibilite {
  id: number;
  version: number;
  dtDispo: Date;
  formateur: Formateur;


  constructor(id?: number, version?: number, dtDispo?: Date, formateur?: Formateur) {
    this.id = id;
    this.version = version;
    this.dtDispo = dtDispo;
    this.formateur = formateur;
  }
}





