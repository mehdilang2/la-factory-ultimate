import {Matiere} from './matiere';
import {Module} from './module';
import {Utilisateur} from './utilisateur';
import {Contact} from "./contact";
import {Disponibilite} from "./disponibilite";

export class Formateur {
  id: number;
  version: number;
  civilite: string;
  nom: string;
  prenom: string;
  telephone: string;
  matieres: Array<Matiere>;
  modules: Array<Module>;
  utilisateur: Utilisateur = new Utilisateur("Formateur");
  disponibilites: Array<Disponibilite>;


  constructor(id?: number, version?: number, civilite?: string, nom?: string, prenom?: string, telephone?: string, matieres?: Array<Matiere>, modules?: Array<Module>, disponibilites?: Array<Disponibilite>) {
    this.id = id;
    this.version = version;
    this.civilite = civilite;
    this.nom = nom;
    this.prenom = prenom;
    this.telephone = telephone;
    this.matieres = matieres;
    this.modules = modules;
    this.disponibilites = disponibilites;
  }
}
