import {Matiere} from "./matiere";
import {Formateur} from './formateur';
import {Formation} from './formation';

export class Module {
  id: number;
  titre: string;
  dtDebut: Date;
  dtFin: Date;
  formation: Formation;
  matiere: Matiere = new Matiere();
  formateur: Formateur = new Formateur();


  constructor(id?: number, titre?: string,  dtDebut?: Date, dtFin?: Date) {
    this.id = id;
    this.titre = titre;
    this.dtDebut = dtDebut;
    this.dtFin = dtFin;
  }
}
