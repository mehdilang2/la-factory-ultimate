import {MaterielFormation} from './materielFormation';
import {Module} from './module';
import {Formateur} from './formateur';
import {Matiere} from './matiere';
import {StagiaireFormation} from "./stagiaire-formation";
import {Gestionnaire} from './gestionnaire';


export class Formation {
  id: number;
  version: number;
  dtDebut: Date;
  dtFin: Date;
  intitule: string;
  capacite: number;
  statut: string;
  materielFormation: MaterielFormation;
  ville: string;
  modules: Array<Module>;
  stagiairesFormations: Array<StagiaireFormation> = new Array<StagiaireFormation>();
  formateurReferent: Formateur = new Formateur();
  matiere: Matiere;
  module: Module = new Module();
  gestionnaire: Gestionnaire = new Gestionnaire();

  constructor(id?: number, version?: number, dtDebut?: Date, dtFin?: Date, intitule?: string, capacite?: number, statut?: string, ville?: string) {
    this.id = id;
    this.version = version;
    this.dtDebut = dtDebut;
    this.dtFin = dtFin;
    this.intitule = intitule;
    this.capacite = capacite;
    this.statut = statut;
    this.ville = ville;
  }
}
