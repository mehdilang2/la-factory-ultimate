
export class Utilisateur {
  id: number;
  version: number;
  typeUser: string;
  motDePasse: string;
  mail: string;

  constructor(typeUser?: string, id?: number, version?: number, motDePasse?: string, mail?: string) {
    this.id = id;
    this.version = version;
    this.typeUser = typeUser;
    this.motDePasse = motDePasse;
    this.mail = mail;
  }
}
