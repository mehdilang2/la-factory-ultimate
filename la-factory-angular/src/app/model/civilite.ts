export enum Civilite {
  MLLE = 'Mademoiselle',
  MME = 'Madame',
  M = 'Monsieur',
  NSP = 'Ne se prononce pas'
}
