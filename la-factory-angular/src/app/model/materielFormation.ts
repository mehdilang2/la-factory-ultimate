import {Formation} from './formation';
import {Salle} from './salle';

export class MaterielFormation {
  id: number;
 version: number;
 dtDebut: Date;
 dtFin: Date;
 formation: Formation;
 salle: Salle;

  constructor(id?: number, version?: number, dtDebut?: Date,  dtFin?: Date) {

    this.id = id;
    this.version = version;
    this.dtDebut = dtDebut;
    this.dtFin = dtFin;


  }
}
