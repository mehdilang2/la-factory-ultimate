import {Formation} from './formation';
import {MaterielFormation} from './materielFormation';

export class Ordinateur {
  id: number;
  type: string = "Ordinateur";
  code: number;
  version: number;
  coutUtilisation: number;
  processeur: string;
  ram: number;
  quantiteDD: number;
  dtAchat: Date;
  materielFormation: Array<MaterielFormation> = new Array<MaterielFormation>();
  formation: Formation;


  constructor(id?: number, code?: number, version?: number, coutUtilisation ?: number, processeur?: string, ram?: number, quantiteDD?: number, dtAchat?: Date){
    this.id = id;
    this.code = code;
    this.version = version;
    this.coutUtilisation = coutUtilisation;
    this.processeur = processeur;
    this.ram = ram;
    this.quantiteDD = quantiteDD;
    this.dtAchat = dtAchat;


  }
}
