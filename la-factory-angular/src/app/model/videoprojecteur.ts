import {Resolution} from './resolution';
import {Formation} from './formation';
import {MaterielFormation} from './materielFormation';

export class Videoprojecteur {
  id: number;
  type: string = "Videoprojecteur";
  code: number;
  version: number;
  coutUtilisation: number;
  hdmi: boolean;
  vga: boolean;
  resolution: string;
  materielFormation: Array<MaterielFormation> = new Array<MaterielFormation>();
  formation: Formation;

  constructor(id?: number, code?: number, version?: number, coutUtilisation ?: number, hdmi?: boolean, vga?: boolean) {
    this.id = id;
    this.code = code;
    this.version = version;
    this.coutUtilisation = coutUtilisation;
    this.hdmi = hdmi;
    this.vga = vga;

  }
}
