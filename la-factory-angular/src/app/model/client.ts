import {Adresse} from './adresse';
import {Contact} from './contact';
import {Utilisateur} from './utilisateur';

export class Client {
  id: number;
  version: number;
  nomEntreprise: string;
  nomBusinessUnit: string;
  numeroSiret: number;
  typeEntreprise: string;
  numTva: string;
  adresse: Adresse = new Adresse();
  contacts: Array<Contact>;


  constructor(id?: number, version?: number, nomEntreprise?: string, businessUnit?: string, numeroSiret?: number, typeEntreprise?: string, numTva?: string, contacts? : Array<Contact>) {
    this.id = id;
    this.version = version;
    this.nomEntreprise = nomEntreprise;
    this.nomBusinessUnit = businessUnit;
    this.numeroSiret = numeroSiret;
    this.typeEntreprise = typeEntreprise;
    this.numTva = numTva;
    this.contacts = contacts;

  }
}
