import {Adresse} from './adresse';
import {Utilisateur} from './utilisateur';

export class Administrateur {
  id: number;
  civilite: string;
  nom: string;
  prenom: string;
  telephone: string;
  adresse : Adresse= new Adresse();
  utilisateur : Utilisateur= new Utilisateur();

  constructor(id?: number, civilite?: string, nom?: string, prenom?: string, telephone?: string,  ) {
    this.id = id;
    this.civilite = civilite;
    this.nom = nom;
    this.prenom = prenom;
    this.telephone = telephone;

  }

}
