import {Client} from './client';
import {Utilisateur} from './utilisateur';

export class Contact {
  id: number;
  version: number;
  civilite: string;
  nom: string;
  prenom: string;
  telephone: string;
  mail: string;
  client: Client = new Client();
  utilisateur: Utilisateur = new Utilisateur();

  constructor(id?: number, version?: number, civilite?: string, nom?: string, prenom?: string, telephone?: string, mail?:string) {
    this.id = id;
    this.version = version;
    this.civilite = civilite;
    this.nom = nom;
    this.prenom = prenom;
    this.telephone = telephone;
    this.mail = mail;
  }

}
