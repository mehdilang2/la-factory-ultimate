import {Adresse} from './adresse';
import {Formation} from './formation';
import { Client } from './client';
import {Presence} from "./presence";
import {Utilisateur} from './utilisateur';
import {Ordinateur} from './ordinateur';
import {nullSafeIsEquivalent} from '@angular/compiler/src/output/output_ast';
import {Civilite} from './civilite';
import {StagiaireFormation} from './stagiaireFormation';

export class Stagiaire {
  id: number;
  version: number;
  civilite: Civilite;
  nom: string;
  prenom: string;
  telephone: string;
  commentaires: string;
  adresse: Adresse = new Adresse();
  presences: Array<Presence> = null;
  presence: Presence = new Presence();

  ordinateur: Ordinateur = new Ordinateur();
  client: Client = new Client();
 stagiaireFormations: Array<StagiaireFormation>;
  utilisateur: Utilisateur = new Utilisateur();

  constructor(id?: number, version?: number, civilite?: Civilite, nom?: string, prenom?: string, telephone?: string, commentaires?: string, presences?: Array<Presence>) {
    this.id = id;
    this.version = version;
    this.civilite = civilite;
    this.nom = nom;
    this.prenom = prenom;
    this.telephone = telephone;
    this.commentaires = commentaires;
    this.presences = presences;
  }
}
