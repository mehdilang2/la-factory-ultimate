export class Adresse {
  voie: string;
  complement: string;
  codePostal: string;
  ville: string;
  pays: string;

  constructor(voie?: string, complement?: string, codePostal?: string, ville?: string, pays?: string) {
    this.voie = voie;
    this.complement = complement;
    this.codePostal = codePostal;
    this.ville = ville;
    this.pays = pays;
  }

}
