import {MaterielFormation} from './materielFormation';
import {Formation} from './formation';
import {Adresse} from './adresse';

export class Salle {
  id: number;
  type: string = 'Salle';
  code: number;
  coutUtilisation: number;
  placeMax: number;
  materielFormation: Array<MaterielFormation> = new Array<MaterielFormation>();
  adresse: Adresse = new Adresse();
  formation: Formation;

  constructor(id?: number, code?: number, coutUtilisation ?: number, placeMax?: number, materielFormation?: Array<MaterielFormation>) {
    this.id = id;
    this.code = code;
    this.coutUtilisation = coutUtilisation;
    this.placeMax = placeMax;
    this.materielFormation = materielFormation;
}
}
