import {Formateur} from './formateur';

export class Matiere {
  id: number;
  version: number;
  titre: string;
  duree: number;
  objectifs: string;
  prerequis: string;
  contenu: string;
  formateurs: Array<Formateur>;

  // tslint:disable-next-line:max-line-length
  constructor(id?: number, version?: number, titre?: string, duree?: number, objectifs?: string, prerequis?: string, contenu?: string) {
    this.id = id;
    this.version = version;
    this.titre = titre;
    this.duree = duree;
    this.objectifs = objectifs;
    this.prerequis = prerequis;
    this.contenu = contenu;
  }
}
