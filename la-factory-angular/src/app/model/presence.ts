import {Stagiaire} from './stagiaire';

export class Presence {
  id: number;
  version: number;
  date: Date;
  presenceMatin: boolean;
  presenceApresMidi: boolean;
  stagiaire: Stagiaire;
  ouvertureSignature: boolean = false;

  constructor(id?: number, version?: number, date?: Date, presenceMatin?: boolean, presenceApresMidi?: boolean){
    this.id= id;
    this.version = version;
    this.date = date;
    this.presenceMatin = presenceMatin;
    this.presenceApresMidi = presenceApresMidi;

  }
}
