import {Formation} from './formation';
import {MaterielFormation} from './materielFormation';

export class StagiaireFormation{
  id : number;
  formation : Formation;
  constructor(id?: number) {
    this.id = id;
  }
}
