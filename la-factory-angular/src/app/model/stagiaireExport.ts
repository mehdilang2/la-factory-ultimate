import {Adresse} from './adresse';
import {Formation} from './formation';
import { Client } from './client';
import {Presence} from "./presence";
import {Utilisateur} from './utilisateur';
import {Ordinateur} from './ordinateur';
import {nullSafeIsEquivalent} from '@angular/compiler/src/output/output_ast';
import {Civilite} from './civilite';
import {StagiaireFormation} from './stagiaireFormation';

export class StagiaireExport {

  civilite: Civilite;
  nom: string;
  prenom: string;
  telephone: string;
  mail: string;
  client: string;
  voie : string;
  complement: string;
  codePostal : string;
  ville : string;
  pays : string;
  commentaire: string;


  constructor( ) {

  }
}
