import {Utilisateur} from './utilisateur';

import {Adresse} from './adresse';

export class Gestionnaire {
  id: number;
  version: number;
  civilite: string;
  nom: string;
  prenom: string;
  telephone: string;
  adresse: Adresse = new Adresse();
  utilisateur: Utilisateur= new Utilisateur();


  constructor(id?: number, version?: number, civilite?: string, nom?: string, prenom?: string, telephone?: string) {
    this.id = id;
    this.version = version;
    this.civilite = civilite;
    this.nom = nom;
    this.prenom = prenom;
    this.telephone = telephone;

  }

}
