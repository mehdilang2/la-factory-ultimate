import {Stagiaire} from "./stagiaire";
import {Formation} from "./formation";


export class StagiaireFormation {
  id: number;
  version: number;
  stagiaire: Stagiaire = new Stagiaire();
  formation: Formation = new Formation();


  constructor(id?: number, version?: number, stagiaire?:Stagiaire) {
    this.id = id;
    this.version = version;
    this.stagiaire = stagiaire;  }
}
