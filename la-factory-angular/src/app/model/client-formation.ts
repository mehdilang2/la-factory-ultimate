import {Client} from './client';
import {Formation} from './formation';

export class ClientFormation {
  id: number;
  version: number;
  demandes: number;
  commentaires: string;
  statut: boolean;
  accordes: number;
  client: Client = new Client();
  formation: Formation = new Formation();


  constructor(id?: number, version?: number, demandes?: number, commentaires?: string, statut?: boolean, accordes?: number) {
    this.id = id;
    this.version = version;
    this.demandes = demandes;
    this.commentaires = commentaires;
    this.statut = statut;
    this.accordes = accordes;
  }
}
