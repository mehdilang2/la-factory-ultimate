import {Injectable} from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {Contact} from '../model/contact';
import {Observable} from 'rxjs';

@Injectable()
export class ContactHttpService {

  private contacts: any;

  constructor(private http: HttpClient) {
    this.load();
  }

  load() {
    this.http.get('http://localhost:8080/api/contact').subscribe(resp => {
      this.contacts = resp;
    }, err => console.log(err));
  }

  findAll(): Array<Contact> {
    return this.contacts;
  }

  findById(id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/contact/' + id);
  }

  findCivilite(): Observable<Object> {
    return this.http.get('http://localhost:8080/api/contact/civilites');
  }

  save(contact:Contact) {
    if (contact) {
      if (contact.id) {
        this.http.put('http://localhost:8080/api/contact/' + contact.id, contact).subscribe(resp => this.load(),
          err => console.log(err)
        );
      } else {
        this.http.post('http://localhost:8080/api/contact', contact).subscribe(resp => this.load(),
          err => console.log(err));
      }
    }
  }

  delete(id:number) {
    this.http.delete('http://localhost:8080/api/contact/' + id).subscribe(resp => this.load(),
      err => console.log(err));

  }

}
