import { Component, OnInit } from '@angular/core';
import {Contact} from '../model/contact';
import {ContactHttpService} from './contact-http.service';
import {Client} from '../model/client';
import {ClientHttpService} from '../client/client-http.service';
import {Utilisateur} from '../model/utilisateur';
import {NgbCalendar} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  private utilisateur: any = null;
  private contactForm: any = null;
  private civilites: any = null;
  private Client: any = null;


  constructor(private contactHttpService: ContactHttpService, private clientHttpService: ClientHttpService) {
    this.civilite();
  }

  ngOnInit() {
  }

  list(): Array<Contact> {
    return this.contactHttpService.findAll();
  }


  add() {
    this.contactForm = new Contact();
  }

  edit(id: number) {
    this.contactHttpService.findById(id).subscribe(resp => {
      this.contactForm = resp;
      if (this.contactForm.nomEntreprise == null) {
        this.contactForm.nomEntreprise = new Client();
      }
      if (this.contactForm.mail == null) {
        this.contactForm.mail = new Utilisateur();
      }
    }, err => console.log(err));
  }

  delete(id: number) {
    this.contactHttpService.delete(id);
  }

  save() {
    if(!this.contactForm.utilisateur){
      this.utilisateur = new Utilisateur('Contact');
      this.contactForm.utilisateur = this.utilisateur;
      this.utilisateur = null;
    }
    this.contactHttpService.save(this.contactForm);
    this.contactForm = null;
  }

  cancel() {
    this.contactForm = null;
  }

  civilite(){
    this.contactHttpService.findCivilite().subscribe(resp => this.civilites = resp, err => console.log(err));
  }

  filterclient() {
    return this.clientHttpService.findAll();
  }

}
