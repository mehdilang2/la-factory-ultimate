import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PresenceStagiaireComponent } from './presence-stagiaire.component';

describe('PresenceStagiaireComponent', () => {
  let component: PresenceStagiaireComponent;
  let fixture: ComponentFixture<PresenceStagiaireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PresenceStagiaireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PresenceStagiaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
