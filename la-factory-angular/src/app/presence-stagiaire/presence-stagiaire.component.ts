import { Component, OnInit } from '@angular/core';
import {DatePipe} from '@angular/common';
import {Presence} from '../model/presence';

@Component({
  selector: 'app-presence-stagiaire',
  templateUrl: './presence-stagiaire.component.html',
  styleUrls: ['./presence-stagiaire.component.css']
})
export class PresenceStagiaireComponent implements OnInit {
private date = new Date();
private presence = new Presence();
  constructor() {}

  ngOnInit() {
  }
  validerSignature(){
 this.presence.date = this.date;
  }
}
