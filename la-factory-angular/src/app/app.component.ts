import { Component } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {AuthService} from './service/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [AuthService]
})
export class AppComponent {
  title = 'la-factory-angular';

  constructor(public translate: TranslateService, private authService: AuthService) {
    translate.addLangs(['en', 'fr']);
    translate.setDefaultLang('fr');

    const browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/en|fr/) ? browserLang : 'fr');
  }

  useLanguage(language: string) {
    this.translate.use(language);
  }

  hasAnyRole(roles: string[]) {
    return this.authService.hasAnyRole(roles);
  }


  getLogin() {
    return this.authService.getUser().login;
  }

  logout() {
    return this.authService.logout();
  }
}


