import { Component, OnInit } from '@angular/core';
import {PresenceHttpService} from './presence-http.service';
import {Gestionnaire} from '../model/gestionnaire';
import {Presence} from '../model/presence';
import {Stagiaire} from '../model/stagiaire';

@Component({
  selector: 'app-presence',
  templateUrl: './presence.component.html',
  styleUrls: ['./presence.component.css']
})
export class PresenceComponent implements OnInit {
private stagiaire : any = null;

  constructor( private presenceHttpService : PresenceHttpService) { }

  ngOnInit() {
  }
  list(): Array<Stagiaire> {
    return this.presenceHttpService.findAll();
  }

  activerSignature(){

  }
  validerSignature(){

  }

}
