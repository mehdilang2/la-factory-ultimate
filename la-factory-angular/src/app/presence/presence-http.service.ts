import {Injectable} from '@angular/core';

import {Observable} from 'rxjs';
import {Formateur} from '../model/formateur';
import {HttpClient} from '@angular/common/http';
import {Gestionnaire} from '../model/gestionnaire';
import {Presence} from '../model/presence';
import {Stagiaire} from '../model/stagiaire';

@Injectable()
export class PresenceHttpService {

 private stagiaire: any;
 private stagiairebis :any ;
  private presence: Presence = new Presence();
  private  dateJour: string = '2019-09-20'; // TODO changer par date du jour

  constructor(private http: HttpClient) {
   this.load();
  }


  load() {
    this.http.get('http://localhost:8080/api/formateur/7/presence').subscribe(resp => {
        this.stagiaire = resp;
        console.log(resp)
        for (let j in this.stagiaire){
            for (let i in this.stagiaire[j].presences) {
              if (this.stagiaire[j].presences[i].date == this.dateJour){
                this.stagiaire[j].presence = this.stagiaire[j].presences[i];
                break ;
              }
          }
        }

        }
        , err => console.log(err));
  }


  findAll(): Array<Stagiaire> {
    return this.stagiaire;
  }


  }


