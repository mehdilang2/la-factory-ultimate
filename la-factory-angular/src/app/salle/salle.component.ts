import {Component, OnInit} from '@angular/core';
import {Salle} from '../model/salle';
import {SalleHttpService} from './salle-http.service';
import {FormationHttpService} from "../formation/formation-http.service";
import {MaterielFormationHttpService} from "../materiel-formation/materielFormation-http.service";
import {Formation} from "../model/formation";
import {MaterielFormation} from "../model/materielFormation";
import {Ordinateur} from "../model/ordinateur";

@Component({
  selector: 'salle',
  templateUrl: './salle.component.html',
  styleUrls: ['./salle.component.css'],
  providers: [FormationHttpService]
})
export class SalleComponent implements OnInit {

  private salleForm: any = null;
  private salleView: any = null;
  private salleResa: any = null;
  private materielFormationCalendar: any = null;
  private formations: any = null;
  private salleId: number;

  constructor(private salleHttpService: SalleHttpService, private formationHttpService: FormationHttpService, private materielFormationHttpService: MaterielFormationHttpService) {
  this.listFormation();
  }

  ngOnInit() {
  }

  list(): Array<Salle> {
    return this.salleHttpService.findAll();
  }

  listFormation() {
    this.formationHttpService.findAllFormation().subscribe(resp => this.formations = resp, err => console.log(err));
    console.log(this.formations);
  }

  listReservationById(id: number) {
    this.salleId = id;

    return this.salleHttpService.findMaterielFormationById(id).subscribe(resp => {
      this.salleResa = resp;
    }, err => console.log(err));

  }


  add() {
    this.salleForm = new Salle();
  }

  addResa() {
    this.materielFormationCalendar = new MaterielFormation();
    if (this.materielFormationCalendar.formation == null) {
      this.materielFormationCalendar.formation = new Formation();
    }
    console.log(this.salleId);
    if (this.materielFormationCalendar.materiel == null) {
      this.materielFormationCalendar.materiel = new Ordinateur();
      this.materielFormationCalendar.materiel.id = this.salleId;
    }
  }

  view(id: number) {
    this.salleHttpService.findById(id).subscribe(resp => this.salleView = resp, err => console.log(err));
  }

  edit(id: number) {
    this.salleHttpService.findById(id).subscribe(resp => this.salleForm = resp, err => console.log(err));
  }

  editResa(id: number) {
    this.materielFormationHttpService.findById(id).subscribe(resp => {
      this.materielFormationCalendar = resp;

    }, err => console.log(err));
  }

  delete(id: number) {
    this.salleHttpService.delete(id);
  }

  deleteResa(id: number) {
    this.materielFormationHttpService.delete(id);
  }

  save() {
    this.salleHttpService.save(this.salleForm);
    this.salleForm = null;
  }

  reserv() {

    this.materielFormationHttpService.save(this.materielFormationCalendar);
    this.materielFormationCalendar = null;

  }

  cancel() {
    this.salleForm = null;
  }


  // public filterDispo() {
  //   if (this.searchMaterielFormation) {
  //     return this.salleHttpService.findAll().filter(s => s.date.toLowerCase().indexOf(this.search.toLowerCase()) !== -1);
  //   } else {
  //     return this.salleHttpService.findAll();
  //   }
  // }
}
