import {Ordinateur} from '../model/ordinateur';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Salle} from '../model/salle';
import {MaterielFormationHttpService} from "../materiel-formation/materielFormation-http.service";

@Injectable()
export class SalleHttpService {

  private salles: any;

  constructor(private http: HttpClient) {
    this.load();
  }

  load() {
    this.http.get('http://localhost:8080/api/salle').subscribe(resp => {
      this.salles = resp;
    }, err => console.log(err));
  }

  findAll(): Array<Salle> {
    return this.salles;
  }

  findById(id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/salle/' + id);
  }

  findMaterielFormationById(id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/salle/' + id + '/materielFormation');
  }


  findByIdWithDates(id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/salle/' + id + '/dates');
  }

  save(salle: Salle) {
    if (salle) {
      if (salle.id) {
        this.http.put('http://localhost:8080/api/salle/' + salle.id, salle).subscribe(resp =>{

          this.load();
          },
          err => console.log(err)
        );
      } else {
        this.http.post('http://localhost:8080/api/salle', salle).subscribe(resp => this.load(),
          err => console.log(err));
      }
    }
  }

  delete(id: number) {
    this.http.delete('http://localhost:8080/api/salle/' + id).subscribe(resp => this.load(),
      err => console.log(err));

  }

}
