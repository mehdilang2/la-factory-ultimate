import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../service/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [AuthService]
})

export class LoginComponent implements OnInit {

  model: any = {};

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit() { }

  login() {
    console.log(this.model);
    this.authService.login(this.model);
  }
}
