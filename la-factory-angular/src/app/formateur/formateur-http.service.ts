import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Formateur} from '../model/formateur';

@Injectable()
export class FormateurHttpService {

  private formateurs: any;

  constructor(private http: HttpClient) {
    this.load();
  }

  load() {
    this.http.get('http://localhost:8080/api/formateur').subscribe(resp => {
      this.formateurs = resp;
    }, err => console.log(err));
  }

  findAll(): Array<Formateur> {
    return this.formateurs;
  }

  findById(id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/formateur/' + id);
  }

  findCivilite(): Observable<Object> {
    return this.http.get('http://localhost:8080/api/formateur/civilites');
  }

  findMatieres(id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/formateur/' + id + '/matieres');
  }

  findAllForDragAndDrop(id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/formateur/' + id + '/matieresAll');
  }

  save(formateur: Formateur) {
    if (formateur) {
      if (formateur.id) {
        this.http.put('http://localhost:8080/api/formateur/' + formateur.id, formateur).subscribe(resp => this.load(),
          err => console.log(err)
        );
      } else {
        this.http.post('http://localhost:8080/api/formateur/', formateur).subscribe(resp => this.load(),
          err => console.log(err));
      }
    }
  }

  updateFormateur(formateur: Formateur) {
    this.http.put('http://localhost:8080/api/formateur/' + formateur.id + '/matieres', formateur).subscribe(resp => this.load(),
      err => console.log(err)
    );
  }

  delete(id: number) {
    this.http.delete('http://localhost:8080/api/formateur/' + id).subscribe(resp => this.load(),
      err => console.log(err));

  }


}
