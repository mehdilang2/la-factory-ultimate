import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormateurHttpService} from './formateur-http.service';
import {Formateur} from '../model/formateur';
import {ActivatedRoute} from '@angular/router';
import {Utilisateur} from '../model/utilisateur';
import {UtilisateurHttpService} from '../utilisateur/utilisateur-http.service';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {Matiere} from '../model/matiere';
import {MatiereHttpService} from '../matiere/matiere-http.service';
import {MatiereComponent} from '../matiere/matiere.component';
import {AuthService} from '../service/auth/auth.service';
import {FullCalendarComponent} from "@fullcalendar/angular";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGrigPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";
import {EventInput} from "@fullcalendar/core/structs/event";
import timeGridPlugin from "@fullcalendar/timegrid";
import {Disponibilite} from "../model/disponibilite";
import {DisponibiliteHttpService} from "../disponibilite/disponibilite-http.service";
import {Client} from "../model/client";

@Component({
  selector: 'formateur',
  templateUrl: './formateur.component.html',
  styleUrls: ['./formateur.component.css'],
  providers: [AuthService]
})
export class FormateurComponent implements OnInit {
  private utilisateur: any = null;
  private formateurForm: any = null;
  private civilites: any = null;
  private search: string;
  private searchFormateur: string;
  public formateurDetail: any = null;
  public formateurCalendar: any = null;
  private matieres: Array<Matiere> = new Array<Matiere>();
  private matieresIntituleSelect: any = new Array<string>();
  private formateurCompetences: any;
  public allCompetences: any;
  private idUtilisateur: number;
  private dispoFormateur: any;
  events: any = new Array;
  options: any;
  eventsModel: any = null;
  el: any;
  dayDispo: Array<Date> = [];
  @ViewChild('fullcalendar', {static: false}) fullcalendar: FullCalendarComponent;
  @ViewChild('external', {static: false}) external: ElementRef;

  calendarVisible = true;
  calendarPlugins = [dayGridPlugin, timeGrigPlugin, interactionPlugin];
  calendarWeekends = true;
  calendarEvents: EventInput[] = [
    {'start': new Date()}
  ];


  constructor(private route: ActivatedRoute,
              private formateurHttpService: FormateurHttpService,
              private utilisateurHttpService: UtilisateurHttpService,
              private matiereHttpService: MatiereHttpService,
              private disponibiliteHttpService: DisponibiliteHttpService,
              private authService: AuthService) {
    this.civilite();
  }


  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

  ngOnInit() {
    this.options = {
      editable: true,
      selectable: true,
      customButtons: {
        myCustomButton: {
          text: 'custom!',
          click: function () {
            alert('clicked the custom button!');
          }
        }
      },
      theme: 'standart',
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay'
      },




      // add other plugins
      plugins: [dayGridPlugin, interactionPlugin, timeGridPlugin]
    };

  }

  list(): Array<Formateur> {
    return this.formateurHttpService.findAll();
  }

  add() {
    this.formateurForm = new Formateur();
  }

  matieresSelect(id: number) {
    this.formateurCompetences = new Array<string>();
    this.matieresIntituleSelect = new Array<string>();
    this.formateurHttpService.findMatieres(id).subscribe(resp => {
      this.formateurCompetences = resp;
    }, err => console.log(err));
  }

  detail(id: number) {
    console.log(id);
    this.formateurHttpService.findById(id).subscribe(resp => this.formateurDetail = resp, err => console.log(err));
    this.matieresSelect(id);
    this.formateurHttpService.findAllForDragAndDrop(id).subscribe(resp => {
      this.allCompetences = resp;
    }, err => console.log(err));
    this.formateurCalendar = null;
    this.formateurForm = null;
  }


  edit(id: number) {
    this.formateurHttpService.findById(id).subscribe(resp => {this.formateurForm = resp;
    if (this.formateurForm.utilisateur == null) {
      this.formateurForm.utilisateur = new Utilisateur('Formateur');
      console.log(this.formateurForm.utilisateur.typeUser);
    }},err => console.log(err));
    this.formateurDetail = null;
    this.formateurCalendar = null;
  }

  delete(id: number) {
    this.formateurHttpService.delete(id);
  }

  save() {
    if (this.formateurForm.utilisateur == null) {
      this.utilisateur = new Utilisateur('Formateur');
      this.formateurForm.utilisateur = this.utilisateur;
      this.utilisateur = null;
    }
    this.formateurHttpService.save(this.formateurForm);
    this.formateurForm = null;
  }

  updateFormateur() {
    this.formateurDetail.matieres = this.formateurCompetences;
    this.formateurHttpService.updateFormateur(this.formateurDetail);
  }

  cancel() {
    this.formateurForm = null;
  }

  cancelformateurDetail() {
    this.formateurDetail = null;
  }

  cancelformateurCalendar() {
    this.formateurCalendar = null;
  }

  civilite() {
    this.formateurHttpService.findCivilite().subscribe(resp => this.civilites = resp, err => console.log(err));
  }

  filter() {
    if (this.search) {

      let formateurs: any = new Array();

      for (let f of this.formateurHttpService.findAll()) {
        for (let mat of f.matieres) {
          if (mat.titre.toLowerCase().indexOf(this.search.toLowerCase()) !== -1) {
            formateurs.push(f);
          }
        }
      }



      return formateurs;
    }if(this.searchFormateur){

      return this.formateurHttpService.findAll().filter(s => s.nom.toLowerCase().indexOf(this.search.toLowerCase()) !== -1 || s.prenom.toLowerCase().indexOf(this.search.toLowerCase()) !== -1);

    }  else{
      return this.formateurHttpService.findAll();
    }
  }

  hasAnyRole(roles: string[]) {
    return this.authService.hasAnyRole(roles);
  }

  //FONCTIONS DU CALENDRIER

  cancelCalendar() {
    this.formateurCalendar = null;
  }

  calendar(id: number) {
    console.log(id);

    this.disponibiliteHttpService.findByFormateur(id).subscribe(resp => {
      this.dispoFormateur = resp;
      for (let i of this.dispoFormateur) {
        this.events.push({ start : i.dtDispo, title : 'Disponible'});
      }

    }, err => console.log(err))


    this.formateurHttpService.findById(id).subscribe(resp => {
      this.formateurCalendar = resp;
      console.log(this.formateurCalendar);
    }, err => console.log(err));
    this.formateurDetail = null;
    this.formateurForm = null;
    this.formateurCalendar = null;
  }

  saveCalendar(idFormateur: number) {
    for (let i = 0; i < this.dayDispo.length; i++) {
      let dispo: Disponibilite = new Disponibilite(null, null, this.dayDispo[i], this.formateurCalendar);
      this.disponibiliteHttpService.save(dispo);
      console.log(dispo);
    }
    this.dayDispo = null;
    this.formateurCalendar = null;
  }

  selectCalendar(obj) {
    console.log(obj);

    let trouducStart1 = obj.start.getFullYear();
    let trouducStart2 = obj.start.getMonth();
    let trouducStart3 = obj.start.getDate() + 1;
    let dateStart: Date = new Date(trouducStart1, trouducStart2, trouducStart3);

    let trouducEnd1 = obj.end.getFullYear();
    let trouducEnd2 = obj.end.getMonth();
    let trouducEnd3 = obj.end.getDate() + 1;
    let dateEnd: Date = new Date(trouducEnd1, trouducEnd2, trouducEnd3);

    console.log(dateStart);
    console.log(dateEnd);
    console.log('###################');


    let d: any = null;
    for (d = dateStart; d < dateEnd; d.setDate(d.getDate() + 1)) {
      this.dayDispo.push(new Date(d));
    }
    console.log(this.dayDispo);


  }

  eventDragStop(model) {
  }


  dateClick(model) {
    console.log(model);
  }

  updateHeader() {
    this.options.header = {
      left: 'prev,next myCustomButton',
      center: 'title',
      right: ''
    };
  }

  updateEvents() {
    this.eventsModel = [{
      title: 'Updaten Event',
      start: this.yearMonth + '-08',
      end: this.yearMonth + '-10'
    }];
  }

  get yearMonth(): string {
    const dateObj = new Date();
    return dateObj.getUTCFullYear() + '-' + (dateObj.getUTCMonth() + 1);
  }

  dayRender(ev) {
    ev.el.addEventListener('dblclick', () => {
      alert('double click!');
    });
  }

  eventClick(eventsModel) {
    alert(eventsModel);
  }



}
