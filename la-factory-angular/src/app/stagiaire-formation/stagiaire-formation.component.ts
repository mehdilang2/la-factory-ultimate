import {Component, OnInit} from '@angular/core';
import {FormationHttpService} from "../formation/formation-http.service";
import {StagiaireFormation} from "../model/stagiaire-formation";
import {StagiaireFormationHttpService} from "./stagiaire-formation-http.service";
import {FormateurHttpService} from "../formateur/formateur-http.service";
import {AuthService} from "../service/auth/auth.service";
import {Formation} from "../model/formation";
import {Observable} from "rxjs";

@Component({
  selector: 'app-stagiaire-formation',
  templateUrl: './stagiaire-formation.component.html',
  styleUrls: ['./stagiaire-formation.component.css'],
  providers: [FormationHttpService, FormationHttpService, StagiaireFormationHttpService, AuthService]
})
export class StagiaireFormationComponent implements OnInit {

  private moduleList: any = null;
  private formateurList: any = null;
  private formationList: any = null;

  constructor(private stagiaireFormationHttpService: StagiaireFormationHttpService, private formationHttpService: FormationHttpService, private authService: AuthService) {

    this.listFormations(this.getIdUser());

  }

  ngOnInit() {

  }

  listFormations(id: number) {
    this.stagiaireFormationHttpService.findFormations(id).subscribe(resp => this.formationList = resp, err => console.log(err));
  }

  listModules(id: number) {
    this.formateurList = null;
    this.formationHttpService.findModules(id).subscribe(resp => this.moduleList = resp, err => console.log(err));
  }

  listFormateurs(id: number) {
    this.moduleList = null;
    this.formationHttpService.findFormateur(id).subscribe(resp => this.formateurList = resp, err => console.log(err));
  }

  getIdUser() {
   return this.authService.getUser().id;
  }
}

