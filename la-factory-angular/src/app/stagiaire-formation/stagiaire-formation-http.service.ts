import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {StagiaireFormation} from "../model/stagiaire-formation";

@Injectable()
export class StagiaireFormationHttpService {

  private stagiaireFormation: any;

  constructor(private http: HttpClient) {
    this.load();
  }

  load() {
    this.http.get('http://localhost:8080/api/stagiaireFormation').subscribe(resp => {
      this.stagiaireFormation = resp;
    }, err => console.log(err));
  }

  findById(id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/stagiaireFormation/' + id);
  }

  findFormations(id:number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/stagiaire/' + id + '/formations');
  }

  save(stagiaireFormation: StagiaireFormation) {
    if (stagiaireFormation) {
      if (stagiaireFormation.id) {
        this.http.put('http://localhost:8080/api/stagiaireFormation/' + stagiaireFormation.id, stagiaireFormation).subscribe(resp => this.load(),
          err => console.log(err)
        );
      } else {
        this.http.post('http://localhost:8080/api/stagiaireFormation/', stagiaireFormation).subscribe(resp => this.load(),
          err => console.log(err));
      }
    }
  }

  delete(id:number) {
    this.http.delete('http://localhost:8080/api/stagiaireFormation/' + id).subscribe(resp => this.load(),
      err => console.log(err));
  }
}
