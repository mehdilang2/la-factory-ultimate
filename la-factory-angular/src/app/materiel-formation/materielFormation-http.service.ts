import {Ordinateur} from '../model/ordinateur';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {MaterielFormation} from '../model/materielFormation';
import {VideoprojecteurHttpService} from "../videoprojecteur/videoprojecteur-http.service";

@Injectable()
export class MaterielFormationHttpService {

  private materielFormations: any;

  constructor(private http: HttpClient, private videoprojecteurHttpService: VideoprojecteurHttpService) {
    this.load();
  }

  load() {
    this.http.get('http://localhost:8080/api/materielFormation').subscribe(resp => {
      this.materielFormations = resp;
    }, err => console.log(err));
  }



  findAll(): Array<MaterielFormation> {
    return this.materielFormations;
  }

  findById(id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/materielFormation/' + id);
  }

  save(materielFormation: MaterielFormation) {
    if (materielFormation) {
      if (materielFormation.id) {
        this.http.put('http://localhost:8080/api/materielFormation/' + materielFormation.id, materielFormation).subscribe(resp => {
          this.videoprojecteurHttpService.load();
          this.load();
        }, err => console.log(err));
      } else {
        this.http.post('http://localhost:8080/api/materielFormation', materielFormation).subscribe(resp => this.load(),
          err => console.log(err));
      }
    }
  }

  delete(id: number) {
    this.http.delete('http://localhost:8080/api/materielFormation/' + id).subscribe(resp => this.load(),
      err => console.log(err));

  }

}
