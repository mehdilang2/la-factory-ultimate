import { Component, OnInit } from '@angular/core';
import {MaterielFormation} from '../model/materielFormation';
import {MaterielFormationHttpService} from './materielFormation-http.service';

@Component({
  selector: 'app-materiel-formation',
  templateUrl: './materiel-formation.component.html',
  styleUrls: ['./materiel-formation.component.css']
})
export class MaterielFormationComponent implements OnInit {

  private materielFormationForm : any = null;

  constructor(private materielFormationHttpService: MaterielFormationHttpService) {
  }

  ngOnInit() {
  }

  list(): Array<MaterielFormation> {
    return this.materielFormationHttpService.findAll();
  }

  add() {
    this.materielFormationForm = new MaterielFormation();
  }

  edit(id: number) {
    this.materielFormationHttpService.findById(id).subscribe(resp => this.materielFormationForm = resp, err => console.log(err));
  }

  delete(id: number) {
    this.materielFormationHttpService.delete(id);
  }

  save() {
    this.materielFormationHttpService.save(this.materielFormationForm);
    this.materielFormationForm = null;
  }

  cancel() {
    this.materielFormationForm = null;
  }

}
