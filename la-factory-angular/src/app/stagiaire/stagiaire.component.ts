import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {ModuleHttpService} from '../module/module-http.service';
import {StagiaireHttpService} from './stagiaire-http.service';
import {Stagiaire} from '../model/stagiaire';
import {Adresse} from '../model/adresse';
import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import { FullCalendarComponent } from '@fullcalendar/angular';
import { EventInput } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGrigPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import {MatTableDataSource} from '@angular/material';
import {Angular5Csv} from 'angular5-csv/dist/Angular5-csv';
import {Ordinateur} from '../model/ordinateur';
import {ClientHttpService} from '../client/client-http.service';
import {OrdinateurHttpService} from '../ordinateur/ordinateur-http.service';
import {Client} from '../model/client';
import {Utilisateur} from '../model/utilisateur';
import {FormationHttpService} from '../formation/formation-http.service';
import {Formation} from '../model/formation';
import {AuthService} from '../service/auth/auth.service';

import { ExportToCsv } from 'export-to-csv';
import {StagiaireExport} from '../model/stagiaireExport';

@Component({
  selector: 'app-stagiaire',
  templateUrl: './stagiaire.component.html',
  styleUrls: ['./stagiaire.component.css'],
  providers: [FormationHttpService, AuthService]
})
export class StagiaireComponent implements OnInit {
  private utilisateur: Utilisateur;
  private search: string;
  private search2: string;
  private searchclient: string;
  private searchordi: string;
  private searchformation: string;
  private civilites: any = null;
  private stagiaireForm: any = null;
  private stagiaireView: any = null;
  private stagiairePresence: any = null;
 // modelCal: NgbDateStruct;
  //date: {year: number, month: number};
  private selectedStagiaire: any = null;
  private data: any =null;
  private stagiaireexport= new StagiaireExport();
  private stagiaireExports : any =null;
  private stagiaireList : any = null;
  private clientContact : any = null;


  @ViewChild('calendar', {static: true} ) calendarComponent: FullCalendarComponent; // the #calendar in the template

  calendarVisible = true;
  calendarPlugins = [dayGridPlugin, timeGrigPlugin, interactionPlugin];
  calendarWeekends = true;
  calendarEvents: EventInput[] =  [
    {"start":new Date()}
  ];

  constructor(private stagiaireHttpService: StagiaireHttpService,
              private calendar: NgbCalendar,
              private clientHttpService: ClientHttpService,
              private ordinateurHttpService: OrdinateurHttpService,
              private formationHttpService: FormationHttpService,
              private authService: AuthService) {
    this.civilite();
    this.findIdClientByContact(this.getIdUser());

  }

  ngOnInit() {

    this.calendarEvents.push()

    }

  public filter() {
    if (this.search) {
      return this.stagiaireHttpService.findAll().filter(s => s.nom.toLowerCase().indexOf(this.search.toLowerCase()) !== -1 || s.prenom.toLowerCase().indexOf(this.search.toLowerCase()) !== -1);
    } else {
      return this.stagiaireHttpService.findAll();
    }
  }

  public filter2() {
    if (this.search2) {
      return this.filter().filter(s => s.client.nomEntreprise.toLowerCase().indexOf(this.search2.toLowerCase()) !== -1 || s.prenom.toLowerCase().indexOf(this.search2.toLowerCase()) !== -1);
      console.log(this.filter2());
    } else {
      return this.filter();
    }
  }


  // public filterStagiaireByClient(id:number) {
  //   if (this.search) {
  //     return this.stagiaireHttpService.findByIdClient(id).filter(s => s.nom.toLowerCase().indexOf(this.search.toLowerCase()) !== -1 || s.prenom.toLowerCase().indexOf(this.search.toLowerCase()) !== -1);
  //   } else {
  //     return this.stagiaireHttpService.findByIdClient(id);
  //   }
  // }
  //
  // public filter2StagiaireByClient() {
  //   if (this.search2) {
  //     return this.filterStagiaireByClient(this.getIdUser()).filter(s => s.client.nomEntreprise.toLowerCase().indexOf(this.search2.toLowerCase()) !== -1 || s.prenom.toLowerCase().indexOf(this.search2.toLowerCase()) !== -1);
  //     console.log(this.filter2());
  //   } else {
  //     return this.filterStagiaireByClient(this.getIdUser());
  //   }
  // }

  findStagiaireByClient(id: number){
    return this.stagiaireHttpService.findByIdClient(id).subscribe(resp => this.stagiaireList = resp, err => console.log(err));

  }

  list(): Array<Stagiaire> {
    return this.stagiaireHttpService.findAll();
  }

  add() {
    this.stagiaireForm = new Stagiaire();
    console.log(this.stagiaireForm);
  }

  view(id: number) {
    this.stagiaireHttpService.findById(id).subscribe(resp => {this.stagiaireView = resp;
      if (this.stagiaireView.adresse == null) {
        this.stagiaireView.adresse = new Adresse();
      }
      if (this.stagiaireView.formation == null) {
        this.stagiaireView.formation = new Formation();
      }
      if (this.stagiaireView.ordinateur == null) {
        this.stagiaireView.ordinateur = new Ordinateur();
      }
      if (this.stagiaireView.client == null){
        this.stagiaireView.client = new Client();
      }}, err => console.log(err));
  }

  presence(id: number) {
    this.stagiaireHttpService.findById(id).subscribe(resp => {this.stagiairePresence = resp;
      this.selectedStagiaire = resp;
    }, err => console.log(err));
  }

  edit(id: number) {
    this.stagiaireHttpService.findById(id).subscribe(resp => {this.stagiaireForm = resp;
      if (this.stagiaireForm.adresse == null){
        this.stagiaireForm.adresse = new Adresse();
      }
      if (this.stagiaireForm.formation == null){
        this.stagiaireForm.formation = new Formation();
      }
      if (this.stagiaireForm.ordinateur == null){
        this.stagiaireForm.ordinateur = new Ordinateur();
      }
      if (this.stagiaireForm.client == null){
        this.stagiaireForm.client = new Client();
      }}, err => console.log(err));
  }

  delete(id: number) {
    this.stagiaireHttpService.delete(id);
  }

  save() {
    if(!this.stagiaireForm.utilisateur){
      this.utilisateur = new Utilisateur('Stagiaire');
      this.stagiaireForm.utilisateur = this.utilisateur;
      this.utilisateur = null;
    }
    if (this.stagiaireForm.client.id == null){
      this.stagiaireForm.client = new Client();
    }
    this.stagiaireHttpService.save(this.stagiaireForm);
    this.stagiaireForm = null;
  }

  cancel() {
    this.stagiaireForm = null;
  }

  civilite() {
    this.stagiaireHttpService.findCivilite().subscribe(resp => this.civilites = resp, err => console.log(err));
  }

  //selectToday() {
  //  this.modelCal = this.calendar.getToday();
  //}

  // exportCsv(){
  //   new Angular5Csv(TableData,'Test Report');
  // }

  toggleVisible() {
    this.calendarVisible = !this.calendarVisible;
  }

  toggleWeekends() {
    this.calendarWeekends = !this.calendarWeekends;
  }

  gotoPast() {
    let calendarApi = this.calendarComponent.getApi();
    calendarApi.gotoDate('2000-01-01'); // call a method on the Calendar object
  }

  //handleDateClick(arg) {
    //if (confirm('Would you like to add an event to ' + arg.dateStr + ' ?')) {
    //  this.calendarEvents = this.calendarEvents.concat({ // add new event data. must create new array
     //   title: 'New Event',
     //   start: arg.date,
     //   allDay: arg.allDay
     // })
    //}
  //}

  filterclient() {
    return this.clientHttpService.findAll();
    // if (this.searchclient) {
    //   return this.clientHttpService.findAll().filter(c => c.nomEntreprise.toLowerCase().indexOf(this.searchclient.toLowerCase()) !== -1 ||  c.nomBusinessUnit.toLowerCase().indexOf(this.searchclient .toLowerCase()) !== -1 );
    // } else {
    //   return this.clientHttpService.findAll();
    // }
  }

  filterordi() {
    if (this.searchordi) {
      return this.ordinateurHttpService.findAll().filter(o => o.code !== -1);
    } else {
      return this.ordinateurHttpService.findAll();
    }
  }

  filterformation() {
    if (this.searchformation) {
      return this.formationHttpService.findAll().filter(f => f.intitule.toLowerCase().indexOf(this.search.toLowerCase()) !== -1);
    } else {
      return this.formationHttpService.findAll();
    }
  }
savecsv(){
 this.stagiaireExports = Array<StagiaireExport>();
    this.data = this.filter2();
    for (var i in this.data){
      console.log(this.data[i]);
   this.stagiaireexport.civilite= this.data[i].civilite;
      this.stagiaireexport.nom= this.data[i].nom;
      this.stagiaireexport.prenom= this.data[i].prenom;
      this.stagiaireexport.telephone= this.data[i].telephone;
      if (this.data[i].client!=null) {
        this.stagiaireexport.client = this.data[i].client.nomEntreprise;
      }else{
        this.stagiaireexport.client ="";
      }
      this.stagiaireexport.mail=this.data[i].utilisateur.mail;
      this.stagiaireexport.voie=this.data[i].adresse.voie;
      this.stagiaireexport.complement=this.data[i].adresse.complement;
      this.stagiaireexport.codePostal=this.data[i].adresse.codePostal;
      this.stagiaireexport.ville=this.data[i]. adresse.ville;
      this.stagiaireexport.pays=this.data[i]. adresse.pays;
      this.stagiaireexport.commentaire=this.data[i].commentaires;
      this.stagiaireExports.push(this.stagiaireexport);
      this.stagiaireexport= new StagiaireExport();
    }
  const options = {
    fieldSeparator: ';',
    quoteStrings: '"',
    decimalSeparator: '.',
    showLabels: true,
    showTitle: true,
    title: 'listes des stagiaires',
    filename : 'liste des stagiaires',
    useTextFile: false,
    useBom: true,
    //useKeysAsHeaders: true,
    headers: ['Civilité', 'Nom', 'Prenom', 'Telephone' , 'Client' , 'Mail' , 'Voie'  , 'Complement ', 'Codepostal ', 'Ville' , 'Pays' , 'Commentaire' ,]


}
    const csvExporter = new ExportToCsv(options);
  csvExporter.generateCsv(this.stagiaireExports);
  } ;

  hasAnyRole(roles: string[]) {
    return this.authService.hasAnyRole(roles);
  }

  getIdUser() {
    return this.authService.getUser().id;
  }

  findIdClientByContact(id:number){

    this.stagiaireHttpService.findClientByContact(id).subscribe(resp => {this.clientContact = resp;
    this.findStagiaireByClient(this.clientContact.id)


    }, err => console.log(err));




  }

}
