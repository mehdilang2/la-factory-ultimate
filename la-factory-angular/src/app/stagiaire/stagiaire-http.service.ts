import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Stagiaire} from '../model/stagiaire';

@Injectable()
export class StagiaireHttpService {

  private stagiaire: any;

  constructor(private http: HttpClient) {
    this.load();
  }

  load() {
    this.http.get('http://localhost:8080/api/stagiaire/formation').subscribe(resp => {
      this.stagiaire = resp;
      console.log(this.stagiaire);
    }, err => console.log(err));
  }

  findAll(): Array<Stagiaire> {
    return this.stagiaire;
  }

  findById(id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/stagiaire/' + id);
  }

  findByIdClient(id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/client/' + id + '/stagiaires');
  }

  findClientByContact(id: number): Observable<Object>{
    return this.http.get( 'http://localhost:8080/api/contact/' + id + '/client');
}

  findCivilite(): Observable<Object> {
    return this.http.get('http://localhost:8080/api/stagiaire/civilites');
  }

  save(stagiaire: Stagiaire) {
    if (stagiaire) {
      if (stagiaire.id) {
        this.http.put('http://localhost:8080/api/stagiaire/' + stagiaire.id, stagiaire).subscribe(resp => this.load(),
          err => console.log(err)
        );
      } else {
        this.http.post('http://localhost:8080/api/stagiaire/', stagiaire).subscribe(resp => this.load(),
          err => console.log(err));
      }
    }
  }

  delete(id:number) {
    this.http.delete('http://localhost:8080/api/stagiaire/' + id).subscribe(resp => this.load(),
      err => console.log(err));

  }

}
