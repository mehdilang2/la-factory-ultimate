import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Ordinateur} from '../model/ordinateur';
import {Observable} from 'rxjs';
import {MaterielFormationHttpService} from "../materiel-formation/materielFormation-http.service";



@Injectable()
export class OrdinateurHttpService {

  private ordinateurs: any;
  formation:any = null;


  constructor(private http: HttpClient) {
    this.load();
  }

  load() {
    this.http.get('http://localhost:8080/api/ordinateur').subscribe(resp => {
      this.ordinateurs = resp;
    }, err => console.log(err));
  }

  findAll(): Array<Ordinateur> {
    return this.ordinateurs;
  }

  findById(id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/ordinateur/' + id);
  }

  findMaterielFormationById(id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/ordinateur/' + id + '/materielFormation');
  }

  findByIdWithDates(id: number): Observable<Object> {
    return this.http.get('http://localhost:8080/api/salle/' + id + '/dates');
  }

  save(ordinateur: Ordinateur) {
    if (ordinateur) {
      if (ordinateur.id) {
        this.http.put('http://localhost:8080/api/ordinateur/' + ordinateur.id, ordinateur).subscribe(resp =>{
          this.load();
          },
          err => console.log(err)
        );
      } else {
        this.http.post('http://localhost:8080/api/ordinateur', ordinateur).subscribe(resp => this.load(),
          err => console.log(err));
      }
    }
  }

  delete(id: number) {
    this.http.delete('http://localhost:8080/api/ordinateur/' + id).subscribe(resp => this.load(),
      err => console.log(err));

  }

}
