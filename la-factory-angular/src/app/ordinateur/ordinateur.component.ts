import { Component, OnInit } from '@angular/core';
import {OrdinateurHttpService} from './ordinateur-http.service';
import {Ordinateur} from '../model/ordinateur';
import {FormationHttpService} from "../formation/formation-http.service";
import {MaterielFormationHttpService} from "../materiel-formation/materielFormation-http.service";
import {Formation} from "../model/formation";
import {MaterielFormation} from "../model/materielFormation";
import {Videoprojecteur} from "../model/videoprojecteur";


@Component({
  selector: 'ordinateur',
  templateUrl: './ordinateur.component.html',
  styleUrls: ['./ordinateur.component.css'],
  providers: [FormationHttpService]
})
export class OrdinateurComponent implements OnInit {

  private ordinateurForm: any = null;
  private ordinateurView: any = null;
  private ordinateurResa: any = null;
  private materielFormationCalendar: any = null;
  private formations: any = null;
  private ordinateurId: number;

  constructor(private ordinateurHttpService: OrdinateurHttpService, private formationHttpService: FormationHttpService, private materielFormationHttpService: MaterielFormationHttpService) {
    this.listFormation();
  }

  ngOnInit() {
  }

  list(): Array<Ordinateur> {
    return this.ordinateurHttpService.findAll();
  }

  listFormation() {
    this.formationHttpService.findAllFormation().subscribe(resp => this.formations = resp, err => console.log(err));
    console.log(this.formations);
  }

  listReservationById(id: number) {
    this.ordinateurId = id;

    return this.ordinateurHttpService.findMaterielFormationById(id).subscribe(resp => {
      this.ordinateurResa = resp;
    }, err => console.log(err));

  }

  findByIdWithDates(id: number) {
    return this.ordinateurHttpService.findByIdWithDates(id).subscribe(resp => this.ordinateurForm = resp, err => console.log(err));
  }

  add() {
    this.ordinateurForm = new Ordinateur();
  }

  addResa() {
    this.materielFormationCalendar = new MaterielFormation();
    if (this.materielFormationCalendar.formation == null) {
      this.materielFormationCalendar.formation = new Formation();
    }
    console.log(this.ordinateurId);
    if (this.materielFormationCalendar.materiel == null) {
      this.materielFormationCalendar.materiel = new Ordinateur();
      this.materielFormationCalendar.materiel.id = this.ordinateurId;
    }
  }

  edit(id: number) {
    this.ordinateurHttpService.findById(id).subscribe(resp => {
        this.ordinateurForm = resp

        for (let mf of this.ordinateurForm.materielFormation) {
          if (!mf.formation) {
            mf.formation = new Formation();
          }

        }


      }


      , err => console.log(err));

  }


  editResa(id: number) {
    this.materielFormationHttpService.findById(id).subscribe(resp => {
      this.materielFormationCalendar = resp;

    }, err => console.log(err));
  }

  view(id: number) {
    this.ordinateurHttpService.findById(id).subscribe(resp => this.ordinateurView = resp, err => console.log(err));
  }

  delete(id: number) {
    this.ordinateurHttpService.delete(id);
  }

  deleteResa(id: number) {
    this.materielFormationHttpService.delete(id);
  }

  save() {
    this.ordinateurHttpService.save(this.ordinateurForm);
    this.ordinateurForm = null;
  }

  reserv() {

    this.materielFormationHttpService.save(this.materielFormationCalendar);
    this.materielFormationCalendar = null;

  }

  cancel() {
    this.ordinateurForm = null;
  }

  cancelResa() {
    this.materielFormationCalendar = null;
  }

  // disponibilite(){
  //   let year = (new Date()).getFullYear();
  //   let month = (new Date()).getUTCMonth() + 1;
  //   let day = (new Date()).getDate()
  //
  //   alert(year +" "+ month +" " + day);
  //
  //
  //
  // }



}
